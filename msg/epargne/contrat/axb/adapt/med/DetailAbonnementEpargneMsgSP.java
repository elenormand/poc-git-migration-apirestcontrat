package msg.epargne.contrat.axb.adapt.med;

import java.util.Arrays;
import java.util.Date;

import fr.axb.lib.exchange.retour.msg.RetourMsg;

public class DetailAbonnementEpargneMsgSP {

	private String idContrat;

	private String codProduit;
	
	private Integer periodicite;
	
	private Integer jourPrelevement;
	
	private Date dateCreation;
	
	private Date dateDebutSuspension;
	
	private Date dateFinSuspension;
	
	private ContratMsgSP contratPortefeuille;
	
	private ContratMsgSP contratPrelevement;
	
	private ValeurParametreeMsgSP [] valeursParametrees;
	
	private ValeurPossibleMsgSP [] valeursPossibles;
	
	private RetourMsg retour;

	public String getIdContrat() {
		return idContrat;
	}

	public void setIdContrat(String idContrat) {
		this.idContrat = idContrat;
	}

	public String getCodProduit() {
		return codProduit;
	}

	public void setCodProduit(String codProduit) {
		this.codProduit = codProduit;
	}

	public Integer getPeriodicite() {
		return periodicite;
	}

	public void setPeriodicite(Integer periodicite) {
		this.periodicite = periodicite;
	}

	public Integer getJourPrelevement() {
		return jourPrelevement;
	}

	public void setJourPrelevement(Integer jourPrelevement) {
		this.jourPrelevement = jourPrelevement;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateDebutSuspension() {
		return dateDebutSuspension;
	}

	public void setDateDebutSuspension(Date dateDebutSuspension) {
		this.dateDebutSuspension = dateDebutSuspension;
	}

	public Date getDateFinSuspension() {
		return dateFinSuspension;
	}

	public void setDateFinSuspension(Date dateFinSuspension) {
		this.dateFinSuspension = dateFinSuspension;
	}

	public ContratMsgSP getContratPortefeuille() {
		return contratPortefeuille;
	}

	public void setContratPortefeuille(ContratMsgSP contratPortefeuille) {
		this.contratPortefeuille = contratPortefeuille;
	}

	public ContratMsgSP getContratPrelevement() {
		return contratPrelevement;
	}

	public void setContratPrelevement(ContratMsgSP contratPrelevement) {
		this.contratPrelevement = contratPrelevement;
	}

	public ValeurParametreeMsgSP[] getValeursParametrees() {
		return valeursParametrees;
	}

	public void setValeursParametrees(ValeurParametreeMsgSP[] valeursParametrees) {
		this.valeursParametrees = valeursParametrees;
	}

	public ValeurPossibleMsgSP[] getValeursPossibles() {
		return valeursPossibles;
	}

	public void setValeursPossibles(ValeurPossibleMsgSP[] valeursPossibles) {
		this.valeursPossibles = valeursPossibles;
	}
	
	public RetourMsg getRetour() {
		return retour;
	}

	public void setRetour(RetourMsg retour) {
		this.retour = retour;
	}
	
	@Override
	public String toString() {
		return "DetailAbonnementEpargneMsg [numContrat=" + idContrat + ", codProduit=" + codProduit + ", periodicite="
				+ periodicite + ", jourPrelevement=" + jourPrelevement + ", dateCreation=" + dateCreation
				+ ", dateDebutSuspension=" + dateDebutSuspension + ", dateFinSuspension=" + dateFinSuspension
				+ ", contratPortefeuille=" + contratPortefeuille + ", contratPrelevement=" + contratPrelevement
				+ ", valeursParametrees=" + Arrays.toString(valeursParametrees) + ", valeursPossibles="
				+ Arrays.toString(valeursPossibles) + ", retour=" + retour + "]";
	}
	
	

}
