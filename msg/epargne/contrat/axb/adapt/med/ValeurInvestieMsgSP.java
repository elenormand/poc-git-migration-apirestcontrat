package msg.epargne.contrat.axb.adapt.med;

public class ValeurInvestieMsgSP extends ValeurMsgSP {


	private String idInvestissement;

	private String suppressionBln;

	private String montant;
	
	private String codDevise;

	public String getIdInvestissement() {
		return idInvestissement;
	}

	public void setIdInvestissement(String idInvestissement) {
		this.idInvestissement = idInvestissement;
	}

	public String getSuppressionBln() {
		return suppressionBln;
	}

	public void setSuppressionBln(String suppressionBln) {
		this.suppressionBln = suppressionBln;
	}

	public String getMontant() {
		return montant;
	}

	public void setMontant(String montant) {
		this.montant = montant;
	}

	public String getCodDevise() {
		return codDevise;
	}

	public void setCodDevise(String codDevise) {
		this.codDevise = codDevise;
	}

	@Override
	public String toString() {
		return "ValeurInvestieMsg [idInvestissement=" + idInvestissement
				+ ", idValeur=" + getIdValeur()
				+ ", suppressionBln=" + suppressionBln + ", montant=" + montant
				+ ", codDevise=" + codDevise + "]";
	}
	
}
