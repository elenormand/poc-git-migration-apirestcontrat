package msg.epargne.contrat.axb.adapt.med;

import java.io.Serializable;
import java.util.Arrays;

import fr.axb.lib.exchange.retour.msg.RetourMsg;

public class DetailValeursAbonnementPossibleMsgSP implements Serializable {

	private static final long serialVersionUID = 1L;

	private ValeurPossibleMsgSP[] valeursPossibles;
	private RetourMsg retour;

	public ValeurPossibleMsgSP[] getValeursPossibles() {
		return valeursPossibles;
	}
	public void setValeursPossibles(ValeurPossibleMsgSP[] valeursPossibles) {
		this.valeursPossibles = valeursPossibles;
	}
	public RetourMsg getRetour() {
		return retour;
	}
	public void setRetour(RetourMsg retour) {
		this.retour = retour;
	}
	@Override
	public String toString() {
		return String.format("DetailValeursAbonnementPossibleMsgSP [valeursPossibles=%s, retour=%s]", Arrays.toString(valeursPossibles), retour);
	}
}