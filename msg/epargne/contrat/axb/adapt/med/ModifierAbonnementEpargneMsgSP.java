package msg.epargne.contrat.axb.adapt.med;

import java.util.Arrays;

public class ModifierAbonnementEpargneMsgSP {

	private String idContrat;

	private String idContratPrelevement;

	private Integer periodicite;

	private Integer jourPrelevement;

	private ValeurInvestieMsgSP[] valeursInvsties;

	public String getIdContrat() {
		return idContrat;
	}

	public void setIdContrat(String idContrat) {
		this.idContrat = idContrat;
	}

	public String getIdContratPrelevement() {
		return idContratPrelevement;
	}

	public void setIdContratPrelevement(String idContratPrelevement) {
		this.idContratPrelevement = idContratPrelevement;
	}

	public Integer getPeriodicite() {
		return periodicite;
	}

	public void setPeriodicite(Integer periodicite) {
		this.periodicite = periodicite;
	}

	public Integer getJourPrelevement() {
		return jourPrelevement;
	}

	public void setJourPrelevement(Integer jourPrelevement) {
		this.jourPrelevement = jourPrelevement;
	}

	public ValeurInvestieMsgSP[] getValeursInvsties() {
		return valeursInvsties;
	}

	public void setValeursInvsties(ValeurInvestieMsgSP[] valeursInvsties) {
		this.valeursInvsties = valeursInvsties;
	}

	@Override
	public String toString() {
		return "AbonnementEpargneModifMsg [idContrat=" + idContrat
				+ ", idContratPrelevement="	+ idContratPrelevement 
				+ ", periodicite=" + periodicite
				+ ", jourPrelevement=" + jourPrelevement + ", valeursInvsties="
				+ Arrays.toString(valeursInvsties) + "]";
	}

}
