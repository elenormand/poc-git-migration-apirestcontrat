package msg.epargne.contrat.axb.adapt.med;

public class ValeurPossibleMsgSP extends ValeurMsgSP {

	private String codIsin;

	private String nom;

	private Double dernierCours;
	
	private String restriction;
	
	
	public String getCodIsin() {
		return codIsin;
	}

	public void setCodIsin(String codIsin) {
		this.codIsin = codIsin;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Double getDernierCours() {
		return dernierCours;
	}

	public void setDernierCours(Double dernierCours) {
		this.dernierCours = dernierCours;
	}

	public String getRestriction() {
		return restriction;
	}

	public void setRestriction(String restriction) {
		this.restriction = restriction;
	}

	@Override
	public String toString() {
		return "ValeurPossible [codIsin=" + codIsin 
				+ ", idValeur=" + getIdValeur()
				+ ", nom=" + nom
				+ ", dernierCours=" + dernierCours + ", restriction="
				+ restriction + "]";
	}
}
	
