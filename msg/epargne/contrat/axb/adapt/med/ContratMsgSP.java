package msg.epargne.contrat.axb.adapt.med;

public class ContratMsgSP {

	private String numContrat;

	private String codProduit;

	private String codSousProduit;
	
	private String numCompte;

	private String codFamille;
	
	private String codPackage;

	private Long id;

	public String getNumContrat() {
		return numContrat;
	}

	public void setNumContrat(String numContrat) {
		this.numContrat = numContrat;
	}

	public String getCodProduit() {
		return codProduit;
	}

	public void setCodProduit(String codProduit) {
		this.codProduit = codProduit;
	}

	public String getCodSousProduit() {
		return codSousProduit;
	}

	public void setCodSousProduit(String codSousProduit) {
		this.codSousProduit = codSousProduit;
	}

	public String getNumCompte() {
		return numCompte;
	}

	public void setNumCompte(String numCompte) {
		this.numCompte = numCompte;
	}

	public String getCodFamille() {
		return codFamille;
	}

	public void setCodFamille(String codFamille) {
		this.codFamille = codFamille;
	}

	public String getCodPackage() {
		return codPackage;
	}

	public void setCodPackage(String codPackage) {
		this.codPackage = codPackage;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "ContratMsg [numContrat=" + numContrat + ", codProduit=" + codProduit + ", codSousProduit="
				+ codSousProduit + ", numCompte=" + numCompte + ", codFamille=" + codFamille + ", codPackage="
				+ codPackage + ", id=" + id + "]";
	}
	
	
}
