package msg.epargne.contrat.axb.adapt.med;

public class ValeurParametreeMsgSP {

	private String idInvestissement;

	private Double montant;

	private String idValeur;

	public String getIdValeur() {
		return idValeur;
	}

	public void setIdValeur(String idValeur) {
		this.idValeur = idValeur;
	}

	public String getIdInvestissement() {
		return idInvestissement;
	}

	public void setIdInvestissement(String idInvestissement) {
		this.idInvestissement = idInvestissement;
	}


	public Double getMontant() {
		return montant;
	}

	public void setMontant(Double montant) {
		this.montant = montant;
	}

	@Override
	public String toString() {
		return "ValeurParametree [idInvestissement=" + idInvestissement
				+ ", idValeur=" + idValeur
				+ ", montant=" + montant + "]";
	}
	
	

}

