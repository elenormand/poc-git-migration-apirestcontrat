package msg.detailcredit.contrat.apirest.axb.adapt.med;

import java.io.Serializable;

import fr.axb.lib.exchange.retour.msg.CodMsg;

public class EmprunteurMsg implements Serializable{
	private static final long serialVersionUID = 1L;
	private String idClient;
	private String nom;
	private CodMsg codRole;
	
	public EmprunteurMsg(String idClient, String nom, CodMsg codRole) {
		super();
		this.idClient = idClient;
		this.nom = nom;
		this.codRole = codRole;
	}
	public String getIdClient() {
		return idClient;
	}
	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public CodMsg getCodRole() {
		return codRole;
	}
	public void setCodRole(CodMsg codRole) {
		this.codRole = codRole;
	}
}
