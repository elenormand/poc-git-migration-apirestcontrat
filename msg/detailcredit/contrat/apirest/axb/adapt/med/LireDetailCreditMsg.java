package msg.detailcredit.contrat.apirest.axb.adapt.med;

import java.io.Serializable;
import java.util.Date;

import msg.listercontrat.contrat.apirest.axb.adapt.med.ProduitMsg;
import fr.axb.lib.exchange.retour.msg.CodMsg;
import fr.axb.lib.exchange.retour.msg.RetourMsg;
import static fr.axb.lib.exchange.retour.msg.CodeRetourFactory.RETOUR_CORRECT;

/**
 * Retour du Sp LireDetailCredit
 * 
 * @author naje99k
 *
 */
public class LireDetailCreditMsg implements Serializable{
	private static final long serialVersionUID = 1L;
	private String idContrat = null;
	private ProduitMsg produit = null;
	private String numDossier = null;
	private Integer duree = null;
	private Date dateDebutCredit = null;
	private Date dateFinInitiale = null;
	private Date dateDerniereMensualite = null;
	private Double montantEmprunte = null;
	private Double montantMensualite = null;
	private Double mntCapitalRestantDu = null;
	private Double montantProchaineEcheance = null;
	private Double montantReserve = null;
	private Double montantDisponibleReserve = null;
	private Double tauxAnnuelEffectifGlobal = null;
	private CodMsg codRoleCredit = null;
	private Boolean assuranceBln = null;
	private String tauxReference = null;
	private Double tauxFixe = null;
	private Double tauxAssurance = null;
	private EmprunteurMsg[] emprunteurs  = null;
	private RetourMsg retour = RETOUR_CORRECT;
	public String getIdContrat() {
		return idContrat;
	}
	public void setIdContrat(String idContrat) {
		this.idContrat = idContrat;
	}
	public ProduitMsg getProduit() {
		return produit;
	}
	public void setProduit(ProduitMsg produit) {
		this.produit = produit;
	}
	public Integer getDuree() {
		return duree;
	}
	public void setDuree(Integer duree) {
		this.duree = duree;
	}
	public Date getDateDebutCredit() {
		return dateDebutCredit;
	}
	public void setDateDebutCredit(Date dateDebutCredit) {
		this.dateDebutCredit = dateDebutCredit;
	}
	public Date getDateFinInitiale() {
		return dateFinInitiale;
	}
	public void setDateFinInitiale(Date dateFinInitiale) {
		this.dateFinInitiale = dateFinInitiale;
	}
	public Date getDateDerniereMensualite() {
		return dateDerniereMensualite;
	}
	public void setDateDerniereMensualite(Date dateDerniereMensualite) {
		this.dateDerniereMensualite = dateDerniereMensualite;
	}
	public Double getMontantEmprunte() {
		return montantEmprunte;
	}
	public void setMontantEmprunte(Double montantEmprunte) {
		this.montantEmprunte = montantEmprunte;
	}
	public Double getMontantMensualite() {
		return montantMensualite;
	}
	public void setMontantMensualite(Double montantMensualite) {
		this.montantMensualite = montantMensualite;
	}
	public Double getMntCapitalRestantDu() {
		return mntCapitalRestantDu;
	}
	public void setMntCapitalRestantDu(Double mntCapitalRestantDu) {
		this.mntCapitalRestantDu = mntCapitalRestantDu;
	}
	public Double getMontantProchaineEcheance() {
		return montantProchaineEcheance;
	}
	public void setMontantProchaineEcheance(Double montantProchaineEcheance) {
		this.montantProchaineEcheance = montantProchaineEcheance;
	}
	public Double getMontantReserve() {
		return montantReserve;
	}
	public void setMontantReserve(Double montantReserve) {
		this.montantReserve = montantReserve;
	}
	public Double getTauxAnnuelEffectifGlobal() {
		return tauxAnnuelEffectifGlobal;
	}
	public void setTauxAnnuelEffectifGlobal(Double tauxAnnuelEffectifGlobal) {
		this.tauxAnnuelEffectifGlobal = tauxAnnuelEffectifGlobal;
	}
	public CodMsg getCodRoleCredit() {
		return codRoleCredit;
	}
	public void setCodRoleCredit(CodMsg codRoleCredit) {
		this.codRoleCredit = codRoleCredit;
	}
	public Boolean getAssuranceBln() {
		return assuranceBln;
	}
	public void setAssuranceBln(Boolean assuranceBln) {
		this.assuranceBln = assuranceBln;
	}
	public String getTauxReference() {
		return tauxReference;
	}
	public void setTauxReference(String tauxReference) {
		this.tauxReference = tauxReference;
	}
	public Double getTauxFixe() {
		return tauxFixe;
	}
	public void setTauxFixe(Double tauxFixe) {
		this.tauxFixe = tauxFixe;
	}
	public Double getTauxAssurance() {
		return tauxAssurance;
	}
	public void setTauxAssurance(Double tauxAssurance) {
		this.tauxAssurance = tauxAssurance;
	}
	public EmprunteurMsg[] getEmprunteurs() {
		return emprunteurs;
	}
	public void setEmprunteurs(EmprunteurMsg[] emprunteurs) {
		this.emprunteurs = emprunteurs;
	}
	public RetourMsg getRetour() {
		return retour;
	}
	public void setRetour(RetourMsg retour) {
		this.retour = retour;
	}
	public String getNumDossier() {
		return numDossier;
	}
	public void setNumDossier(String numDossier) {
		this.numDossier = numDossier;
	}
	public Double getMontantDisponibleReserve() {
		return montantDisponibleReserve;
	}
	public void setMontantDisponibleReserve(Double montantDisponibleReserve) {
		this.montantDisponibleReserve = montantDisponibleReserve;
	}
	
	
}
