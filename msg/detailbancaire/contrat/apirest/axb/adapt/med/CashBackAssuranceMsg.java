//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.10.03 at 05:44:18 PM CEST 
//


package msg.detailbancaire.contrat.apirest.axb.adapt.med;

import java.io.Serializable;
import java.util.Date;

public class CashBackAssuranceMsg implements Serializable{
	private static final long serialVersionUID = 1L;
	protected Date dateOperation;
    protected String libelleProduitAssurance;
    protected Double montantCashBack;
    protected Double montantNetAnnuel;
    protected String numContratAssurance;
    protected Double tauxRetrocession;

    /**
     * Gets the value of the dateOperation property.
     * 
     * @return
     *     possible object is
     *     {@link Date }
     *     
     */
    public Date getDateOperation() {
        return dateOperation;
    }

    /**
     * Sets the value of the dateOperation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Date }
     *     
     */
    public void setDateOperation(Date value) {
        this.dateOperation = value;
    }

    /**
     * Gets the value of the libelleProduitAssurance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLibelleProduitAssurance() {
        return libelleProduitAssurance;
    }

    /**
     * Sets the value of the libelleProduitAssurance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLibelleProduitAssurance(String value) {
        this.libelleProduitAssurance = value;
    }

    /**
     * Gets the value of the montantCashBack property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getMontantCashBack() {
        return montantCashBack;
    }

    /**
     * Sets the value of the montantCashBack property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setMontantCashBack(Double value) {
        this.montantCashBack = value;
    }

    /**
     * Gets the value of the montantNetAnnuel property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getMontantNetAnnuel() {
        return montantNetAnnuel;
    }

    /**
     * Sets the value of the montantNetAnnuel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setMontantNetAnnuel(Double value) {
        this.montantNetAnnuel = value;
    }

    /**
     * Gets the value of the numContratAssurance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumContratAssurance() {
        return numContratAssurance;
    }

    /**
     * Sets the value of the numContratAssurance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumContratAssurance(String value) {
        this.numContratAssurance = value;
    }

    /**
     * Gets the value of the tauxRetrocession property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTauxRetrocession() {
        return tauxRetrocession;
    }

    /**
     * Sets the value of the tauxRetrocession property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTauxRetrocession(Double value) {
        this.tauxRetrocession = value;
    }

}
