//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.10.03 at 05:44:18 PM CEST 
//


package msg.detailbancaire.contrat.apirest.axb.adapt.med;

import java.io.Serializable;


public class VersementsAnnuelsMsg implements Serializable{
	private static final long serialVersionUID = 1L;
    protected Integer anneeVersement;
    protected Double doitsAPret;
    protected Double interetsHorsPrime;
    protected Double interetsTauxBancaire;
    protected Double interetsHorsContrat;
    protected Double primeEtat;
    protected Double versementAnnuelTotal;

    /**
     * Gets the value of the anneeVersement property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAnneeVersement() {
        return anneeVersement;
    }

    /**
     * Sets the value of the anneeVersement property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAnneeVersement(Integer value) {
        this.anneeVersement = value;
    }

    /**
     * Gets the value of the doitsAPret property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getDoitsAPret() {
        return doitsAPret;
    }

    /**
     * Sets the value of the doitsAPret property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setDoitsAPret(Double value) {
        this.doitsAPret = value;
    }

    /**
     * Gets the value of the interetHorsPrime property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getInteretsHorsPrime() {
        return interetsHorsPrime;
    }

    /**
     * Sets the value of the interetHorsPrime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setInteretsHorsPrime(Double value) {
        this.interetsHorsPrime = value;
    }

    /**
     * Gets the value of the interetTauxBancaire property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getInteretsTauxBancaire() {
        return interetsTauxBancaire;
    }

    /**
     * Sets the value of the interetTauxBancaire property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setInteretsTauxBancaire(Double value) {
        this.interetsTauxBancaire = value;
    }

    /**
     * Gets the value of the interetHorsContrat property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getInteretsHorsContrat() {
        return interetsHorsContrat;
    }

    /**
     * Sets the value of the interetHorsContrat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setInteretsHorsContrat(Double value) {
        this.interetsHorsContrat = value;
    }

    /**
     * Gets the value of the primeEtat property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPrimeEtat() {
        return primeEtat;
    }

    /**
     * Sets the value of the primeEtat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPrimeEtat(Double value) {
        this.primeEtat = value;
    }

    /**
     * Gets the value of the versementAnnuelTotal property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getVersementAnnuelTotal() {
        return versementAnnuelTotal;
    }

    /**
     * Sets the value of the versementAnnuelTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setVersementAnnuelTotal(Double value) {
        this.versementAnnuelTotal = value;
    }

}
