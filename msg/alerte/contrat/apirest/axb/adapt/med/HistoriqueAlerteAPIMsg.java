package msg.alerte.contrat.apirest.axb.adapt.med;

import java.io.Serializable;
import java.util.Date;

import fr.axb.lib.exchange.retour.msg.CodMsg;

public class HistoriqueAlerteAPIMsg implements Serializable {

	private static final long serialVersionUID = 1L;

	private CodMsg codAlerte;
	private CodMsg codCanal;
	private Date dteEnvoi;
	private String emailLib;
	private String mobileTel;
	private Double montant;
	private String idClient;
	private String idContrat;
	private Double seuil;

	public CodMsg getCodAlerte() {
		return codAlerte;
	}
	public void setCodAlerte(CodMsg codAlerte) {
		this.codAlerte = codAlerte;
	}
	public CodMsg getCodCanal() {
		return codCanal;
	}
	public void setCodCanal(CodMsg codCanal) {
		this.codCanal = codCanal;
	}
	public Date getDteEnvoi() {
		return dteEnvoi;
	}
	public void setDteEnvoi(Date dteEnvoi) {
		this.dteEnvoi = dteEnvoi;
	}
	public String getEmailLib() {
		return emailLib;
	}
	public void setEmailLib(String emailLib) {
		this.emailLib = emailLib;
	}
	public String getMobileTel() {
		return mobileTel;
	}
	public void setMobileTel(String mobileTel) {
		this.mobileTel = mobileTel;
	}
	public Double getMontant() {
		return montant;
	}
	public void setMontant(Double montant) {
		this.montant = montant;
	}
	public String getIdClient() {
		return idClient;
	}
	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}
	public String getIdContrat() {
		return idContrat;
	}
	public void setIdContrat(String idContrat) {
		this.idContrat = idContrat;
	}
	public Double getSeuil() {
		return seuil;
	}
	public void setSeuil(Double seuil) {
		this.seuil = seuil;
	}
	@Override
	public String toString() {
		return String
				.format(
						"HistoriqueAlerteAPIMsg [codAlerte=%s, codCanal=%s, dteEnvoi=%s, emailLib=%s, mobileTel=%s, montant=%s, idClient=%s, idContrat=%s, seuil=%s",
						codAlerte, codCanal, dteEnvoi, emailLib, mobileTel,
						montant, idClient, idContrat, seuil);
	}
}
