package msg.alerte.contrat.apirest.axb.adapt.med;

import java.io.Serializable;

import fr.axb.lib.exchange.retour.msg.CodMsg;

public class AlerteParametreeAPIMsg implements Serializable {

	private static final long serialVersionUID = 1L;

	private CodMsg code;
	private CodMsg canal;
	private CodMsg support;
	private String id;
	private String idContratSupport;
	private Double seuil;

    public CodMsg getCode() {
        return code;
    }
    public void setCode(CodMsg code) {
        this.code = code;
    }
    public CodMsg getCanal() {
        return canal;
    }
    public void setCanal(CodMsg canal) {
        this.canal = canal;
    }
    public CodMsg getSupport() {
        return support;
    }
    public void setSupport(CodMsg support) {
        this.support = support;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getIdContratSupport() {
        return idContratSupport;
    }
    public void setIdContratSupport(String idContratSupport) {
        this.idContratSupport = idContratSupport;
    }
    public Double getSeuil() {
        return seuil;
    }
    public void setSeuil(Double seuil) {
        this.seuil = seuil;
    }
    @Override
    public String toString() {
        return String.format("AlerteParametreeAPIMsg [code=%s, canal=%s, support=%s, id=%s, idContratSupport=%s, seuil=%s]", code, canal, support, id,
                idContratSupport, seuil);
    }
}