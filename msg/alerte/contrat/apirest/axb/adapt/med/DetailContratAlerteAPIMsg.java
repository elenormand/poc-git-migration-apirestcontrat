package msg.alerte.contrat.apirest.axb.adapt.med;

import java.io.Serializable;
import java.util.Arrays;

import fr.axb.lib.exchange.action.msg.ActionMsg;
import fr.axb.lib.exchange.retour.msg.RetourMsg;

public class DetailContratAlerteAPIMsg implements Serializable {

	private static final long serialVersionUID = 1L;

	private String idContrat;
	private String idContratFacturation;
	private Integer nombreMaxAlertes;
	private HistoriqueAlerteAPIMsg[] historiqueAlertes;
	private AlerteParametreeAPIMsg[] alertesParametrees;
	private AlertePossibleAPIMsg[] alertesPossibles;
	private ActionMsg[] actions;
	private RetourMsg retour;

	public DetailContratAlerteAPIMsg() {
		super();
	}
	
	public DetailContratAlerteAPIMsg(RetourMsg retour) {
		super();
		this.retour = retour;
	}

	public String getIdContrat() {
		return idContrat;
	}

	public void setIdContrat(String idContrat) {
		this.idContrat = idContrat;
	}

	public String getIdContratFacturation() {
		return idContratFacturation;
	}

	public void setIdContratFacturation(String idContratFacturation) {
		this.idContratFacturation = idContratFacturation;
	}

	public Integer getNombreMaxAlertes() {
		return nombreMaxAlertes;
	}

	public void setNombreMaxAlertes(Integer nombreMaxAlertes) {
		this.nombreMaxAlertes = nombreMaxAlertes;
	}

	public HistoriqueAlerteAPIMsg[] getHistoriqueAlertes() {
		return historiqueAlertes;
	}

	public void setHistoriqueAlertes(HistoriqueAlerteAPIMsg[] historiqueAlertes) {
		this.historiqueAlertes = historiqueAlertes;
	}

	public AlerteParametreeAPIMsg[] getAlertesParametrees() {
		return alertesParametrees;
	}

	public void setAlertesParametrees(AlerteParametreeAPIMsg[] alertesParametrees) {
		this.alertesParametrees = alertesParametrees;
	}

	public AlertePossibleAPIMsg[] getAlertesPossibles() {
		return alertesPossibles;
	}

	public void setAlertesPossibles(AlertePossibleAPIMsg[] alertesPossibles) {
		this.alertesPossibles = alertesPossibles;
	}

	public ActionMsg[] getActions() {
		return actions;
	}

	public void setActions(ActionMsg[] actions) {
		this.actions = actions;
	}

	public RetourMsg getRetour() {
		return retour;
	}

	public void setRetour(RetourMsg retour) {
		this.retour = retour;
	}

	@Override
	public String toString() {
		return String
				.format(
						"DetailContratAlerteAPIMsg [idContrat=%s, idContratFacturation=%s, nombreMaxAlertes=%s, historiqueAlertes=%s, alertesParametrees=%s, alertesPossibles=%s, actions=%s, retour=%s]",
						idContrat, idContratFacturation, nombreMaxAlertes,
						Arrays.toString(historiqueAlertes), Arrays.toString(alertesParametrees),
						Arrays.toString(alertesPossibles), Arrays.toString(actions), retour);
	}
}
