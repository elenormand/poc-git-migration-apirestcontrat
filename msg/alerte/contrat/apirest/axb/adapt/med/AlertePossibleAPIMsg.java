package msg.alerte.contrat.apirest.axb.adapt.med;

import java.io.Serializable;
import java.util.Arrays;

import fr.axb.lib.exchange.retour.msg.CodMsg;

public class AlertePossibleAPIMsg implements Serializable {

	private static final long serialVersionUID = 1L;

	private CodMsg code;
	private CodMsg[] canaux;
	private CodMsg[] supports;

    public CodMsg getCode() {
        return code;
    }
    public void setCode(CodMsg code) {
        this.code = code;
    }
    public CodMsg[] getCanaux() {
        return canaux;
    }
    public void setCanaux(CodMsg[] canaux) {
        this.canaux = canaux;
    }
    public CodMsg[] getSupports() {
        return supports;
    }
    public void setSupports(CodMsg[] supports) {
        this.supports = supports;
    }
    @Override
    public String toString() {
        return String.format("Test [code=%s, canaux=%s, supports=%s]", code, Arrays.toString(canaux), Arrays.toString(supports));
    }
}