package msg.alerte.contrat.apirest.axb.adapt.med;

import java.io.Serializable;
import java.util.Arrays;

public class CritereModifierContratAlerteAPIMsg implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idContrat;
    private String codTypeRole;
    private String nombreMaxAlertes;
    private AlerteParametreeAPIMsg[] alertes;

    public String getIdContrat() {
        return idContrat;
    }
    public void setIdContrat(String idContrat) {
        this.idContrat = idContrat;
    }
    public String getCodTypeRole() {
        return codTypeRole;
    }
    public void setCodTypeRole(String codTypeRole) {
        this.codTypeRole = codTypeRole;
    }
    public String getNombreMaxAlertes() {
        return nombreMaxAlertes;
    }
    public void setNombreMaxAlertes(String nombreMaxAlertes) {
        this.nombreMaxAlertes = nombreMaxAlertes;
    }
    public AlerteParametreeAPIMsg[] getAlertes() {
        return alertes;
    }
    public void setAlertes(AlerteParametreeAPIMsg[] alertes) {
        this.alertes = alertes;
    }
    @Override
    public String toString() {
        return String.format("CritereModifierContratAlerteAPIMsg [idContrat=%s, codTypeRole=%s, nombreMaxAlertes=%s, alertes=%s]", idContrat, codTypeRole, nombreMaxAlertes, Arrays.toString(alertes));
    }
}