package msg.listercontrat.contrat.apirest.axb.adapt.med;

import java.io.Serializable;
import java.util.Date;

import fr.axb.lib.exchange.action.msg.ActionMsg;
import fr.axb.lib.exchange.retour.msg.CodMsg;

/**
 * 
 * @author naje99k
 *
 */
public class ServiceMsg implements IContratMsg, Serializable {
	private static final long serialVersionUID = 854918975759839999L;
	private CodMsg codRole;
	private ProduitMsg produit;
	private Date dteCloture;
	private Date dteDebutSuspension;
	private Date dteFinSuspension;
	private String libelle;
	private String idContrat;
	private ActionMsg[] actions;
	private String numContrat;
	
	public CodMsg getCodRole() {
		return codRole;
	}
	public void setCodRole(CodMsg codRole) {
		this.codRole = codRole;
	}
	public ProduitMsg getProduit() {
		return produit;
	}
	public void setProduit(ProduitMsg produit) {
		this.produit = produit;
	}
	public Date getDteCloture() {
		return dteCloture;
	}
	public void setDteCloture(Date dteCloture) {
		this.dteCloture = dteCloture;
	}
	public Date getDteDebutSuspension() {
		return dteDebutSuspension;
	}
	public void setDteDebutSuspension(Date dteDebutSuspension) {
		this.dteDebutSuspension = dteDebutSuspension;
	}	
	public Date getDteFinSuspension() {
		return dteFinSuspension;
	}
	public void setDteFinSuspension(Date dteFinSuspension) {
		this.dteFinSuspension = dteFinSuspension;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getNumContrat() {	
		return numContrat;
	}
	public void setNumContrat(String numContrat) {	
		this.numContrat = numContrat;
	}
	public String getIdContrat() {
		return idContrat;
	}
	public void setIdContrat(String idContrat) {
		this.idContrat = idContrat;
	}
	public ActionMsg[] getActions() {
		return actions;
	}
	public void setActions(ActionMsg[] actions) {
		this.actions = actions;
	}		
}
