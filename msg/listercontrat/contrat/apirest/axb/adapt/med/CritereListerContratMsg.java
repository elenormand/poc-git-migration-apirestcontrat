package msg.listercontrat.contrat.apirest.axb.adapt.med;

import java.io.Serializable;

public class CritereListerContratMsg implements Serializable {

	private static final long serialVersionUID = 854918975759839999L;
	private String idClient;
	private Integer codTypeRole;
	private Integer typeClient;
	public String getIdClient() {
		return idClient;
	}
	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}
	public Integer getCodTypeRole() {
		return codTypeRole;
	}
	public void setCodTypeRole(Integer codTypeRole) {
		this.codTypeRole = codTypeRole;
	}
	public Integer getTypeClient() {
		return typeClient;
	}
	public void setTypeClient(Integer typeClient) {
		this.typeClient = typeClient;
	}	
}
