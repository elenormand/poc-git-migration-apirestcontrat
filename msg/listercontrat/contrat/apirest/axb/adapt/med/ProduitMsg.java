package msg.listercontrat.contrat.apirest.axb.adapt.med;

import java.io.Serializable;

import fr.axb.lib.exchange.retour.msg.CodMsg;

public class ProduitMsg implements Serializable {

	private static final long serialVersionUID = 854918975759839999L;
	private CodMsg codProduit;
	private CodMsg codSousProduit;
	private CodMsg codFamille;
	private CodMsg codPackage;
	private CodMsg codProduitAgrege;
	public CodMsg getCodProduit() {
		return codProduit;
	}
	public void setCodProduit(CodMsg codProduit) {
		this.codProduit = codProduit;
	}
	public void setCodProduit(Integer codProduit) {		
		this.codProduit = toCodMsg(codProduit);
	}
	public CodMsg getCodSousProduit() {
		return codSousProduit;
	}
	public void setCodSousProduit(CodMsg codSousProduit) {
		this.codSousProduit = codSousProduit;
	}
	public void setCodSousProduit(Integer codSousProduit) {	
		this.codSousProduit = toCodMsg(codSousProduit);
	}
	public CodMsg getCodFamille() {
		return codFamille;
	}
	public void setCodFamille(CodMsg codFamille) {
		this.codFamille = codFamille;
	}
	public void setCodFamille(Integer codFamille) {		
		this.codFamille = toCodMsg(codFamille);
	}
	public CodMsg getCodPackage() {
		return codPackage;
	}
	public void setCodPackage(CodMsg codPackage) {
		this.codPackage = codPackage;
	}
	public void setCodPackage(Integer codPackage) {
		this.codPackage = toCodMsg(codPackage);
	}
	public CodMsg getCodProduitAgrege() {
		return codProduitAgrege;
	}
	public void setCodProduitAgrege(CodMsg codProduitAgrege) {
		this.codProduitAgrege = codProduitAgrege;
	}
	public void setCodProduitAgrege(Integer codProduitAgrege) {
		this.codProduitAgrege = toCodMsg(codProduitAgrege);
	}
	
	static CodMsg toCodMsg(Integer code, String libelle){
		return new CodMsg(code, libelle);
	}
	public static CodMsg toCodMsg(Integer code){
		return toCodMsg(code, null);
	}
}
