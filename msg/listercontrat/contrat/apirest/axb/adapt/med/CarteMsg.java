package msg.listercontrat.contrat.apirest.axb.adapt.med;

import java.io.Serializable;

import fr.axb.lib.exchange.action.msg.ActionMsg;
import fr.axb.lib.exchange.retour.msg.CodMsg;

/**
 * 
 * @author naje99k
 *
 */
public class CarteMsg implements IContratMsg, Serializable {

	private static final long serialVersionUID = 854918975759839999L;
	private ProduitMsg produit;
	private String numContrat;
	private String libelle;
	private Double montantDebit;
	private String numCarte;
	private String numComptePassage;
	private CodMsg codTypeCarte;
	private CodMsg  codTypeDebit;
	private CodMsg  codVisuel;
	private String idContrat;
	private ActionMsg[] actions;
	public ProduitMsg getProduit() {
		return produit;
	}
	public void setProduit(ProduitMsg produit) {
		this.produit = produit;
	}
	public String getNumContrat() {
		return numContrat;
	}
	public void setNumContrat(String numContrat) {
		this.numContrat = numContrat;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public Double getMontantDebit() {
		return montantDebit;
	}
	public void setMontantDebit(Double montantDebit) {
		this.montantDebit = montantDebit;
	}
	public String getNumCarte() {
		return numCarte;
	}
	public void setNumCarte(String numCarte) {
		this.numCarte = numCarte;
	}
	public String getNumComptePassage() {
		return numComptePassage;
	}
	public void setNumComptePassage(String numComptePassage) {
		this.numComptePassage = numComptePassage;
	}
	public CodMsg getCodTypeCarte() {
		return codTypeCarte;
	}
	public void setCodTypeCarte(CodMsg codTypeCarte) {
		this.codTypeCarte = codTypeCarte;
	}
	public CodMsg getCodTypeDebit() {
		return codTypeDebit;
	}
	public void setCodTypeDebit(CodMsg codTypeDebit) {
		this.codTypeDebit = codTypeDebit;
	}
	public CodMsg getCodVisuel() {
		return codVisuel;
	}
	public void setCodVisuel(CodMsg codVisuel) {
		this.codVisuel = codVisuel;
	}
	public String getIdContrat() {
		return idContrat;
	}
	public void setIdContrat(String idContrat) {
		this.idContrat = idContrat;
	}
	public ActionMsg[] getActions() {
		return actions;
	}
	public void setActions(ActionMsg[] actions) {
		this.actions = actions;
	}	
}
