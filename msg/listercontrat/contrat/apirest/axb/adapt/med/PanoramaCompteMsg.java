package msg.listercontrat.contrat.apirest.axb.adapt.med;

import java.io.Serializable;

import fr.axb.lib.exchange.retour.msg.RetourMsg;

public class PanoramaCompteMsg implements Serializable {
	
	private static final long serialVersionUID = 854918975759839999L;
	private CompteMsg[] comptes;
	private AssuranceVieMsg[] assurancesvie;
	private CreditMsg[]  credits;
	private CarteMsg[]  cartes;
	private ServiceMsg[] services;
	private Double soldeGlobalClient;
	private RetourMsg retour;
	public CompteMsg[] getComptes() {
		return comptes;
	}
	public void setComptes(CompteMsg[] comptes) {
		this.comptes = comptes;
	}
	public AssuranceVieMsg[] getAssurancesvie() {
		return assurancesvie;
	}
	public void setAssurancesvie(AssuranceVieMsg[] assurancesvie) {
		this.assurancesvie = assurancesvie;
	}
	public CreditMsg[] getCredits() {
		return credits;
	}
	public void setCredits(CreditMsg[] credits) {
		this.credits = credits;
	}
	public CarteMsg[] getCartes() {
		return cartes;
	}
	public void setCartes(CarteMsg[] cartes) {
		this.cartes = cartes;
	}
	public ServiceMsg[] getServices() {
		return services;
	}
	public void setServices(ServiceMsg[] services) {
		this.services = services;
	}	
	public Double getSoldeGlobalClient() {
		return soldeGlobalClient;
	}
	public void setSoldeGlobalClient(Double soldeGlobalClient) {
		this.soldeGlobalClient = soldeGlobalClient;
	}
	public RetourMsg getRetour() {
		return retour;
	}
	public void setRetour(RetourMsg retour) {
		this.retour = retour;
	}
	
}
