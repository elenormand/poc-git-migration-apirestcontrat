package msg.listercontrat.contrat.apirest.axb.adapt.med;

import fr.axb.lib.exchange.action.msg.ActionMsg;

public interface IContratMsg {
	public void setActions(ActionMsg[] actions);
	public String getIdContrat();
	public ProduitMsg getProduit();
}
