package msg.listercontrat.contrat.apirest.axb.adapt.med;

import java.io.Serializable;
import java.util.Date;

import fr.axb.lib.exchange.action.msg.ActionMsg;
import fr.axb.lib.exchange.retour.msg.CodMsg;

/**
 * 
 * @author naje99k
 *
 */
public class CompteMsg implements IContratMsg, Serializable {

	private static final long serialVersionUID = 854918975759839999L;
	private ProduitMsg produit;
	private CodMsg codRole;
	private Date  dteValorisationPortefeuille;
	private String  intitule;
	private CarteMsg[] cartes;
	private Double mntValorisationPortefeuille;
	private String numCompte;
	private String numContrat;
	private Double solde;
	private Double mntTotalEstimePorteFeuille;
	private Integer codCanalMetierCreationOffre;
	private CodMsg codTypeAppartenance;
	private Boolean gestionSousMandatBln;
	private Boolean mntInvestiNulBln;
	private String idContrat;
	private ActionMsg[] actions;
	public ProduitMsg getProduit() {
		return produit;
	}
	public void setProduit(ProduitMsg produit) {
		this.produit = produit;
	}
	public CodMsg getCodRole() {
		return codRole;
	}
	public void setCodRole(CodMsg codRole) {
		this.codRole = codRole;
	}
	public Date getDteValorisationPortefeuille() {
		return dteValorisationPortefeuille;
	}
	public void setDteValorisationPortefeuille(Date dteValorisationPortefeuille) {
		this.dteValorisationPortefeuille = dteValorisationPortefeuille;
	}
	public String getIntitule() {
		return intitule;
	}
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	public CarteMsg[] getCartes() {
		return cartes;
	}
	public void setCartes(CarteMsg[] cartes) {
		this.cartes = cartes;
	}
	public Double getMntValorisationPortefeuille() {
		return mntValorisationPortefeuille;
	}
	public void setMntValorisationPortefeuille(
			Double mntValorisationPortefeuille) {
		this.mntValorisationPortefeuille = mntValorisationPortefeuille;
	}
	public String getNumCompte() {
		return numCompte;
	}
	public void setNumCompte(String numCompte) {
		this.numCompte = numCompte;
	}
	public String getNumContrat() {
		return numContrat;
	}
	public void setNumContrat(String numContrat) {
		this.numContrat = numContrat;
	}
	public Double getSolde() {
		return solde;
	}
	public void setSolde(Double solde) {
		this.solde = solde;
	}
	public Double getMntTotalEstimePortefeuille() {
		return mntTotalEstimePorteFeuille;
	}
	public void setMntTotalEstimePortefeuille(Double mntTotalEstimePorteFeuille) {
		this.mntTotalEstimePorteFeuille = mntTotalEstimePorteFeuille;
	}
	public Integer getCodCanalMetierCreationOffre() {
		return codCanalMetierCreationOffre;
	}
	public void setCodCanalMetierCreationOffre(Integer codCanalMetierCreationOffre) {
		this.codCanalMetierCreationOffre = codCanalMetierCreationOffre;
	}
	public CodMsg getCodTypeAppartenance() {
		return codTypeAppartenance;
	}
	public void setCodTypeAppartenance(CodMsg codTypeAppartenance) {
		this.codTypeAppartenance = codTypeAppartenance;
	}	
	public Boolean getGestionSousMandatBln() {
		return gestionSousMandatBln;
	}
	public void setGestionSousMandatBln(Boolean gestionSousMandatBln) {
		this.gestionSousMandatBln = gestionSousMandatBln;
	}
	public Boolean getMntInvestiNulBln() {
		return mntInvestiNulBln;
	}
	public void setMntInvestiNulBln(Boolean mntInvestiNulBln) {
		this.mntInvestiNulBln = mntInvestiNulBln;
	}
	public String getIdContrat() {
		return idContrat;
	}
	public void setIdContrat(String idContrat) {
		this.idContrat = idContrat;
	}
	public ActionMsg[] getActions() {
		return actions;
	}
	public void setActions(ActionMsg[] actions) {
		this.actions = actions;
	}	
}
