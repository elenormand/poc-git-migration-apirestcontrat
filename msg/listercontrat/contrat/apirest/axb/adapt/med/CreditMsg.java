package msg.listercontrat.contrat.apirest.axb.adapt.med;

import java.io.Serializable;
import java.util.Date;

import fr.axb.lib.exchange.action.msg.ActionMsg;
import fr.axb.lib.exchange.retour.msg.CodMsg;

/**
 * 
 * @author naje99k
 *
 */
public class CreditMsg implements IContratMsg, Serializable {

	private static final long serialVersionUID = 854918975759839999L;
	private ProduitMsg produit;
	private Date dtePositionGestion;
	private String gestionSICLID;
	private String intitule;
	private Double montant;
	private String numDossier;
	private String numContrat;
	private CodMsg codRole;
	private Double capitalRestantDu;
	private String idContrat;
	private ActionMsg[] actions;
	public ProduitMsg getProduit() {
		return produit;
	}
	public void setProduit(ProduitMsg produit) {
		this.produit = produit;
	}
	public Date getDtePositionGestion() {
		return dtePositionGestion;
	}
	public void setDtePositionGestion(Date dtePositionGestion) {
		this.dtePositionGestion = dtePositionGestion;
	}
	public String getGestionSICLID() {
		return gestionSICLID;
	}
	public void setGestionSICLID(String gestionSICLID) {
		this.gestionSICLID = gestionSICLID;
	}
	public String getIntitule() {
		return intitule;
	}
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	public Double getMontant() {
		return montant;
	}
	public void setMontant(Double montant) {
		this.montant = montant;
	}
	public String getNumDossier() {
		return numDossier;
	}
	public void setNumDossier(String numDossier) {
		this.numDossier = numDossier;
	}
	public String getNumContrat() {
		return numContrat;
	}
	public void setNumContrat(String numContrat) {
		this.numContrat = numContrat;
	}
	public CodMsg getCodRole() {
		return codRole;
	}
	public void setCodRole(CodMsg codRole) {
		this.codRole = codRole;
	}
	public Double getCapitalRestantDu() {
		return capitalRestantDu;
	}
	public void setCapitalRestantDu(Double capitalRestantDu) {
		this.capitalRestantDu = capitalRestantDu;
	}
	public String getIdContrat() {
		return idContrat;
	}
	public void setIdContrat(String idContrat) {
		this.idContrat = idContrat;
	}
	public ActionMsg[] getActions() {
		return actions;
	}
	public void setActions(ActionMsg[] actions) {
		this.actions = actions;
	}
}
