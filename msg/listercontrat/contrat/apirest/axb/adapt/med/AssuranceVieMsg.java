package msg.listercontrat.contrat.apirest.axb.adapt.med;

import java.io.Serializable;
import java.util.Date;

import fr.axb.lib.exchange.action.msg.ActionMsg;
import fr.axb.lib.exchange.retour.msg.CodMsg;

public class AssuranceVieMsg implements IContratMsg, Serializable {

	private static final long serialVersionUID = 854918975759839999L;
	private ProduitMsg produit;
	private CodMsg codRole;
	private CodMsg codStatut;
	private String intitule;
	private Double solde;
	private String numContrat;	
	private String numContratAssurance;
	private Date dteSituation;
	private Double mntValeurRachat;
	private String idContrat;
	private ActionMsg[] actions;
	public ProduitMsg getProduit() {
		return produit;
	}
	public void setProduit(ProduitMsg produit) {
		this.produit = produit;
	}
	public CodMsg getCodRole() {
		return codRole;
	}
	public void setCodRole(CodMsg codRole) {
		this.codRole = codRole;
	}
	public CodMsg getCodStatut() {
		return codStatut;
	}
	public void setCodStatut(CodMsg codStatut) {
		this.codStatut = codStatut;
	}
	public String getIntitule() {
		return intitule;
	}
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	public Double getSolde() {
		return solde;
	}
	public void setSolde(Double solde) {
		this.solde = solde;
	}
	public String getNumContrat() {
		return numContrat;
	}
	public void setNumContrat(String numContrat) {
		this.numContrat = numContrat;
	}
	public String getNumContratAssurance() {
		return numContratAssurance;
	}
	public void setNumContratAssurance(String numContratAssurance) {
		this.numContratAssurance = numContratAssurance;
	}
	public Date getDteSituation() {
		return dteSituation;
	}
	public void setDteSituation(Date dteSituation) {
		this.dteSituation = dteSituation;
	}
	public Double getMntValeurRachat() {
		return mntValeurRachat;
	}
	public void setMntValeurRachat(Double mntValeurRachat) {
		this.mntValeurRachat = mntValeurRachat;
	}
	public String getIdContrat() {
		return idContrat;
	}
	public void setIdContrat(String idContrat) {
		this.idContrat = idContrat;
	}
	public ActionMsg[] getActions() {
		return actions;
	}
	public void setActions(ActionMsg[] actions) {
		this.actions = actions;
	}
	
}
