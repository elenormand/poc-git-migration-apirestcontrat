package msg.listerhistmouvement.contrat.apirest.axb.adapt.med;

import java.io.Serializable;
import java.util.Date;

import fr.axb.lib.exchange.action.msg.ActionMsg;
import fr.axb.lib.exchange.retour.msg.CodMsg;

public class MouvementPaginationMsg implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5177544667323406297L;
	
	private String idMouvement; 
	
	private String code;

	private Integer devise;

	private Date dteMouvement;

	private String libelle;

	private Double montant;

	private Integer sens;

	private Double soldeJournalier;

	/*private Integer codCategorie;
	
	private String	libCategorie;*/
	private CodMsg categorie;
	
	private ActionMsg[] actions;

	public MouvementPaginationMsg() {
		super();
	}
	
	public String getIdMouvement() {
		return idMouvement;
	}

	public void setIdMouvement(String idMouvement) {
		this.idMouvement = idMouvement;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getDevise() {
		return devise;
	}

	public void setDevise(Integer devise) {
		this.devise = devise;
	}

	public Date getDteMouvement() {
		return dteMouvement;
	}

	public void setDteMouvement(Date dteMouvement) {
		this.dteMouvement = dteMouvement;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Double getMontant() {
		return montant;
	}

	public void setMontant(Double montant) {
		this.montant = montant;
	}

	public Integer getSens() {
		return sens;
	}

	public void setSens(Integer sens) {
		this.sens = sens;
	}

	public Double getSoldeJournalier() {
		return soldeJournalier;
	}

	public void setSoldeJournalier(Double soldeJournalier) {
		this.soldeJournalier = soldeJournalier;
	}

	public void setCategorie(CodMsg categorie) {
		this.categorie = categorie;
	}

	public CodMsg getCategorie() {
		return categorie;
	}

	public void setActions(ActionMsg[] actions) {
		this.actions = actions;
	}

	public ActionMsg[] getActions() {
		return actions;
	}

}
