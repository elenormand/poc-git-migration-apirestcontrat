package msg.listerhistmouvement.contrat.apirest.axb.adapt.med;

import java.io.Serializable;

public class FiltreMsg implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5177544667323406297L;

	private String rubriqueFiltre;
	
	private Integer conditionFiltre;

	private String valeurFiltre;

	public String getRubriqueFiltre() {
		return rubriqueFiltre;
	}

	public Integer getConditionFiltre() {
		return conditionFiltre;
	}

	public String getValeurFiltre() {
		return valeurFiltre;
	}

	public void setRubriqueFiltre(String rubriqueFiltre) {
		this.rubriqueFiltre = rubriqueFiltre;
	}

	public void setConditionFiltre(Integer conditionFiltre) {
		this.conditionFiltre = conditionFiltre;
	}

	public void setValeurFiltre(String valeurFiltre) {
		this.valeurFiltre = valeurFiltre;
	}

}
