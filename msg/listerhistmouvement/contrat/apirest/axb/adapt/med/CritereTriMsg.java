package msg.listerhistmouvement.contrat.apirest.axb.adapt.med;

import java.io.Serializable;
import java.lang.String;

public class CritereTriMsg implements Serializable {

	private static final long serialVersionUID = 5177544667323406297L;

	private String rubriqueTri;

	private String sensTri;


	public String getRubriqueTri() {
		return this.rubriqueTri;
	}

	public void setRubriqueTri(String rubriqueTri) {
		this.rubriqueTri = rubriqueTri;
	}


	public String getSensTri() {
		return this.sensTri;
	}

	public void setSensTri(String sensTri) {
		this.sensTri = sensTri;
	}

	public String toString() {
		StringBuffer resultat = new StringBuffer(this.getClass().getName());
		resultat.append("\n rubriqueTri : " + this.getRubriqueTri());
		resultat.append("\n sensTri : " + this.getSensTri());
		return resultat.toString();
	}
}
