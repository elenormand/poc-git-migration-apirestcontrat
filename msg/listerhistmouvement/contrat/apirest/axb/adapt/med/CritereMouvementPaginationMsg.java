package msg.listerhistmouvement.contrat.apirest.axb.adapt.med;

import java.io.Serializable;
import java.util.Date;

import fr.axabanque.crm.comptebancaire.api.exceptions.CritereInvalideBusinessException;
import fr.axabanque.framework.exceptions.BusinessException;

/**
 * 
 * @author naje99k
 *
 */
public class CritereMouvementPaginationMsg implements Serializable {

	/**
	 * Empreinte
	 */
	private static final long serialVersionUID = 5177544667323406297L;
	
	/**
	 * Num�ro du compte auquel est attach� l'historique des mouvements
	 */
	private String idContrat;

	/**
	 * Date de d�but de l'intervalle de temps des mouvements enregistr�s
	 */
	private Date dteDebut;

	/**
	 * Date de fin de l'intervalle de temps des mouvements enregistr�s
	 */
	private Date dteFin;

	/**
	 * Crit�re de pagination l'historique des mouvements
	 */
	private CriterePaginationHistoMouvMsg criterePagination;

	/**
	 * Constructeur
	 */
	public CritereMouvementPaginationMsg() {
		super();
	}

	/**
	 * @return Date, la date de d�but de l'intervalle de temps des mouvements enregistr�s
	 */
	public Date getDteDebut() {
		return dteDebut;
	}

	/**
	 * @return Date, la date de fin de l'intervalle de temps des mouvements enregistr�s
	 */
	public Date getDteFin() {
		return dteFin;
	}

	/**
	 * @return String, L'id Client auquel est attach� l'historique des mouvements
	 */
	public String getIdContrat() {
		return idContrat;
	}

	/**
	 * @param Date,
	 *            la date de d�but de l'intervalle de temps des mouvements enregistr�s
	 */
	public void setDteDebut(Date dteDebut) {
		this.dteDebut = dteDebut;
	}

	/**
	 * @param Date,
	 *            la date de fin de l'intervalle de temps des mouvements enregistr�s
	 */
	public void setDteFin(Date dteFin) {
		this.dteFin = dteFin;
	}

	/**
	 * @param String,
	 *            l'id client est attach� l'historique des mouvements
	 */
	public void setIdContrat(String idContrat) {
		this.idContrat = idContrat;
	}


	public CriterePaginationHistoMouvMsg getCriterePagination() {
		return criterePagination;
	}


	public void setCriterePagination(CriterePaginationHistoMouvMsg criterePagination) {
		this.criterePagination = criterePagination;
	}
	public void validerMessage() throws BusinessException {
		this.validerNumCompte();
	}

	public void validerNumCompte() throws BusinessException{
		if(idContrat == null || idContrat.length() == 0){
			throw new CritereInvalideBusinessException(CritereInvalideBusinessException.COMPTEBANCAIRE_NUM_COMPTE_ABSENT);
		}
	}
}
