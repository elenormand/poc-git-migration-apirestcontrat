package msg.listerhistmouvement.contrat.apirest.axb.adapt.med;

import java.io.Serializable;

import fr.axb.lib.exchange.retour.msg.RetourMsg;

public class ListeMouvementMsg implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5177544667323406297L;

	private MouvementPaginationMsg[] mouvementsMsg;
	private RetourPaginationHistoMouvMsg retourPagination;
	private RetourMsg	retour;

	public MouvementPaginationMsg[] getMouvementsMsg() {
		return mouvementsMsg;
	}

	public void setMouvementsMsg(MouvementPaginationMsg[] mouvementsMsg) {
		this.mouvementsMsg = mouvementsMsg;
	}

	public RetourPaginationHistoMouvMsg getRetourPagination() {
		return retourPagination;
	}

	public void setRetourPagination(RetourPaginationHistoMouvMsg retourPagination) {
		this.retourPagination = retourPagination;
	}

	public void setRetour(RetourMsg retour) {
		this.retour = retour;
	}

	public RetourMsg getRetour() {
		return retour;
	}

}
