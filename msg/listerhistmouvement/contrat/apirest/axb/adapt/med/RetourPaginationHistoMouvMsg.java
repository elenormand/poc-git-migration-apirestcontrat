package msg.listerhistmouvement.contrat.apirest.axb.adapt.med;

import java.io.Serializable;

public class RetourPaginationHistoMouvMsg implements Serializable {

	private static final long serialVersionUID = 5177544667323406297L;

	private Integer nbreTotalLigne;

	private Integer nbreTotalPage;

	private Boolean paginationBln;

	private Boolean ctrleTotalLigneBln;

	private Integer numPage;

	private CritereTriMsg[] listeCritereTri;

	public Boolean getCtrleTotalLigneBln() {
		return ctrleTotalLigneBln;
	}

	public void setCtrleTotalLigneBln(Boolean ctrleTotalLigneBln) {
		this.ctrleTotalLigneBln = ctrleTotalLigneBln;
	}

	public CritereTriMsg[] getListeCritereTri() {
		return listeCritereTri;
	}

	public void setListeCritereTri(CritereTriMsg[] listeCritereTri) {
		this.listeCritereTri = listeCritereTri;
	}

	public Integer getNbreTotalLigne() {
		return nbreTotalLigne;
	}

	public void setNbreTotalLigne(Integer nbreTotalLigne) {
		this.nbreTotalLigne = nbreTotalLigne;
	}

	public Integer getNbreTotalPage() {
		return nbreTotalPage;
	}

	public void setNbreTotalPage(Integer nbreTotalPage) {
		this.nbreTotalPage = nbreTotalPage;
	}

	public Integer getNumPage() {
		return numPage;
	}

	public void setNumPage(Integer numPage) {
		this.numPage = numPage;
	}

	public Boolean getPaginationBln() {
		return paginationBln;
	}

	public void setPaginationBln(Boolean paginationBln) {
		this.paginationBln = paginationBln;
	}

}
