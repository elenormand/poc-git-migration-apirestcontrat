package msg.listerhistmouvement.contrat.apirest.axb.adapt.med;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;

public class RetourPaginationMsg implements Serializable {

	private static final long serialVersionUID = 5177544667323406297L;

	private Integer nbreTotalLigne;

	private Integer nbreTotalPage;

	private Boolean blnPagination;

	private Boolean blnCtrleTotalLigne;

	private Integer numPage;

	private CritereTriMsg[] critereTri;

	public Integer getNbreTotalLigne() {
		return this.nbreTotalLigne;
	}

	public void setNbreTotalLigne(Integer nbreTotalLigne) {
		this.nbreTotalLigne = nbreTotalLigne;
	}

	public Integer getNbreTotalPage() {
		return this.nbreTotalPage;
	}

	public void setNbreTotalPage(Integer nbreTotalPage) {
		this.nbreTotalPage = nbreTotalPage;
	}

	public Boolean getBlnPagination() {
		return this.blnPagination;
	}

	public void setBlnPagination(Boolean blnPagination) {
		this.blnPagination = blnPagination;
	}

	public Boolean getBlnCtrleTotalLigne() {
		return this.blnCtrleTotalLigne;
	}

	public void setBlnCtrleTotalLigne(Boolean blnCtrleTotalLigne) {
		this.blnCtrleTotalLigne = blnCtrleTotalLigne;
	}

	public Integer getNumPage() {
		return this.numPage;
	}

	public void setNumPage(Integer numPage) {
		this.numPage = numPage;
	}

	public CritereTriMsg[] getCritereTri() {
		return this.critereTri;
	}

	public void setCritereTri(CritereTriMsg[] critereTri) {
		this.critereTri = critereTri;
	}

	public String toString() {
		StringBuffer resultat = new StringBuffer(this.getClass().getName());
		resultat.append("\n nbreTotalLigne : " + this.getNbreTotalLigne());
		resultat.append("\n nbreTotalPage : " + this.getNbreTotalPage());
		resultat.append("\n blnPagination : " + this.getBlnPagination());
		resultat.append("\n blnCtrleTotalLigne : " + this.getBlnCtrleTotalLigne());
		resultat.append("\n numPage : " + this.getNumPage());
		resultat.append("\n critereTri[] : ");

		if (this.getCritereTri() != null) {
			for (int i = 0; i < this.getCritereTri().length; i++) {
				resultat.append("\n  N�" + i + " : " + this.getCritereTri()[i]);
			}
		}
		else {
			resultat.append(this.getCritereTri());
		}

		return resultat.toString();
	}
}
