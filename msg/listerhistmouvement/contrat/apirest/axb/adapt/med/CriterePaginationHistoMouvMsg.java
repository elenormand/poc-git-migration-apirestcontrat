package msg.listerhistmouvement.contrat.apirest.axb.adapt.med;

import java.io.Serializable;

public class CriterePaginationHistoMouvMsg implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5177544667323406297L;

	private Boolean paginationBln;

	private Boolean ctrleTotalLigneBln;

	private Integer numPage;

	private Integer nbreLignePage;

	private FiltreMsg[] filtre;

	private CritereTriMsg[] critereTri;

	public Boolean getPaginationBln() {
		return paginationBln;
	}

	public Boolean getCtrleTotalLigneBln() {
		return ctrleTotalLigneBln;
	}

	public Integer getNumPage() {
		return numPage;
	}

	public Integer getNbreLignePage() {
		return nbreLignePage;
	}

	public FiltreMsg[] getFiltre() {
		return filtre;
	}

	public CritereTriMsg[] getCritereTri() {
		return critereTri;
	}

	public void setPaginationBln(Boolean paginationBln) {
		this.paginationBln = paginationBln;
	}

	public void setCtrleTotalLigneBln(Boolean ctrleTotalLigneBln) {
		this.ctrleTotalLigneBln = ctrleTotalLigneBln;
	}

	public void setNumPage(Integer numPage) {
		this.numPage = numPage;
	}

	public void setNbreLignePage(Integer nbreLignePage) {
		this.nbreLignePage = nbreLignePage;
	}

	public void setFiltre(FiltreMsg[] filtre) {
		this.filtre = filtre;
	}

	public void setCritereTri(CritereTriMsg[] critereTri) {
		this.critereTri = critereTri;
	}

}
