package historiquemvt.msg.util;

import fr.axabanque.common.parametrage.api.msg.CritereDictionaryMsg;
import fr.axabanque.common.parametrage.api.msg.DictionaryRefMsg;
import fr.axabanque.common.parametrage.api.msg.DictionaryRefValueMsg;
import fr.axabanque.common.parametrage.bc.ejb.interfaces.ParametrageBusinessComponentEJB;
import fr.axabanque.crm.comptebancaire.api.msg.CritereMouvementPaginationMsg;
import fr.axabanque.crm.comptebancaire.api.msg.CriterePaginationHistoMouvMsg;
import fr.axabanque.framework.exceptions.BusinessException;
import fr.axabanque.framework.exceptions.TechnicalException;
import fr.axb.lib.apirest.ConverterFactory;
import fr.axb.lib.apirest.IdRest;
import fr.axb.lib.apirest.msg.IdContrat;
import fr.axb.lib.apirest.msg.IdMouvement;
import fr.axb.lib.exchange.retour.msg.CodMsg;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import msg.listerhistmouvement.contrat.apirest.axb.adapt.med.CritereTriMsg;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author naje99k
 * @Projet : G�n�ralisation Service REST: ListerHistoriqueMouvement
 */
public class HistoMouvementUtil {
	// Logguer
	private static Log log = LogFactory.getLog(HistoMouvementUtil.class);
	// Nombre de ligne maximal pa page
	public static int nbreLigneMax = 50;
	
	@SuppressWarnings("deprecation")
	private static void genereBusinessException(Integer codeRetour) throws BusinessException {
		// RDG-PRES-LHM-05
		BusinessException businessException = new BusinessException();
		businessException.setErrorCode(codeRetour);
		throw businessException;
	}
	
	/**
	 * Utilitaire de conversion des messages SP vers des messages SM 
	 * @param critereMvtPaginationMsgSP
	 * @param idRest 
	 * @return
	 * @throws BusinessException
	 */
	public static CritereMouvementPaginationMsg adaptMsgJavaSPToMsgSM(msg.listerhistmouvement.contrat.apirest.axb.adapt.med.CritereMouvementPaginationMsg critereMvtPaginationMsgSP, IdRest idRest) throws BusinessException {

		// RDG-PRES-LHM-01
		if (critereMvtPaginationMsgSP.getIdContrat() == null || critereMvtPaginationMsgSP.getIdContrat().equals(""))
			genereBusinessException(60);

		// RDG-PRES-LHM-02
		if (critereMvtPaginationMsgSP.getDteDebut() != null && critereMvtPaginationMsgSP.getDteFin() != null
				&& critereMvtPaginationMsgSP.getDteDebut().after(critereMvtPaginationMsgSP.getDteFin()))
			genereBusinessException(70);

		CritereMouvementPaginationMsg cMvtPagMsg = new CritereMouvementPaginationMsg();
		CriterePaginationHistoMouvMsg cPagHistoMvtSMMsg;
		List<fr.axabanque.crm.comptebancaire.api.msg.FiltreMsg> listFiltreMsg;
		fr.axabanque.crm.comptebancaire.api.msg.FiltreMsg filtreMsg;
		List<fr.axabanque.crm.comptebancaire.api.msg.CritereTriMsg> listCriTriMsg;
		fr.axabanque.crm.comptebancaire.api.msg.CritereTriMsg cTriMsg;

		if (critereMvtPaginationMsgSP.getCriterePagination() != null) {
			cPagHistoMvtSMMsg = new CriterePaginationHistoMouvMsg();
			Boolean paginationBln = critereMvtPaginationMsgSP.getCriterePagination().getPaginationBln();
			msg.listerhistmouvement.contrat.apirest.axb.adapt.med.FiltreMsg[] filtreMsgs = critereMvtPaginationMsgSP.getCriterePagination().getFiltre();
			msg.listerhistmouvement.contrat.apirest.axb.adapt.med.CritereTriMsg[] cTriMsgs = critereMvtPaginationMsgSP.getCriterePagination().getCritereTri();

			// NB: un entier est toujours = 0, jamais null sous ESB
			// LHM-04 : Gestion NbreLignePage
			Integer nbreLignePage = critereMvtPaginationMsgSP.getCriterePagination().getNbreLignePage();
			if ((paginationBln != null && paginationBln.booleanValue()) && (nbreLignePage == null || nbreLignePage.intValue() == 0)) {
				genereBusinessException(20);
			}
			else if ((paginationBln != null && paginationBln.booleanValue()) && (nbreLignePage.intValue() > nbreLigneMax)) {
				genereBusinessException(30);
			}
			cPagHistoMvtSMMsg.setNbreLignePage(nbreLignePage);

			// LHM-04 : Gestion de NumPage
			Integer numPage = critereMvtPaginationMsgSP.getCriterePagination().getNumPage();
			if ((paginationBln != null && paginationBln.booleanValue()) && (numPage == null || numPage.intValue() == 0))
				genereBusinessException(10);

			cPagHistoMvtSMMsg.setNumPage(numPage);

			cPagHistoMvtSMMsg.setCtrleTotalLigneBln(critereMvtPaginationMsgSP.getCriterePagination().getCtrleTotalLigneBln());

			cPagHistoMvtSMMsg.setPaginationBln(paginationBln);

			// Gestion des filtres
			if ((filtreMsgs != null) && (filtreMsgs.length > 0)) {
				listFiltreMsg = new ArrayList<fr.axabanque.crm.comptebancaire.api.msg.FiltreMsg>();
				for (msg.listerhistmouvement.contrat.apirest.axb.adapt.med.FiltreMsg elt : filtreMsgs) {
					filtreMsg = new fr.axabanque.crm.comptebancaire.api.msg.FiltreMsg();
					filtreMsg.setRubriqueFiltre(elt.getRubriqueFiltre());

					filtreMsg.setConditionFiltre(elt.getConditionFiltre());
					if (elt.getConditionFiltre() == null || elt.getConditionFiltre().intValue() == 0)
						filtreMsg.setConditionFiltre(null);

					filtreMsg.setValeurFiltre(elt.getValeurFiltre());

					listFiltreMsg.add(filtreMsg);
				}

				cPagHistoMvtSMMsg.setFiltre(listFiltreMsg.toArray(new fr.axabanque.crm.comptebancaire.api.msg.FiltreMsg[listFiltreMsg.size()]));
			}

			// LHM-04 : Gestion Critere Tri
			/*if ((paginationBln != null && paginationBln.booleanValue()) && (cTriMsgs[0] == null || cTriMsgs[0].getRubriqueTri() == null
					|| cTriMsgs[0].getRubriqueTri().equals("")
					|| cTriMsgs[0].getSensTri() == null || cTriMsgs[0].getSensTri().equals(""))) {
				genereBusinessException(40);
			}*/

			// Gestion des CritereTri
			if ((cTriMsgs != null) && (cTriMsgs.length > 0)) {
				listCriTriMsg = new ArrayList<fr.axabanque.crm.comptebancaire.api.msg.CritereTriMsg>();
				for (msg.listerhistmouvement.contrat.apirest.axb.adapt.med.CritereTriMsg elt : cTriMsgs) {
					cTriMsg = new fr.axabanque.crm.comptebancaire.api.msg.CritereTriMsg();
					cTriMsg.setRubriqueTri(elt.getRubriqueTri());
					cTriMsg.setSensTri(elt.getSensTri());

					listCriTriMsg.add(cTriMsg);
				}

				cPagHistoMvtSMMsg.setCritereTri(listCriTriMsg.toArray(new fr.axabanque.crm.comptebancaire.api.msg.CritereTriMsg[listCriTriMsg.size()]));
			}
			cMvtPagMsg.setCriterePagination(cPagHistoMvtSMMsg);
		}
		cMvtPagMsg.setNumCompte(((IdContrat)idRest).getNumReference()); // TODO : Il FAUT UTILISER UN NUMERO DE COMPTE ????? numReference Peut �tre BON
		cMvtPagMsg.setDteDebut(critereMvtPaginationMsgSP.getDteDebut());
		cMvtPagMsg.setDteFin(critereMvtPaginationMsgSP.getDteFin());
		//cMvtPagMsg.setCriterePagination(critereMvtPaginationMsgSP.getCriterePagination()); // TODO : Transformation des crit�res
		return cMvtPagMsg;
	}
	
	/**
	 * 
	 * @param listMvtMsgSM
	 * @param codProduit, 
	 * @return
	 */
	public static msg.listerhistmouvement.contrat.apirest.axb.adapt.med.ListeMouvementMsg adaptMsgJavaSMToMsgSP(
			fr.axabanque.crm.comptebancaire.api.msg.ListeMouvementMsg listMvtMsgSM,
			String codProduit, 
			ParametrageBusinessComponentEJB parametrageBusinessComponent) {	
		msg.listerhistmouvement.contrat.apirest.axb.adapt.med.ListeMouvementMsg listMvtMsgSP = new msg.listerhistmouvement.contrat.apirest.axb.adapt.med.ListeMouvementMsg();
		try {
			String nomDictionnaire = ((codProduit != null) && (codProduit.equals("44")))?"CodCatMvtCptePMO":"CodCatMvtCpte";
			CritereDictionaryMsg critereDictionnaireRole = getDictionnaire(nomDictionnaire);
			DictionaryRefMsg dictionnaireRole  = parametrageBusinessComponent.lireDicoRef(critereDictionnaireRole);
			Map<Integer, CodMsg> categorieMap = parseToDictionnaireMapAsObject(dictionnaireRole);
			listMvtMsgSP.setMouvementsMsg(convertSMMsgToSPMsg(listMvtMsgSM.getListeMouvement(), categorieMap));
			listMvtMsgSP.setRetourPagination(convertSMMsgToSPMsg(listMvtMsgSM.getRetourPagination()));
		} catch (RemoteException remoteException) {
			log.error("Erreur:HistoMouvementUtil:RemoteException" + remoteException.getMessage());
		} catch (BusinessException businessException) {
			log.error("Erreur:HistoMouvementUtil:RemoteException" + businessException.getMessage());
		} catch (TechnicalException technicalException) {
			log.error("Erreur:HistoMouvementUtil:RemoteException" + technicalException.getMessage());
		}
		return listMvtMsgSP;
	}
	
	/**
	 * Permet de recuperer un dictionnaire de reference  
	 * @author 	Issam CHEBBI
	 * @param 	String -  nom de reference du dictionnaire
	 * @return	CritereDictionaryMsg - critere du dictionnaire 
	 */
	public static CritereDictionaryMsg getDictionnaire(String refDictionnaire) {
		CritereDictionaryMsg critereDictionnaire	=	new CritereDictionaryMsg();
		critereDictionnaire.setDicoClassName(refDictionnaire);
		critereDictionnaire.setGroupElement("1");
		return critereDictionnaire;
	}
	
	/**
	 * Permet de construire un map de dictionnaire  
	 * @author 	Issam CHEBBI
	 * @param 	DictionaryRefMsg -  reference du dictionnaire
	 * @return	Map<Integer, CodMsg> - map des valeurs du dictionnaire 
	 */
	private static Map<Integer, CodMsg> parseToDictionnaireMapAsObject(
			DictionaryRefMsg dictionnaire) {
		Map<Integer, CodMsg> dictionnaireMap	=	null;
		if(dictionnaire != null){
			if((dictionnaire.getDicoValue() != null) && (dictionnaire.getDicoValue().length > 0)){
				dictionnaireMap	=	new HashMap<Integer, CodMsg>();
				DictionaryRefValueMsg[] listeValeurs = dictionnaire.getDicoValue();
				CodMsg libelle = null;
				for(DictionaryRefValueMsg valeur : listeValeurs){
					libelle = new CodMsg(valeur.getIntCodeElement().intValue(), valeur.getLibElement());
					dictionnaireMap.put(valeur.getIntCodeElement().intValue(), libelle);
				}
			}
		}
		return dictionnaireMap;
	}
	
	/**
	 * Convertit un tableau de messages SO vers un tableau de messages SP
	 * @param mvtPageMsgs Tableau de MouvementPaginationMsg ( SO )
	 * @param categorieMap 
	 * @return Tableau de MouvementPaginationMsg ( SP )
	 */
	public static msg.listerhistmouvement.contrat.apirest.axb.adapt.med.MouvementPaginationMsg[] convertSMMsgToSPMsg(
			fr.axabanque.crm.comptebancaire.api.msg.MouvementPaginationMsg[] mvtPageMsgs, 
			Map<Integer, CodMsg> categorieMap){
		msg.listerhistmouvement.contrat.apirest.axb.adapt.med.MouvementPaginationMsg[] spMsgs = null;
		if (mvtPageMsgs != null){
			spMsgs = new msg.listerhistmouvement.contrat.apirest.axb.adapt.med.MouvementPaginationMsg[mvtPageMsgs.length];
			for (int i = 0; i < mvtPageMsgs.length; i++) {
				msg.listerhistmouvement.contrat.apirest.axb.adapt.med.MouvementPaginationMsg spMsg = new msg.listerhistmouvement.contrat.apirest.axb.adapt.med.MouvementPaginationMsg();
				IdRest idMouvementStruct = new IdMouvement("", "", "");
				String idMouvement = ConverterFactory.getInstance().convertStructToId(idMouvementStruct);
				spMsg.setIdMouvement(idMouvement);
				spMsg.setCode(mvtPageMsgs[i].getCode());
				spMsg.setMontant(mvtPageMsgs[i].getMontant());
				spMsg.setDevise(mvtPageMsgs[i].getDevise());
				if(mvtPageMsgs[i].getCodCategorie() != null){
					spMsg.setCategorie(categorieMap.get(mvtPageMsgs[i].getCodCategorie()));
				}	
				spMsg.setDteMouvement(mvtPageMsgs[i].getDteMouvement());
				spMsg.setLibelle(mvtPageMsgs[i].getLibelle());
				spMsg.setSoldeJournalier(mvtPageMsgs[i].getSoldeJournalier());
				spMsg.setSens(mvtPageMsgs[i].getSens());
				spMsg.setActions(null);
				spMsgs[i] = spMsg;
			}			
		}
		return spMsgs;
	}
	
	/**
	 * Convertit un tableau de messages SO vers un tableau de messages SP
	 * @param mvtPageMsgs Tableau de RetourPaginationHistoMouvMsg ( SO )
	 * @return Tableau de RetourPaginationHistoMouvMsg ( SP )
	 */
	public static msg.listerhistmouvement.contrat.apirest.axb.adapt.med.RetourPaginationHistoMouvMsg convertSMMsgToSPMsg(fr.axabanque.crm.comptebancaire.api.msg.RetourPaginationHistoMouvMsg retourPaginationSMMsg){
		msg.listerhistmouvement.contrat.apirest.axb.adapt.med.RetourPaginationHistoMouvMsg retourPageSPMsg = null;
		if (retourPaginationSMMsg != null){
			retourPageSPMsg = new msg.listerhistmouvement.contrat.apirest.axb.adapt.med.RetourPaginationHistoMouvMsg();
			retourPageSPMsg.setNbreTotalLigne(retourPaginationSMMsg.getNbreTotalLigne());
			retourPageSPMsg.setNbreTotalPage(retourPaginationSMMsg.getNbreTotalPage());
			retourPageSPMsg.setPaginationBln(retourPaginationSMMsg.getPaginationBln());
			retourPageSPMsg.setCtrleTotalLigneBln(retourPaginationSMMsg.getCtrleTotalLigneBln());
			retourPageSPMsg.setNumPage(retourPaginationSMMsg.getNumPage());
			retourPageSPMsg.setListeCritereTri(convertSMMsgToSPMsg(retourPaginationSMMsg.getListeCritereTri()));
		}
		return retourPageSPMsg;
	}
	
	/**
	 * Convertit un tableau de messages SO vers un tableau de messages SP
	 * @param mvtPageMsgs Tableau de CritereTriMsg ( SO )
	 * @return Tableau de CritereTriMsg ( SP )
	 */
	public static CritereTriMsg[] convertSMMsgToSPMsg(fr.axabanque.crm.comptebancaire.api.msg.CritereTriMsg[] listeCritereTri) {
		msg.listerhistmouvement.contrat.apirest.axb.adapt.med.CritereTriMsg[] spMsgs = null;
		if (listeCritereTri != null){
			spMsgs = new msg.listerhistmouvement.contrat.apirest.axb.adapt.med.CritereTriMsg[listeCritereTri.length];
			for (int i = 0; i < listeCritereTri.length; i++) {
				msg.listerhistmouvement.contrat.apirest.axb.adapt.med.CritereTriMsg spMsg = new msg.listerhistmouvement.contrat.apirest.axb.adapt.med.CritereTriMsg();
				spMsg.setRubriqueTri(listeCritereTri[i].getRubriqueTri());
				spMsg.setSensTri(listeCritereTri[i].getSensTri());
				spMsgs[i] = spMsg;
			}			
		}
		return spMsgs;
	}

}
