package listercontrat.msg.util;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import msg.detailbancaire.contrat.apirest.axb.adapt.med.CashBackAssuranceMsg;
import msg.detailbancaire.contrat.apirest.axb.adapt.med.CumulAchatCarteMsg;
import msg.detailbancaire.contrat.apirest.axb.adapt.med.DetailsCarteMsg;
import msg.detailbancaire.contrat.apirest.axb.adapt.med.DetailsCompteMsg;
import msg.detailbancaire.contrat.apirest.axb.adapt.med.DetailsContratBancaireMsg;
import msg.detailbancaire.contrat.apirest.axb.adapt.med.DetailsEpargneMsg;
import msg.detailbancaire.contrat.apirest.axb.adapt.med.DroitsCELMsg;
import msg.detailbancaire.contrat.apirest.axb.adapt.med.InformationsFiscalesMsg;
import msg.detailbancaire.contrat.apirest.axb.adapt.med.InformationsPELMsg;
import msg.detailbancaire.contrat.apirest.axb.adapt.med.RIBMsg;
import msg.detailbancaire.contrat.apirest.axb.adapt.med.ValeurMsg;
import msg.detailbancaire.contrat.apirest.axb.adapt.med.VersementProgrammeMsg;
import msg.detailbancaire.contrat.apirest.axb.adapt.med.VersementsAnnuelsMsg;
import msg.detailcredit.contrat.apirest.axb.adapt.med.EmprunteurMsg;
import msg.detailcredit.contrat.apirest.axb.adapt.med.LireDetailCreditMsg;
import msg.listercontrat.contrat.apirest.axb.adapt.med.AssuranceVieMsg;
import msg.listercontrat.contrat.apirest.axb.adapt.med.CarteMsg;
import msg.listercontrat.contrat.apirest.axb.adapt.med.CreditMsg;
import msg.listercontrat.contrat.apirest.axb.adapt.med.ProduitMsg;
import msg.listercontrat.contrat.apirest.axb.adapt.med.ServiceMsg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.axabanque.common.parametrage.api.msg.CritereDictionaryMsg;
import fr.axabanque.common.parametrage.api.msg.DictionaryRefMsg;
import fr.axabanque.common.parametrage.api.msg.DictionaryRefValueMsg;
import fr.axabanque.common.parametrage.bc.ejb.interfaces.ParametrageBusinessComponentEJB;
import fr.axabanque.crm.comptebancaire.api.msg.CompteBancaireMsg;
import fr.axabanque.crm.comptebancaire.api.msg.CompteurCarteMsg;
import fr.axabanque.crm.comptebancaire.api.msg.CompteurRetraitCarteMsg;
import fr.axabanque.crm.comptebancaire.api.msg.CumulsPELMsg;
import fr.axabanque.crm.comptebancaire.api.msg.DetailsCELMsg;
import fr.axabanque.crm.comptebancaire.api.msg.DetailsCSLMsg;
import fr.axabanque.crm.comptebancaire.api.msg.DetailsCompteDepotATermeMsg;
import fr.axabanque.crm.comptebancaire.api.msg.DetailsCompteTitreMsg;
import fr.axabanque.crm.comptebancaire.api.msg.DetailsLDDMsg;
import fr.axabanque.crm.comptebancaire.api.msg.DetailsPELMsg;
import fr.axabanque.crm.comptebancaire.api.msg.DonneesCarteMsg;
import fr.axabanque.crm.comptebancaire.api.msg.DroitsAcquisCELMsg;
import fr.axabanque.crm.comptebancaire.api.msg.InformationsFiscalesCompteTitreMsg;
import fr.axabanque.crm.comptebancaire.api.msg.InformationsFiscalesPEAMsg;
import fr.axabanque.crm.comptebancaire.api.msg.PlafondCarteMsg;
import fr.axabanque.crm.comptebancaire.api.msg.ProduitPELMsg;
import fr.axabanque.crm.comptebancaire.api.msg.VersementCELMsg;
import fr.axabanque.crm.comptebancaire.api.msg.VersementCpteACpteMsg;
import fr.axabanque.crm.comptebancaire.api.msg.VersementsAnnuelsPELMsg;
import fr.axabanque.crm.contratcredit.api.msg.ContratCreditMsg;
import fr.axabanque.crm.contratcredit.api.msg.ContratCreditPersonneMsg;
import fr.axabanque.crm.contratcredit.api.msg.InfoPretSpecialiseMsg;
import fr.axabanque.crm.contratcredit.api.msg.PretSpecialiseMsg;
import fr.axabanque.crm.credit.api.msg.InfoPartenaireCreditMsg;
import fr.axabanque.crm.credit.api.msg.ListeInfoPartenaireCreditMsg;
import fr.axabanque.crm.titre.api.msg.StockMsg;
import fr.axabanque.framework.exceptions.BusinessException;
import fr.axabanque.framework.exceptions.TechnicalException;
import fr.axabanque.med.axb.apirest.common.soap.parser.HeaderSoap;
import fr.axabanque.pilotage.axb.apirest.commun.api.constants.CodeTypeRole;
import fr.axabanque.pilotage.axb.apirest.commun.api.contrat.msg.CritereInfoMsg;
import fr.axabanque.pilotage.axb.apirest.commun.api.contrat.msg.CritereListerActionContratMsg;
import fr.axabanque.pilotage.axb.apirest.commun.api.contrat.msg.ListeCritereListerActionContratMsg;
import fr.axabanque.pilotage.axb.apirest.commun.ejb.interfaces.ListerActionEJB;
import fr.axabanque.pilotage.axb.apirest.contrat.api.msg.CritereDetailCreditMsg;
import fr.axabanque.pilotage.axb.apirest.contrat.api.msg.CritereListerContratMsg;
import fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.api.msg.CashBackCompteMsg;
import fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.api.msg.CritereContratBancaireMsg;
import fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.api.msg.DetailContratBancaireMsg;
import fr.axb.lib.apirest.BasicConverterImpl;
import fr.axb.lib.apirest.Converter;
import fr.axb.lib.apirest.ConverterFactory;
import fr.axb.lib.apirest.IdRest;
import fr.axb.lib.apirest.msg.IdClient;
import fr.axb.lib.apirest.msg.IdContrat;
import fr.axb.lib.exchange.action.msg.ActionMsg;
import fr.axb.lib.exchange.action.msg.ListeActionsMsg;
import fr.axb.lib.exchange.exception.RestBusinessException;
import fr.axb.lib.exchange.retour.msg.CodMsg;

/**
 * @author naje99k
 * 
 * @Projet : G�n�ralisation Service REST: Contrat
 */
public class ContratUtil {
	// LOGGER
	// private static Log LOGGER = LogFactory.getLog(ContratUtil.class);
	private static Logger LOGGER = LoggerFactory.getLogger(ContratUtil.class);
	//
	protected final static String REF_TYPE_ROLE_CREDIT_DESC = "RefTypeRoleCreditDesc";
	protected final static String REF_TYPE_PAYS_IBAN_DESC = "RefPaysIBANDesc";
	
	protected final static String REF_TYPE_ROLE_DESC = "RefTypeRoleDesc";

	protected final static String REF_TYPE_PRODUIT_DESC = "RefTypeProduitDesc";
	protected final static String REF_TYPE_SOUS_PRODUIT_ASS_VIE_DESC = "RefTypeSousProduitAssVieDesc";
	protected final static String REF_TYPE_SOUS_PRODUIT_CARTE_DESC = "RefTypeSousProduitCarteDesc";
	protected final static String REF_TYPE_SOUS_PRODUIT_CDD_DESC = "RefTypeSousProduitCddDesc";
	protected final static String REF_TYPE_SOUS_PRODUIT_CDD_TITRE_DESC = "RefTypeSousProduitCddTitreDesc";
	protected final static String REF_TYPE_SOUS_PRODUIT_DAT_DESC = "RefTypeSousProduitDATDesc";
	protected final static String REF_TYPE_SOUS_PRODUIT_DEC_DESC = "RefTypeSousProduitDecDesc";
	protected final static String REF_TYPE_SOUS_PRODUIT_LIVRET_DESC = "RefTypeSousProduitLivretDesc";
	protected final static String REF_TYPE_SOUS_PRODUIT_PMO_CDD_DESC = "RefTypeSousProduitPMOCddDesc";
	protected final static String REF_TYPE_SOUS_PRODUIT_PMO_TITRE = "RefTypeSousProduitPMOTitreDesc";
	protected final static String REF_TYPE_SOUS_PRODUIT_PP_DESC = "RefTypeSousProduitPPDesc";
	protected final static String REF_TYPE_SOUS_PRODUIT_PRET_SPEC_DESC = "RefTypeSousProduitPretSpecDesc";
	protected final static String REF_TYPE_SOUS_PRODUIT_RA_DESC = "RefTypeSousProduitRADesc";
	protected final static String REF_FAMILLE_DESC = "RefFamilleDesc";
	protected final static String REF_TYPE_PACKAGE_DESC = "RefTypePackageDesc";
	protected final static String REF_TYPE_DEBIT_DESC = "RefTypeDebitDesc";
	protected final static String REF_TYPE_CARTE_DESC = "RefTypeCarteWebDesc";
	protected final static String REF_TYPE_APPARTENANCE_DESC = "RefTypeAppartenanceDesc";

	private static Map<Integer, CodMsg> TYPEPRODUITMAP = null;
	private static Map<Integer, CodMsg> SOUSPRODUITASSVIEMAP = null;
	private static Map<Integer, CodMsg> SOUSPRODUITCARTEMAP = null;
	private static Map<Integer, CodMsg> SOUSPRODUITCDDMAP = null;
	private static Map<Integer, CodMsg> SOUSPRODUITCDDTITREMAP = null;
	private static Map<Integer, CodMsg> SOUSPRODUITDATMAP = null;
	private static Map<Integer, CodMsg> SOUSPRODUITDECMAP = null;
	private static Map<Integer, CodMsg> SOUSPRODUITLIVRETMAP = null;
	private static Map<Integer, CodMsg> SOUSPRODUITPMOCDDMAP = null;
	private static Map<Integer, CodMsg> SOUSPRODUITPMOTITREMAP = null;
	private static Map<Integer, CodMsg> SOUSPRODUITPPMAP = null;
	private static Map<Integer, CodMsg> SOUSPRODUITPRETSPECMAP = null;
	private static Map<Integer, CodMsg> SOUSPRODUITRAMAP = null;
	private static Map<Integer, CodMsg> FAMILLEMAP = null;
	private static Map<Integer, CodMsg> PACKAGEMAP = null;
	private static Map<Integer, CodMsg> DEBITMAP = null;
	private static Map<Integer, CodMsg> CARTEMAP = null;
	private static Map<Integer, CodMsg> APPARTENANCEMAP = null;
	
	private static Map<Integer, CodMsg> TYPEROLEMAP = null;

	// Les diff�rents codes de produit
	private final static Integer COD_PRODUIT_41 = new Integer(41);
	private final static Integer COD_PRODUIT_46 = new Integer(46);
	private final static Integer COD_PRODUIT_50 = new Integer(50);
	private final static Integer COD_PRODUIT_51 = new Integer(51);
	// MASQUE
	private final static String MASQUE = "xxxx xxxx xxxx ";
	// Type Client
	private static final Integer TYPECLIENT_PHYSIQUE = 1;

	@SuppressWarnings("deprecation")
	private static void genereBusinessException(Integer codeRetour) throws BusinessException {
		// RDG-PRES-LHM-05
		BusinessException businessException = new BusinessException();
		businessException.setErrorCode(codeRetour);
		throw businessException;
	}

	// l'id Rest
	private static Converter CONVERTER = new BasicConverterImpl();
	private static String RESTRICTION_TITRE = "titre";
	private static final String RESTRICTION_PEA = "pea";
	private static Integer VALUE_INT_0 = new Integer(0);
	private static Integer VALUE_INT_1 = new Integer(1);

	/**
	 * Utilitaire de conversion des messages SP vers des messages SM
	 * 
	 * @param critereListerContratMsg
	 * @param idRest
	 * @param header
	 * @return
	 * @throws BusinessException
	 */
	public static CritereListerContratMsg adaptMsgJavaSPToMsgSM(
			msg.listercontrat.contrat.apirest.axb.adapt.med.CritereListerContratMsg critereListerContratMsgSP, IdRest idRest, HeaderSoap header)
			throws BusinessException {
		// RDG-PRES-LHM-01
		if (critereListerContratMsgSP.getIdClient() == null || critereListerContratMsgSP.getIdClient().equals(""))
			genereBusinessException(60);
		CritereListerContratMsg cListerContratMsg = new CritereListerContratMsg();
		cListerContratMsg.setNumClient(((IdClient) idRest).getNumClient());
		cListerContratMsg.setNumUtilisateur(header.getIdUtilisateur());
		cListerContratMsg.setCodTypeRole(critereListerContratMsgSP.getCodTypeRole());
		cListerContratMsg.setTypeClient(critereListerContratMsgSP.getTypeClient());
		return cListerContratMsg;
	}

	/**
	 * Convertit un tableau de messages SO vers un tableau de messages SP
	 * 
	 * @param idRest
	 * 
	 * @param listMvtMsgSM
	 * @return
	 */
	public static msg.listercontrat.contrat.apirest.axb.adapt.med.PanoramaCompteMsg adaptMsgJavaSMToMsgSP(
			fr.axabanque.pilotage.axb.apirest.contrat.api.msg.PanoramaCompteMsg panoramaCompteMsgSM,
			ParametrageBusinessComponentEJB parametrageBusinessComponentEJB) {

		if (parametrageBusinessComponentEJB == null)
			LOGGER.error("parametrageBusinessComponentEJB null");
		msg.listercontrat.contrat.apirest.axb.adapt.med.PanoramaCompteMsg panoramaCompteMsgSP = new msg.listercontrat.contrat.apirest.axb.adapt.med.PanoramaCompteMsg();
		panoramaCompteMsgSP.setSoldeGlobalClient(panoramaCompteMsgSM.getSoldeGlobalClient());
		panoramaCompteMsgSP.setComptes(convertSMMsgToSPMsg(panoramaCompteMsgSM.getComptes(), parametrageBusinessComponentEJB));
		panoramaCompteMsgSP.setAssurancesvie(convertSMMsgToSPMsg(panoramaCompteMsgSM.getAssurancesvie(), parametrageBusinessComponentEJB));
		panoramaCompteMsgSP.setCredits(convertSMMsgToSPMsg(panoramaCompteMsgSM.getCredits(), parametrageBusinessComponentEJB));
		panoramaCompteMsgSP.setCartes(convertSMMsgToSPMsg(panoramaCompteMsgSM.getCartes(), parametrageBusinessComponentEJB));
		panoramaCompteMsgSP.setServices(convertSMMsgToSPMsg(panoramaCompteMsgSM.getServices(), parametrageBusinessComponentEJB));
		return panoramaCompteMsgSP;
	}

	/**
	 * Convertit un tableau de messages SO vers un tableau de messages SP
	 * 
	 * @param services
	 * @return
	 */
	private static ServiceMsg[] convertSMMsgToSPMsg(fr.axabanque.pilotage.axb.apirest.contrat.api.msg.ServiceMsg[] servicesMsSM,
			ParametrageBusinessComponentEJB parametrageBusinessComponentEJB) {
		msg.listercontrat.contrat.apirest.axb.adapt.med.ServiceMsg[] spMsgs = null;
		if (servicesMsSM != null) {
			spMsgs = new msg.listercontrat.contrat.apirest.axb.adapt.med.ServiceMsg[servicesMsSM.length];
			for (int i = 0; i < servicesMsSM.length; i++) {
				msg.listercontrat.contrat.apirest.axb.adapt.med.ServiceMsg spMsg = new msg.listercontrat.contrat.apirest.axb.adapt.med.ServiceMsg();
				spMsg.setProduit(convertSMMsgToSPMsg(servicesMsSM[i].getProduit(), parametrageBusinessComponentEJB));
				spMsg.setDteCloture(servicesMsSM[i].getDteCloture());
				spMsg.setDteDebutSuspension(servicesMsSM[i].getDteDebutSuspension());
				spMsg.setDteFinSuspension(servicesMsSM[i].getDteFinSuspension());
				spMsg.setLibelle(servicesMsSM[i].getLibelle());
				IdContrat idContrat = constructIdContrat(servicesMsSM[i].getNumContrat(), null, servicesMsSM[i].getProduit());
				spMsg.setIdContrat(CONVERTER.convertStructToId(idContrat));
				spMsgs[i] = spMsg;
			}
		}
		return spMsgs;
	}

	/**
	 * Convertit un tableau de messages SO vers un tableau de messages SP
	 * 
	 * @param credits
	 * @return
	 */
	private static CreditMsg[] convertSMMsgToSPMsg(fr.axabanque.pilotage.axb.apirest.contrat.api.msg.CreditMsg[] creditsMsSM,
			ParametrageBusinessComponentEJB parametrageBusinessComponentEJB) {
		msg.listercontrat.contrat.apirest.axb.adapt.med.CreditMsg[] spMsgs = null;
		if (creditsMsSM != null) {
			spMsgs = new msg.listercontrat.contrat.apirest.axb.adapt.med.CreditMsg[creditsMsSM.length];
			for (int i = 0; i < creditsMsSM.length; i++) {
				msg.listercontrat.contrat.apirest.axb.adapt.med.CreditMsg spMsg = new msg.listercontrat.contrat.apirest.axb.adapt.med.CreditMsg();
				spMsg.setProduit(convertSMMsgToSPMsg(creditsMsSM[i].getProduit(), parametrageBusinessComponentEJB));
				spMsg.setCapitalRestantDu(creditsMsSM[i].getCapitalRestantDu());
				spMsg.setCodRole(new CodMsg(creditsMsSM[i].getCodRole(), null));
				spMsg.setDtePositionGestion(creditsMsSM[i].getDtePositionGestion());
				spMsg.setGestionSICLID(creditsMsSM[i].getCodGestionSICLID());
				spMsg.setIntitule(creditsMsSM[i].getIntitule());
				spMsg.setMontant(creditsMsSM[i].getMontant());
				spMsg.setNumDossier(creditsMsSM[i].getNumDossier());
				IdContrat idContrat = constructIdContrat(creditsMsSM[i].getNumContrat(), creditsMsSM[i].getNumDossier(), creditsMsSM[i].getProduit());
				spMsg.setIdContrat(CONVERTER.convertStructToId(idContrat));
				spMsgs[i] = spMsg;
			}
		}
		return spMsgs;
	}

	/**
	 * Convertit un tableau de messages SO vers un tableau de messages SP
	 * 
	 * @param assurancsevie
	 * @return
	 */
	private static AssuranceVieMsg[] convertSMMsgToSPMsg(fr.axabanque.pilotage.axb.apirest.contrat.api.msg.AssuranceVieMsg[] assurancsevieMsgSM,
			ParametrageBusinessComponentEJB parametrageBusinessComponentEJB) {
		msg.listercontrat.contrat.apirest.axb.adapt.med.AssuranceVieMsg[] spMsgs = null;
		if (assurancsevieMsgSM != null) {
			spMsgs = new msg.listercontrat.contrat.apirest.axb.adapt.med.AssuranceVieMsg[assurancsevieMsgSM.length];
			for (int i = 0; i < assurancsevieMsgSM.length; i++) {
				msg.listercontrat.contrat.apirest.axb.adapt.med.AssuranceVieMsg spMsg = new msg.listercontrat.contrat.apirest.axb.adapt.med.AssuranceVieMsg();
				String numContrat = assurancsevieMsgSM[i].getNumContrat();
				Long numContratAssurance = assurancsevieMsgSM[i].getNumContratAssurance();
				fr.axabanque.pilotage.axb.apirest.contrat.api.msg.ProduitMsg produit = assurancsevieMsgSM[i].getProduit();
				spMsg.setProduit(convertSMMsgToSPMsg(produit, parametrageBusinessComponentEJB));
				spMsg.setCodRole(new CodMsg(assurancsevieMsgSM[i].getCodRole(), null));
				spMsg.setCodStatut(new CodMsg(assurancsevieMsgSM[i].getCodStatut(), null));
				spMsg.setDteSituation(assurancsevieMsgSM[i].getDteSituation());
				spMsg.setIntitule(assurancsevieMsgSM[i].getIntitule());
				spMsg.setMntValeurRachat(assurancsevieMsgSM[i].getMntValeurRachat());
				if(numContratAssurance != null){
					spMsg.setNumContratAssurance(numContratAssurance.toString());
				}
				//QC #427741
				spMsg.setSolde(assurancsevieMsgSM[i].getSolde() == null? 0: assurancsevieMsgSM[i].getSolde());
				//QC #414970	
				IdContrat idContrat = constructIdContrat(numContrat, numContratAssurance == null ? null : numContratAssurance.toString(), produit);
				spMsg.setIdContrat(CONVERTER.convertStructToId(idContrat));
				spMsgs[i] = spMsg;
			}
		}
		return spMsgs;
	}

	/**
	 * Convertit un tableau de messages SO vers un tableau de messages SP
	 * 
	 * @param mvtPageMsgs
	 *            Tableau de MouvementPaginationMsg ( SO )
	 * @return Tableau de MouvementPaginationMsg ( SP )
	 */
	public static msg.listercontrat.contrat.apirest.axb.adapt.med.CompteMsg[] convertSMMsgToSPMsg(
			fr.axabanque.pilotage.axb.apirest.contrat.api.msg.CompteMsg[] comptesMsgSM,
			ParametrageBusinessComponentEJB parametrageBusinessComponentEJB) {
		msg.listercontrat.contrat.apirest.axb.adapt.med.CompteMsg[] spMsgs = null;
		if (comptesMsgSM != null) {
			spMsgs = new msg.listercontrat.contrat.apirest.axb.adapt.med.CompteMsg[comptesMsgSM.length];
			for (int i = 0; i < comptesMsgSM.length; i++) {
				msg.listercontrat.contrat.apirest.axb.adapt.med.CompteMsg spMsg = new msg.listercontrat.contrat.apirest.axb.adapt.med.CompteMsg();
				spMsg.setCartes(convertSMMsgToSPMsg(comptesMsgSM[i].getCartes(), parametrageBusinessComponentEJB));
				spMsg.setProduit(convertSMMsgToSPMsg(comptesMsgSM[i].getProduit(), parametrageBusinessComponentEJB));
				spMsg.setCodCanalMetierCreationOffre(comptesMsgSM[i].getCodCanalMetierCreationOffre());
				// 12 Fev 2016 
				// [QC 3593]: Pas de libelle associ� au codRole dans listerContrats
				spMsg.setCodRole(getTypeRole(comptesMsgSM[i].getCodRole(), parametrageBusinessComponentEJB));
				spMsg.setCodTypeAppartenance(getTypeAppartenance(comptesMsgSM[i].getCodTypeAppartenance(), parametrageBusinessComponentEJB));
				spMsg.setDteValorisationPortefeuille(comptesMsgSM[i].getDteValorisationPortefeuille());
				spMsg.setGestionSousMandatBln(comptesMsgSM[i].getGestionSousMandatBln());
				spMsg.setIntitule(comptesMsgSM[i].getIntitule());
				spMsg.setMntInvestiNulBln(comptesMsgSM[i].getMntInvestiNulBln());
				spMsg.setMntTotalEstimePortefeuille(comptesMsgSM[i].getMntTotalEstimePorteFeuille());
				spMsg.setMntValorisationPortefeuille(comptesMsgSM[i].getMntValorisationPortefeuille());
				spMsg.setNumCompte(comptesMsgSM[i].getNumCompte());
				spMsg.setSolde(comptesMsgSM[i].getSolde());
				IdContrat idContrat = constructIdContrat(comptesMsgSM[i].getNumContrat(), comptesMsgSM[i].getNumCompte(), comptesMsgSM[i]
						.getProduit());
				spMsg.setIdContrat(CONVERTER.convertStructToId(idContrat));
				spMsgs[i] = spMsg;
			}
		}
		return spMsgs;
	}

	/**
	 * Convertit un tableau de messages SO vers un tableau de messages SP
	 * 
	 * @param cartes
	 * @return
	 */
	private static CarteMsg[] convertSMMsgToSPMsg(fr.axabanque.pilotage.axb.apirest.contrat.api.msg.CarteMsg[] cartesMsgSM,
			ParametrageBusinessComponentEJB parametrageBusinessComponentEJB) {
		CarteMsg[] spMsgs = null;
		if (cartesMsgSM != null) {
			spMsgs = new CarteMsg[cartesMsgSM.length];
			for (int i = 0; i < cartesMsgSM.length; i++) {
				CarteMsg spMsg = new CarteMsg();
				spMsg.setCodTypeCarte(new CodMsg(cartesMsgSM[i].getCodTypeCarte(), null));
				spMsg.setCodTypeDebit(new CodMsg(cartesMsgSM[i].getCodTypeDebit(), null));
				spMsg.setCodVisuel(new CodMsg(cartesMsgSM[i].getCodVisuel(), null));
				spMsg.setLibelle(cartesMsgSM[i].getLibelle());
				spMsg.setMontantDebit(cartesMsgSM[i].getMontantDebit());
				spMsg.setNumCarte(cartesMsgSM[i].getNumCarte());
				spMsg.setNumComptePassage(cartesMsgSM[i].getNumComptePassage());
				spMsg.setProduit(convertSMMsgToSPMsg(cartesMsgSM[i].getProduit(), parametrageBusinessComponentEJB));
				// modif JEU : 22/07/2015 - on doit passer le num�ro de compte
				// de passage et non le num�ro de carte
				// IdContrat idContrat =
				// constructIdContrat(cartesMsgSM[i].getNumContrat(),
				// cartesMsgSM[i].getNumCarte(), cartesMsgSM[i].getProduit());
				IdContrat idContrat = constructIdContrat(cartesMsgSM[i].getNumContrat(), cartesMsgSM[i].getNumComptePassage(), cartesMsgSM[i]
						.getProduit());
				spMsg.setIdContrat(CONVERTER.convertStructToId(idContrat));
				spMsgs[i] = spMsg;
			}
		}
		return spMsgs;
	}

	/**
	 * Convertit un tableau de messages SO vers un tableau de messages SP
	 * 
	 * @param produit
	 * @return
	 */
	private static ProduitMsg convertSMMsgToSPMsg(fr.axabanque.pilotage.axb.apirest.contrat.api.msg.ProduitMsg produitMsgSM,
			ParametrageBusinessComponentEJB parametrageBusinessComponentEJB) {
		ProduitMsg produitSPMsg = null;
		if (produitMsgSM != null) {
			produitSPMsg = getProduit(produitMsgSM.getCodProduit() != null ? produitMsgSM.getCodProduit().getCode() : null, produitMsgSM
					.getCodSousProduit() != null ? produitMsgSM.getCodSousProduit().getCode() : null,
					produitMsgSM.getCodFamille() != null ? produitMsgSM.getCodFamille().getCode() : null,
					produitMsgSM.getCodPackage() != null ? produitMsgSM.getCodPackage().getCode() : null, parametrageBusinessComponentEJB);
		}
		return produitSPMsg;
	}

	/**
	 * 
	 * @param
	 * @param codeFamille
	 * @param codeProduit
	 * @param numReference
	 * @param numContrat
	 * @return
	 */
	private static IdContrat constructIdContrat(String numContrat, String numReference,
			fr.axabanque.pilotage.axb.apirest.contrat.api.msg.ProduitMsg produitMsg) {
		Integer codeProduit = null;
		Integer codeSousProduit = null;
		Integer codeFamille = null;
		if (produitMsg != null) {
			if (produitMsg.getCodProduit() != null)
				codeProduit = produitMsg.getCodProduit().getCode();
			if (produitMsg.getCodSousProduit() != null)
				codeSousProduit = produitMsg.getCodSousProduit().getCode();
			if (produitMsg.getCodFamille() != null) {
				codeFamille = produitMsg.getCodFamille().getCode();
			}
		}
		return constructIdContrat(numContrat, numReference, codeProduit, codeSousProduit, codeFamille);
	}

	/**
	 * 
	 * @param
	 * @param codeFamille
	 * @param codeProduit
	 * @param numReference
	 * @param numContrat
	 * @return
	 */
	private static IdContrat constructIdContrat(String numContrat, String numReference, Integer codeProduit, Integer codeSousProduit,
			Integer codeFamille) {
		String[] params = new String[7];
		// params[0] = IdInfo.CTR.toString();
		params[1] = numContrat != null ? numContrat : "";
		params[2] = numReference != null ? numReference : "";
		params[3] = codeProduit != null ? "" + codeProduit : "";
		params[4] = codeSousProduit != null ? "" + codeSousProduit : "";
		params[5] = codeFamille != null ? "" + codeFamille : "";
		params[6] = params[2];
		// return new IdContrat(params);
		return new IdContrat(params[1], params[2], params[3], params[4], params[5], params[6]);
	}

	/**
	 * 
	 * @param idRestContrat
	 * @return
	 */
	public static CritereDetailCreditMsg parseToCritereDetailCreditMsgMsgSO(IdRest idRestContrat) {
		CritereDetailCreditMsg critereDetailCreditMsg = null;
		IdContrat idContrat = (IdContrat) idRestContrat;

		critereDetailCreditMsg = new CritereDetailCreditMsg();
		critereDetailCreditMsg.setNumContrat(Integer.valueOf(idContrat.getNumContrat()));
		critereDetailCreditMsg.setCodProduit(Integer.valueOf(idContrat.getCodProduit()));

		if (!"".equalsIgnoreCase(idContrat.getCodSousProduit())) {
			critereDetailCreditMsg.setCodSousProduit(Integer.valueOf(idContrat.getCodSousProduit()));
		}

		return critereDetailCreditMsg;
	}

	/**
	 * 
	 * @param idRestContrat
	 * @return
	 */
	public static CritereContratBancaireMsg parseToCritereDetailContratBancaireMsgMsgSO(IdRest idRestContrat, HeaderSoap header) {
		CritereContratBancaireMsg critereContratBancaireMsg = null;
		if (idRestContrat != null) {
			critereContratBancaireMsg = new CritereContratBancaireMsg();
			critereContratBancaireMsg.setNumContrat(((IdContrat) idRestContrat).getNumContrat());
			if ((((IdContrat) idRestContrat).getCodFamille() != null) && (!((IdContrat) idRestContrat).getCodFamille().equals(""))) {
				critereContratBancaireMsg.setCodeFamille(Integer.valueOf(((IdContrat) idRestContrat).getCodFamille()));
			}
			if ((((IdContrat) idRestContrat).getCodProduit() != null) && (!((IdContrat) idRestContrat).getCodProduit().equals(""))) {
				critereContratBancaireMsg.setCodeProduit(Integer.valueOf(((IdContrat) idRestContrat).getCodProduit()));
			}
			if ((((IdContrat) idRestContrat).getCodSousProduit() != null) && (!((IdContrat) idRestContrat).getCodSousProduit().equals(""))) {
				critereContratBancaireMsg.setCodeSousProduit(Integer.valueOf(((IdContrat) idRestContrat).getCodSousProduit()));
			}
			critereContratBancaireMsg.setDteDebut(null);
			critereContratBancaireMsg.setDteFin(null);
			critereContratBancaireMsg.setNumClient(header.getIdUtilisateur());
			return critereContratBancaireMsg;
		}
		return null;
	}

	/**
	 * 
	 * @param lireDetailCreditMsgSo
	 * @param idRestContrat
	 * @param converter
	 * @param parametrageBusinessComponentEJB
	 * @return
	 */
	public static LireDetailCreditMsg parseToSP(fr.axabanque.pilotage.axb.apirest.contrat.api.msg.LireDetailCreditMsg lireDetailCreditMsgSo,
			IdRest idRestContrat, Converter converter, ParametrageBusinessComponentEJB parametrageBusinessComponentEJB) throws Exception {
		Map<Integer, String> map = getDicoRefCodeElementForCodeRole(parametrageBusinessComponentEJB);
		Integer codProduit = Integer.valueOf(((IdContrat) idRestContrat).getCodProduit());
		LireDetailCreditMsg lireDetailCreditMsg = null;
		if (lireDetailCreditMsgSo != null) {
			lireDetailCreditMsg = new LireDetailCreditMsg();
			ProduitMsg produit = getProduit((IdContrat) idRestContrat, parametrageBusinessComponentEJB);
			lireDetailCreditMsg.setProduit(produit);
			boolean infoPartenaire = notNullInfoPartenanire(lireDetailCreditMsgSo.getInfosPartenaireCredit());
			boolean pretSpecialise = notNullPretSpecialise(lireDetailCreditMsgSo.getPretSpecialiseMsg());
			boolean contratCredit = notNullContratCredit(lireDetailCreditMsgSo.getContratCreditMsg());
			if (COD_PRODUIT_50.equals(codProduit) || COD_PRODUIT_51.equals(codProduit)) {
				// RDG-LDC-04
				lireDetailCreditMsg.setAssuranceBln(lireDetailCreditMsgSo.getContratCreditMsg().getAssuranceBln() == 1 ? true : false);
				if (infoPartenaire) {

					InfoPartenaireCreditMsg infoPartenaireCreditPrincipal = lireDetailCreditMsgSo.getInfosPartenaireCredit()
							.getInfoPartenaireCreditPrincipal();

					if (COD_PRODUIT_50.equals(codProduit)) {
						// RDG-LDC-05
						lireDetailCreditMsg.setMontantEmprunte(infoPartenaireCreditPrincipal.getMntDecouvertFinance());
					}
					if (COD_PRODUIT_51.equals(codProduit)) {
						// RDG-LDC-05
						lireDetailCreditMsg.setMontantEmprunte(infoPartenaireCreditPrincipal.getMntDisponibleContrat());
						// RDG-LDC-21
						lireDetailCreditMsg.setMontantDisponibleReserve(infoPartenaireCreditPrincipal.getMntDisponibleContrat());
					}

					// RDG-LDC-06
					lireDetailCreditMsg.setDateDebutCredit(infoPartenaireCreditPrincipal.getDteOuverturePartenaireCredit());
					// RDG-LDC-07
					lireDetailCreditMsg.setMntCapitalRestantDu(infoPartenaireCreditPrincipal.getMntCrd());
					// RDG-LDC-08
					lireDetailCreditMsg.setDuree(infoPartenaireCreditPrincipal.getDureeInitCredit());
					// RDG-LDC-09
					lireDetailCreditMsg.setDateFinInitiale(infoPartenaireCreditPrincipal.getDteFinPrevue());
					// RDG-LDC-10
					lireDetailCreditMsg.setTauxAnnuelEffectifGlobal(infoPartenaireCreditPrincipal.getTauxTEG());
					// RDG-LDC-11
					if (lireDetailCreditMsg.getAssuranceBln()) {
						lireDetailCreditMsg.setMontantMensualite(infoPartenaireCreditPrincipal.getMntEcheanceAvecAssurance());
					} else {
						lireDetailCreditMsg.setMontantMensualite(infoPartenaireCreditPrincipal.getMntDerniereEcheance());
					}
					// RDG-LDC-12
					// correctif JEU 16/07/2015 : anomalie 379388
					if (COD_PRODUIT_51.equals(codProduit)) {
						lireDetailCreditMsg.setMontantReserve(infoPartenaireCreditPrincipal.getMntReserve());
					}
					// RDG-LDC-14
					lireDetailCreditMsg.setDateDerniereMensualite(infoPartenaireCreditPrincipal.getDteDernierReglement());
					// RDG-LDC-15
					if (COD_PRODUIT_51.equals(codProduit)) {
						lireDetailCreditMsg.setMontantProchaineEcheance(infoPartenaireCreditPrincipal.getMntProchaineEcheance());
					}
					// RDG-LDC-18
					lireDetailCreditMsg.setTauxAssurance(infoPartenaireCreditPrincipal.getTauxAssurance());

					// RDG-LDC-20
					lireDetailCreditMsg.setNumDossier(infoPartenaireCreditPrincipal.getNumDossierPartenaireCredit());

				}
				// RDG-LDC-13
				if (contratCredit) {
					List<EmprunteurMsg> emprunteurs = new ArrayList<EmprunteurMsg>();
					for (int i = 0; i < lireDetailCreditMsgSo.getContratCreditMsg().getContratCreditPersonneMsgs().length; i++) {
						ContratCreditPersonneMsg contratCreditPersonneMsg = lireDetailCreditMsgSo.getContratCreditMsg()
								.getContratCreditPersonneMsgs()[i];
						String numClient = contratCreditPersonneMsg.getNumClient();
						Integer PERSONNE_PHYSIQUE = new Integer(1);
						IdRest idCli = new IdClient(new String[] { "CLI", numClient, PERSONNE_PHYSIQUE.toString() });
						String idClientConverted = converter.convertStructToId(idCli);
						String nom = contratCreditPersonneMsg.getNomLib();
						CodMsg codRole = new CodMsg(contratCreditPersonneMsg.getCodRole(), map.get(contratCreditPersonneMsg.getCodRole()));
						emprunteurs.add(new EmprunteurMsg(idClientConverted, contratCreditPersonneMsg.getNomLib(), codRole));
					}
					lireDetailCreditMsg.setEmprunteurs(emprunteurs.toArray(new EmprunteurMsg[emprunteurs.size()]));
				}
			}
			if (COD_PRODUIT_41.equals(codProduit) || COD_PRODUIT_46.equals(codProduit)) {
				if (pretSpecialise) {
					InfoPretSpecialiseMsg detailCredit = lireDetailCreditMsgSo.getPretSpecialiseMsg().getDetailCredit();
					// RDG-LDC-04
					lireDetailCreditMsg.setAssuranceBln(detailCredit.getAssuranceBln());
					// RDG-LDC-05
					lireDetailCreditMsg.setMontantEmprunte(detailCredit.getMntPret());
					// RDG-LDC-06
					lireDetailCreditMsg.setDateDebutCredit(detailCredit.getDteOuvertureCredit());
					// RDG-LDC-07
					lireDetailCreditMsg.setMntCapitalRestantDu(detailCredit.getMntCapitalRestantDu());
					// RDG-LDC-08
					lireDetailCreditMsg.setDuree(detailCredit.getDureePret());
					// RDG-LDC-09
					lireDetailCreditMsg.setDateFinInitiale(detailCredit.getDteFinPrevu());
					// RDG-LDC-10
					lireDetailCreditMsg.setTauxAnnuelEffectifGlobal(detailCredit.getTegAnnuel());
					// RDG-LDC-11
					if (lireDetailCreditMsg.getAssuranceBln()) {
						lireDetailCreditMsg.setMontantMensualite(detailCredit.getMntAssurance() + detailCredit.getMntAssurance());
					} else {
						lireDetailCreditMsg.setMontantMensualite(detailCredit.getMntMensualiteHorsAssurance());
					}
					// RDG-LDC-14
					lireDetailCreditMsg.setDateDerniereMensualite(detailCredit.getDteDernierReglement());
					// RDG-LDC-16
					if (detailCredit.getTauxReference() != null && !"".equals(detailCredit.getTauxReference().trim())) {
						lireDetailCreditMsg.setTauxReference(detailCredit.getTauxReference());
					}
					// RDG-LDC-17
					lireDetailCreditMsg.setTauxFixe(detailCredit.getTauxFixe());
					// RDG-LDC-18
					lireDetailCreditMsg.setTauxAssurance(detailCredit.getTauxAssurance());
					// RDG-LDC-20
					lireDetailCreditMsg.setNumDossier(detailCredit.getNumDossierSab());
				}
			}
			return lireDetailCreditMsg;
		}
		return null;
	}

	private static void alimenterProduitMap(ParametrageBusinessComponentEJB parametrageBusinessComponentEJB) {
		if (TYPEPRODUITMAP == null)
			TYPEPRODUITMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_PRODUIT_DESC);

		if (SOUSPRODUITASSVIEMAP == null)
			SOUSPRODUITASSVIEMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_SOUS_PRODUIT_ASS_VIE_DESC);

		if (SOUSPRODUITCARTEMAP == null)
			SOUSPRODUITCARTEMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_SOUS_PRODUIT_CARTE_DESC);

		if (SOUSPRODUITCDDMAP == null)
			SOUSPRODUITCDDMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_SOUS_PRODUIT_CDD_DESC);

		if (SOUSPRODUITCDDTITREMAP == null)
			SOUSPRODUITCDDTITREMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_SOUS_PRODUIT_CDD_TITRE_DESC);

		if (SOUSPRODUITDATMAP == null)
			SOUSPRODUITDATMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_SOUS_PRODUIT_DAT_DESC);

		if (SOUSPRODUITDECMAP == null)
			SOUSPRODUITDECMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_SOUS_PRODUIT_DEC_DESC);

		if (SOUSPRODUITLIVRETMAP == null)
			SOUSPRODUITLIVRETMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_SOUS_PRODUIT_LIVRET_DESC);

		if (SOUSPRODUITPMOCDDMAP == null)
			SOUSPRODUITPMOCDDMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_SOUS_PRODUIT_PMO_CDD_DESC);

		if (SOUSPRODUITPMOTITREMAP == null)
			SOUSPRODUITPMOTITREMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_SOUS_PRODUIT_PMO_TITRE);

		if (SOUSPRODUITPPMAP == null)
			SOUSPRODUITPPMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_SOUS_PRODUIT_PP_DESC);

		if (SOUSPRODUITPRETSPECMAP == null)
			SOUSPRODUITPRETSPECMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_SOUS_PRODUIT_PRET_SPEC_DESC);

		if (SOUSPRODUITRAMAP == null)
			SOUSPRODUITRAMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_SOUS_PRODUIT_RA_DESC);

		if (FAMILLEMAP == null)
			FAMILLEMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_FAMILLE_DESC);

		if (PACKAGEMAP == null)
			PACKAGEMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_PACKAGE_DESC);

		if (DEBITMAP == null)
			DEBITMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_DEBIT_DESC);

		if (CARTEMAP == null)
			CARTEMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_CARTE_DESC);

		if (APPARTENANCEMAP == null)
			APPARTENANCEMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_APPARTENANCE_DESC);
		
	}

	/**
	 * 
	 * @param codProduit
	 * @param codSousProduit
	 * @param codFamille
	 * @param codPackage
	 * @param parametrageBusinessComponentEJB
	 * @return
	 */
	private static ProduitMsg getProduit(Integer codProduit, Integer codSousProduit, Integer codFamille, Integer codPackage,
			ParametrageBusinessComponentEJB parametrageBusinessComponentEJB) {
		ProduitMsg produit = new ProduitMsg();
		alimenterProduitMap(parametrageBusinessComponentEJB);

		if (codProduit != null && TYPEPRODUITMAP != null && TYPEPRODUITMAP.containsKey(codProduit)) {
			LOGGER.error("getProduit: codProduit non null");
			LOGGER.error("getProduit: " + codProduit.intValue());
			produit.setCodProduit(TYPEPRODUITMAP.get(codProduit));
			produit.setCodProduitAgrege(TYPEPRODUITMAP.get(codProduit));
			if (codSousProduit != null) {
				CodMsg sousProduit = getSousProduit(codProduit, codSousProduit, codFamille);
				produit.setCodSousProduit(sousProduit);
			}
		}
		if (codFamille != null && FAMILLEMAP != null && FAMILLEMAP.containsKey(codFamille)) {
			produit.setCodFamille(FAMILLEMAP.get(codFamille));
		}

		if (codPackage != null && PACKAGEMAP != null && PACKAGEMAP.containsKey(codPackage)) {
			produit.setCodPackage(PACKAGEMAP.get(codPackage));
		}
		return produit;
	}
	
	
	/**
	 * 
	 * @param codTypeAppartenance
	 * @param codPackage
	 * @param parametrageBusinessComponentEJB
	 * @return
	 */
	private static CodMsg getTypeAppartenance(Integer codTypeAppartenance, ParametrageBusinessComponentEJB parametrageBusinessComponentEJB) {

		alimenterProduitMap(parametrageBusinessComponentEJB);
		if (codTypeAppartenance != null && APPARTENANCEMAP != null && APPARTENANCEMAP.containsKey(codTypeAppartenance)) {
			return APPARTENANCEMAP.get(codTypeAppartenance);
		}
		return new CodMsg(codTypeAppartenance, null);
	}

	private static ProduitMsg getProduit(IdContrat idContrat, ParametrageBusinessComponentEJB parametrageBusinessComponentEJB) {
		ProduitMsg produit = null;
		if (idContrat != null) {
			produit = new ProduitMsg();

			Integer codProduit = null;
			Integer codSousProduit = null;
			Integer codFamille = null;

			if ((idContrat.getCodProduit() != null) && !(idContrat.getCodProduit().equals(""))) {
				codProduit = Integer.valueOf(idContrat.getCodProduit());
			}
			if ((idContrat.getCodSousProduit() != null) && !(idContrat.getCodSousProduit().equals(""))) {
				codSousProduit = Integer.valueOf(idContrat.getCodSousProduit());
			}
			if ((idContrat.getCodFamille() != null) && !(idContrat.getCodFamille().equals(""))) {
				codFamille = Integer.valueOf(idContrat.getCodFamille());
			}

			produit = getProduit(codProduit, codSousProduit, codFamille, null, parametrageBusinessComponentEJB);
		}
		return produit;
	}

	/**
	 * 
	 * @param parametrageBusinessComponentEJB
	 * @param detailContratBancaireMsg
	 * @param idClient
	 * @param idContrat
	 * @param idRestContrat
	 * @param codRole
	 * @return
	 */
	public static DetailsContratBancaireMsg parseToSPFromSO(ParametrageBusinessComponentEJB parametrageBusinessComponentEJB,
			ListerActionEJB listerActionBusinessComponent, DetailContratBancaireMsg detailContratBancaireMsg, String idContrat, IdRest idRestContrat,
			String idClient, Integer codRole) {
		DetailsContratBancaireMsg detailsContratBancaireMsg = new DetailsContratBancaireMsg();
		if (detailContratBancaireMsg != null) {
			detailsContratBancaireMsg.setNumero(getNumero(detailContratBancaireMsg));
			detailsContratBancaireMsg.setIdContrat(idContrat);
			detailsContratBancaireMsg.setDevise(getDevise(detailContratBancaireMsg));
			detailsContratBancaireMsg.setSolde(getSolde(detailContratBancaireMsg));
			// 12 Fev 2016 
			// [QC 3593]: Pas de libelle associ� au codRole dans listerContrats
			detailsContratBancaireMsg.setCodRole(getTypeRole(codRole, parametrageBusinessComponentEJB));
			detailsContratBancaireMsg.setGestionSousMandatBln(getGestionSousMandatBln(detailContratBancaireMsg));
			detailsContratBancaireMsg.setIntitule(getIntitule(detailContratBancaireMsg));
			detailsContratBancaireMsg.setDateOuverture(getDateOuverture(detailContratBancaireMsg));
			detailsContratBancaireMsg.setDateCloture(getDateCloture(detailContratBancaireMsg));
			detailsContratBancaireMsg.setBlocageDecouvert(getBlocageDecouvert(detailContratBancaireMsg));
			detailsContratBancaireMsg.setMontantDecouvert(getMontantDecouvert(detailContratBancaireMsg));
			detailsContratBancaireMsg.setClientsMandataires(getClientsMandataires(detailContratBancaireMsg));
			detailsContratBancaireMsg.setDetailsCarte(getDetailsCarte(detailContratBancaireMsg));
			detailsContratBancaireMsg.setProduit(getProduit(parametrageBusinessComponentEJB, detailContratBancaireMsg));
			detailsContratBancaireMsg.setDetailsEpargne(getDetailsEpargne(detailContratBancaireMsg));
			detailsContratBancaireMsg.setDetailsCompte(getDetailsCompte(parametrageBusinessComponentEJB, detailContratBancaireMsg, idContrat));
			detailsContratBancaireMsg.setRib(getRib(parametrageBusinessComponentEJB, detailContratBancaireMsg));
			detailsContratBancaireMsg.setValorisationPortefeuille(detailContratBancaireMsg.getValorisationPortefeuille());
			detailsContratBancaireMsg.setInformationsFiscales(getInformationsFiscales(parametrageBusinessComponentEJB, detailContratBancaireMsg));
			// Les actions
			detailsContratBancaireMsg.setActions(getActions(listerActionBusinessComponent, idRestContrat, codRole));
		}
		return detailsContratBancaireMsg;
	}

	// 12 Fev 2016 
	// [QC 3593]: Pas de libelle associ� au codRole dans listerContrats
	private static CodMsg getTypeRole(Integer codeRole, ParametrageBusinessComponentEJB parametrageBusinessComponentEJB) {

		if (TYPEROLEMAP == null)
			TYPEROLEMAP = getDictionnaireMap(parametrageBusinessComponentEJB, REF_TYPE_ROLE_DESC);

		if (codeRole != null && TYPEROLEMAP != null && TYPEROLEMAP.containsKey(codeRole)) {
			return TYPEROLEMAP.get(codeRole);
		} else
			return new CodMsg(codeRole, null);
	}
	
	/**
	 * 
	 * @param listerActionBusinessComponent
	 * @param idRestContrat
	 * @param codRole
	 * @return
	 */
	private static ActionMsg[] getActions(ListerActionEJB listerActionBusinessComponent, IdRest idRestContrat, Integer codRole) {
		ListeCritereListerActionContratMsg listeCritereListerActionContratMsg = new ListeCritereListerActionContratMsg();
		CritereListerActionContratMsg[] critereListerActionContratMsgs = new CritereListerActionContratMsg[1];
		critereListerActionContratMsgs[0] = constructCritereListerActionContratMsg(idRestContrat, codRole);
		listeCritereListerActionContratMsg.setCritereListerActionContratMsg(critereListerActionContratMsgs);
		// Appel du service lister Action
		ListeActionsMsg listeActionsMsg = null;
		ActionMsg[] actions = null;
		try {
			listeActionsMsg = listerActionBusinessComponent.listerActionContrat(listeCritereListerActionContratMsg);
			if (listeActionsMsg != null)
				actions = listeActionsMsg.getActionsForFunctionalKey(((IdContrat) idRestContrat).getNumContrat());
		} catch (RemoteException e) {
			LOGGER.error("Erreur distance lors de la r�cup�ration de la liste des actions pour le contrat : "
					+ ((IdContrat) idRestContrat).getNumContrat());
		} catch (TechnicalException e) {
			LOGGER.error("Erreur technique lors de la r�cup�ration de la liste des actions pour le contrat : "
					+ ((IdContrat) idRestContrat).getNumContrat());
		} catch (RestBusinessException e) {
			LOGGER.error("Erreur m�tier lors de la r�cup�ration de la liste des actions pour le contrat : "
					+ ((IdContrat) idRestContrat).getNumContrat());
		}
		return actions;
	}

	/**
	 * 
	 * @param idRestContrat
	 * @param codRole
	 * @return
	 */
	private static CritereListerActionContratMsg constructCritereListerActionContratMsg(IdRest idRestContrat, Integer codRole) {
		CritereListerActionContratMsg critereListerActionContratMsg = new CritereListerActionContratMsg();
		critereListerActionContratMsg.setCleFonctionnelle(((IdContrat) idRestContrat).getNumContrat());
		CritereInfoMsg critereInfoMsg = new CritereInfoMsg();
		critereInfoMsg.setCodTypeRole(CodeTypeRole.valueOf(codRole));
		critereInfoMsg.setCodProduit(Integer.valueOf(((IdContrat) idRestContrat).getCodProduit()));
		critereListerActionContratMsg.setCritereInfoMsg(critereInfoMsg);
		return critereListerActionContratMsg;
	}

	/**
	 * 
	 * @param parametrageBusinessComponentEJB
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static InformationsFiscalesMsg getInformationsFiscales(ParametrageBusinessComponentEJB parametrageBusinessComponentEJB,
			DetailContratBancaireMsg detailContratBancaireMsg) {
		InformationsFiscalesMsg informationsFiscalesMsg = null;
		if (isNotNull(detailContratBancaireMsg)) {
			informationsFiscalesMsg = new InformationsFiscalesMsg();
			//compte Titre
			if (isNotNull(getInformationsFiscalesCompteTitreMsg(detailContratBancaireMsg))) {
				InformationsFiscalesCompteTitreMsg informationsFiscalesCompteTitreMsg = getInformationsFiscalesCompteTitreMsg(detailContratBancaireMsg);
				informationsFiscalesMsg.setMontantCessionsGlobales(informationsFiscalesCompteTitreMsg.getMntCessionsGlobales());
				informationsFiscalesMsg.setMontantFrancaisSansAbattement(informationsFiscalesCompteTitreMsg.getMntFrSsAbat());
				informationsFiscalesMsg.setMontantPrelevementLiberatoire(informationsFiscalesCompteTitreMsg.getMntPL());
				informationsFiscalesMsg.setMontantPlusValues(informationsFiscalesCompteTitreMsg.getMntPlusValues());
				informationsFiscalesMsg.setMontantRevenusEtrangersAbattement(informationsFiscalesCompteTitreMsg.getMntRevenusEtAbat());
				informationsFiscalesMsg.setMontantRevenusFrancaisAbattement(informationsFiscalesCompteTitreMsg.getMntRevenusFRAbat());
				informationsFiscalesMsg.setMontantRevenusPrelevementLiberatoire(informationsFiscalesCompteTitreMsg.getMntRevenusPL());
			}
			if (isNotNull(getDetailsCompteTitreMsg(detailContratBancaireMsg))) {
				informationsFiscalesMsg.setOptionFiscale(getDetailsCompteTitreMsg(detailContratBancaireMsg).getOptionFiscale());
			}
			//compte PEA
			if (isNotNull(getInformationsFiscalesPEAMsg(detailContratBancaireMsg))) {
				InformationsFiscalesPEAMsg informationsFiscalesPEAMsg = getInformationsFiscalesPEAMsg(detailContratBancaireMsg);
				informationsFiscalesMsg.setCumulRetraitsPartiels(informationsFiscalesPEAMsg.getCumulRetraitsPartiels());
				informationsFiscalesMsg.setDateOuvertureFiscale(informationsFiscalesPEAMsg.getDateOuvertureFiscale());
				informationsFiscalesMsg.setDatePremierRetrait(informationsFiscalesPEAMsg.getDtePremierRetrait());
				informationsFiscalesMsg.setMontantsCumules(informationsFiscalesPEAMsg.getMontantsCumules());
				informationsFiscalesMsg.setMontantsValuesLatentes(informationsFiscalesPEAMsg.getMontantsVL());
				informationsFiscalesMsg.setCumulVersement(informationsFiscalesPEAMsg.getCumulVersement());
			}
			//CompteDepotATerme
			if (isNotNull(getDetailsCompteDepotATermeMsg(detailContratBancaireMsg))) {
				informationsFiscalesMsg.setOptionFiscale(getDetailsCompteDepotATermeMsg(detailContratBancaireMsg).getOptionFiscale());
			}
			//CSL
			else if (isNotNull(getDetailsCSLMsg(detailContratBancaireMsg))) {
				informationsFiscalesMsg.setOptionFiscale(getDetailsCSLMsg(detailContratBancaireMsg).getOptionFiscale());
			}
		}
		return informationsFiscalesMsg;
	}

	/**
	 * 
	 * @param parametrageBusinessComponentEJB
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static RIBMsg getRib(ParametrageBusinessComponentEJB parametrageBusinessComponentEJB, DetailContratBancaireMsg detailContratBancaireMsg) {
		RIBMsg ribMsg = null;
		if (isNotNull(detailContratBancaireMsg)) {
			if (isNotNull(getCompteBancaire(detailContratBancaireMsg))) {
				CompteBancaireMsg compteBancaireMsg = getCompteBancaire(detailContratBancaireMsg);
				ribMsg = new RIBMsg();
				ribMsg.setCleIban(compteBancaireMsg.getCleIban());
				ribMsg.setCleRib(compteBancaireMsg.getCleRib());
				ribMsg.setCodeAgence(compteBancaireMsg.getCodAgence());
				ribMsg.setCodeGuichet(compteBancaireMsg.getCodGuichet());
				ribMsg.setBic(compteBancaireMsg.getCodeIdentifiantBancaire());
				String codePaysIban = compteBancaireMsg.getCodePaysIban();
				ribMsg.setLibellePays(getLibellePays(parametrageBusinessComponentEJB, codePaysIban));
				ribMsg.setIban(compteBancaireMsg.getIban());
				ribMsg.setNumCompte(compteBancaireMsg.getNumCompte());
			}
		}
		return ribMsg;
	}

	/**
	 * R�cup�re le libell� du Pays depuis le DicoRef
	 * 
	 * @param parametrageBusinessComponentEJB
	 * @param codePaysIban
	 * @return
	 */
	private static String getLibellePays(ParametrageBusinessComponentEJB parametrageBusinessComponentEJB, String codePaysIban) {
		CritereDictionaryMsg critereDictionaryMsg = new CritereDictionaryMsg();
		critereDictionaryMsg.setDicoClassName(REF_TYPE_PAYS_IBAN_DESC);
		critereDictionaryMsg.setGroupElement("1");
		DictionaryRefMsg dictionaryRefMsg = null;
		try {
			dictionaryRefMsg = parametrageBusinessComponentEJB.lireDicoRef(critereDictionaryMsg);
		} catch (BusinessException e) {
			LOGGER.error("Erreur m�tier lors de l'appelle du dicoref pour la r�cup�ration du libell� du pays : " + codePaysIban);
			// throw new
			// BusinessException("Erreur lors de l'appel du Param�trage lors de la lecture de la table DICOREF");
		} catch (RemoteException e) {
			LOGGER.error("Erreur distance lors de l'appelle du dicoref pour la r�cup�ration du libell� du pays : " + codePaysIban);
		} catch (TechnicalException e) {
			LOGGER.error("Erreur technique lors de l'appelle du dicoref pour la r�cup�ration du libell� du pays : " + codePaysIban);
		}
		if (dictionaryRefMsg != null) {
			DictionaryRefValueMsg[] dictionaryRefValueMsg = dictionaryRefMsg.getDicoValue();
			for (int i = 0; i < dictionaryRefValueMsg.length; i++) {
				DictionaryRefValueMsg dicoRefValueMsg = dictionaryRefValueMsg[i];
				if (dicoRefValueMsg != null) {
					if (dicoRefValueMsg.getCodElement().equalsIgnoreCase(codePaysIban))
						return dicoRefValueMsg.getLibElement();
				}
			}
		}
		return null;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @param idContrat
	 * @return
	 */
	private static DetailsCompteMsg getDetailsCompte(ParametrageBusinessComponentEJB parametrageBusinessComponentEJB,
			DetailContratBancaireMsg detailContratBancaireMsg, String idContrat) {
		DetailsCompteMsg detailsCompteMsg = null;
		if (detailContratBancaireMsg != null) {
			detailsCompteMsg = new DetailsCompteMsg();
			DetailsCELMsg detailsCELMsg = getDetailsCELMsg(detailContratBancaireMsg);
			DetailsPELMsg detailsPELMsg = getDetailsPELMsg(detailContratBancaireMsg);
			DetailsCompteDepotATermeMsg detailsCompteDepotATermeMsg = getDetailsCompteDepotATermeMsg(detailContratBancaireMsg);
			if (isNotNull(getCashBackCompteMsg(detailContratBancaireMsg))) {
				CashBackCompteMsg cashBackCompteMsg = getCashBackCompteMsg(detailContratBancaireMsg);
				detailsCompteMsg.setDateAnniversaire(cashBackCompteMsg.getDateAnniversaireCpt());
				detailsCompteMsg.setIdCarteRemboursementAnneePrecedente(cashBackCompteMsg.getNumCarteRbtAnneePrecedente());
				detailsCompteMsg.setDateRemboursementAnneePrecedente(cashBackCompteMsg.getDateRbtAnneePrecedente());
				detailsCompteMsg.setMontantRemboursementAnneePrecedente(cashBackCompteMsg.getMontantRbtAnneePrecedente());
			}
			//PEL
			if (isNotNull(detailsPELMsg)) {
				detailsCompteMsg.setDateArrete(detailsPELMsg.getDteArretee());
				detailsCompteMsg.setInteretsProvisionnes(detailsPELMsg.getInteretsProvisionnes());
				detailsCompteMsg.setMontantPlafondVersement(detailsPELMsg.getMontantPlafondVersement());
			}
			//CEL
			if (isNotNull(detailsCELMsg)) {
				detailsCompteMsg.setDateArrete(detailsCELMsg.getDteArrete());
				detailsCompteMsg.setInteretsVerses(detailsCELMsg.getInteretsVerses());
				detailsCompteMsg.setMontantTotalInteretsAnnee(detailsCELMsg.getMontantTotalInteretsAnnee());
				detailsCompteMsg.setMontantTotalVersement(detailsCELMsg.getMontantTotalVersement());
				detailsCompteMsg.setInteretsProvisionnes(detailsCELMsg.getInteretsProvisionnes());
				detailsCompteMsg.setMontantPlafondVersement(detailsCELMsg.getMontantPlafondVersement());
			}
			//compteDepotATerme
			if (isNotNull(detailsCompteDepotATermeMsg)) {
				detailsCompteMsg.setDateEcheancePrevue(detailsCompteDepotATermeMsg.getDteEcheancePrevue());
				detailsCompteMsg.setDuree(detailsCompteDepotATermeMsg.getDuree());
				detailsCompteMsg.setInteretsVersement(detailsCompteDepotATermeMsg.getInteretVersement());
				detailsCompteMsg.setMontantVersement(detailsCompteDepotATermeMsg.getMontantVersement());
				detailsCompteMsg.setNatureOperation(detailsCompteDepotATermeMsg.getNatureOperation());
				detailsCompteMsg.setTaux(detailsCompteDepotATermeMsg.getTaux());
				detailsCompteMsg.setCumulMontant(detailsCompteDepotATermeMsg.getTaux());
			}
			detailsCompteMsg.setCumulAchatCarte(getCumulAchatCarte(parametrageBusinessComponentEJB, detailContratBancaireMsg));
			detailsCompteMsg.setPortefeuilles(getPortefeuilles(detailContratBancaireMsg));
			detailsCompteMsg.setCartes(getCartes(parametrageBusinessComponentEJB, detailContratBancaireMsg, idContrat));
			detailsCompteMsg.setCashBackAssurance(getCashBackAssurance(detailContratBancaireMsg));
		}
		return detailsCompteMsg;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static CashBackAssuranceMsg[] getCashBackAssurance(DetailContratBancaireMsg detailContratBancaireMsg) {
		CashBackAssuranceMsg[] cashsBackAssuranceMsg = null;
		if (isNotNull(detailContratBancaireMsg)) {
			if (isNotNull(getCashBackAssuranceMsg(detailContratBancaireMsg))) {
				fr.axabanque.crm.tarification.api.msg.CashBackAssuranceMsg[] cashBacksAssuranceMsgSM = getCashBackAssuranceMsg(detailContratBancaireMsg);
				cashsBackAssuranceMsg = new CashBackAssuranceMsg[cashBacksAssuranceMsgSM.length];
				CashBackAssuranceMsg cashBackAssuranceMsg = null;
				for (int i = 0; i < cashBacksAssuranceMsgSM.length; i++) {
					cashBackAssuranceMsg = new CashBackAssuranceMsg();
					fr.axabanque.crm.tarification.api.msg.CashBackAssuranceMsg cashBackAssuranceMsgSM = cashBacksAssuranceMsgSM[i];
					cashBackAssuranceMsg.setDateOperation(cashBackAssuranceMsgSM.getDteOperation());
					cashBackAssuranceMsg.setLibelleProduitAssurance(cashBackAssuranceMsgSM.getLibProduitAssurance());
					cashBackAssuranceMsg.setMontantCashBack(cashBackAssuranceMsgSM.getMntCashBack());
					cashBackAssuranceMsg.setMontantNetAnnuel(cashBackAssuranceMsgSM.getMntNetAnnuel());
					cashBackAssuranceMsg.setNumContratAssurance(cashBackAssuranceMsgSM.getNumContratAssurance());
					cashBackAssuranceMsg.setTauxRetrocession(cashBackAssuranceMsgSM.getTauxRetrocession());
					// cashsBackAssuranceMsg
					cashsBackAssuranceMsg[i] = cashBackAssuranceMsg;
				}
			}
		}
		return cashsBackAssuranceMsg;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static DetailsCarteMsg[] getCartes(ParametrageBusinessComponentEJB parametrageBusinessComponentEJB,
			DetailContratBancaireMsg detailContratBancaireMsg, String idContrat) {
		DetailsCarteMsg[] detailsCarteMsg = null;
		if (isNotNull(detailContratBancaireMsg)) {
			fr.axabanque.crm.comptebancaire.api.msg.DetailsCarteMsg detailsCarteCompteBancaire = detailContratBancaireMsg.getDetailsCarteMsg();
			fr.axabanque.crm.comptebancaire.api.msg.PlafondCarteMsg plafondCarteMsg = null;
			fr.axabanque.crm.comptebancaire.api.msg.CompteurCarteMsg compteurCarteMsg = null;
			fr.axabanque.crm.comptebancaire.api.msg.CompteurRetraitCarteMsg compteurRetraitCarteMsg = null;
			if (detailsCarteCompteBancaire != null) {
				plafondCarteMsg = detailsCarteCompteBancaire.getPlafond();
				compteurCarteMsg = detailsCarteCompteBancaire.getCompteurCarte();
				compteurRetraitCarteMsg = detailsCarteCompteBancaire.getCompteurRetraitCarte();
			}
			DonneesCarteMsg[] donneesCartes = detailContratBancaireMsg.getDonneesCarteMsg();
			if (donneesCartes != null) {
				detailsCarteMsg = new DetailsCarteMsg[donneesCartes.length];
				DetailsCarteMsg detailCarteMsg = null;
				DonneesCarteMsg donneeCartes = null;
				for (int i = 0; i < donneesCartes.length; i++) {
					detailCarteMsg = new DetailsCarteMsg();
					donneeCartes = donneesCartes[i];
					// idContrat
					detailCarteMsg.setIdContrat(idContrat);
					// Donn�es de la carte
					detailCarteMsg.setNumCarte(masquerNumeroCarte(donneeCartes.getNumCarte()));
					detailCarteMsg.setDateCreation(donneeCartes.getDteCreation());
					detailCarteMsg.setDateCloture(donneeCartes.getDteCloture());
					detailCarteMsg.setLibelle(donneeCartes.getLibelleCarte());
					detailCarteMsg.setEncoursMontant(donneeCartes.getEncoursMnt() != null ? donneeCartes.getEncoursMnt().doubleValue() : null);
					detailCarteMsg.setProduit(getProduit(parametrageBusinessComponentEJB, detailContratBancaireMsg));
					detailCarteMsg.setCodVisuel(new CodMsg(donneeCartes.getVisuelCarte(), null));
					// Donn�es de plafond
					if (compteurRetraitCarteMsg != null) {
						detailCarteMsg.setRetraitAnneeEnCours(compteurRetraitCarteMsg.getRetraitAnneeEnCours() != null ? compteurRetraitCarteMsg
								.getRetraitAnneeEnCours().doubleValue() : null);
						detailCarteMsg
								.setRetraitAnneePrecedente(compteurRetraitCarteMsg.getRetraitAnneePrecedente() != null ? compteurRetraitCarteMsg
										.getRetraitAnneePrecedente().doubleValue() : null);
						detailCarteMsg.setRetraitMoisEnCours(compteurRetraitCarteMsg.getRetraitMoisEnCours() != null ? compteurRetraitCarteMsg
								.getRetraitMoisEnCours().doubleValue() : null);
						detailCarteMsg.setRetraitMoisPrecedent(compteurRetraitCarteMsg.getRetraitMoisPrecedent() != null ? compteurRetraitCarteMsg
								.getRetraitMoisPrecedent().doubleValue() : null);
					}
					if (compteurCarteMsg != null) {
						detailCarteMsg.setAchatCarteCourants(compteurCarteMsg.getAchatCarteCourants());
						detailCarteMsg.setDateEcheance(compteurCarteMsg.getDteEcheance());
						detailCarteMsg.setEnCours(compteurCarteMsg.getEnCours());
					}
					if (plafondCarteMsg != null) {
						detailCarteMsg.setMontantPaiementEtranger(plafondCarteMsg.getMntPaiementEtranger());
						detailCarteMsg.setMontantPaiementFrance(plafondCarteMsg.getMntPaiementFrance());
						detailCarteMsg.setMontantRetraitBanque(plafondCarteMsg.getMntRetraitBnp());
						detailCarteMsg.setMontantRetraitEtranger(plafondCarteMsg.getMntRetraitEtranger());
						detailCarteMsg.setMontantRetraitFrance(plafondCarteMsg.getMntRetraitFrance());
						detailCarteMsg.setMontantPlafond(plafondCarteMsg.getMontantPlafond());
						detailCarteMsg.setNiveauService(new CodMsg(Integer.valueOf(plafondCarteMsg.getNiveauService()), null));
					}
					//
					detailsCarteMsg[i] = detailCarteMsg;
				}
			}

		}
		return detailsCarteMsg;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static ValeurMsg[] getPortefeuilles(DetailContratBancaireMsg detailContratBancaireMsg) {
		ValeurMsg[] valeursMsg = null;
		if (isNotNull(detailContratBancaireMsg)) {
			StockMsg[] stocksMsg = null;
			if (isNotNull(detailContratBancaireMsg.getPortefeuilleMsg()))
				stocksMsg = detailContratBancaireMsg.getPortefeuilleMsg().getStocks();
			if (isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg())
					&& isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg().getPortefeuilleMsg())
					&& isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg().getPortefeuilleMsg().getStocks())) 
				stocksMsg = detailContratBancaireMsg.getInfoComptePEAMsg().getPortefeuilleMsg().getStocks();
			if (stocksMsg!=null){
				valeursMsg = new ValeurMsg[stocksMsg.length];
				ValeurMsg valeurMsg = null;
				for (int i = 0; i < stocksMsg.length; i++) {
					valeurMsg = new ValeurMsg();
					StockMsg stockMsg = stocksMsg[i];
					valeurMsg.setCodSens(new CodMsg(stockMsg.getCodSens(), null));
					valeurMsg.setCodTypeLigne(new CodMsg(stockMsg.getCodTypeLigne(), null));
					valeurMsg.setCumulCession(stockMsg.getCumulCession());
					valeurMsg.setDateCotation(stockMsg.getDteCotation());
					valeurMsg.setDateTraitement(stockMsg.getDteTraitement());
					valeurMsg.setMontantInvesti(stockMsg.getMontantInvesti());
					valeurMsg.setPrixRevient(stockMsg.getPrixRevient());
					valeurMsg.setPlusValueNonRealisees(stockMsg.getPvNonRealisees());
					valeurMsg.setPlusValueRealisees(stockMsg.getPvRealisees());
					valeurMsg.setQuantite(stockMsg.getQuantite());
					valeurMsg.setSecurityClass(stockMsg.getSecurityClass());
					if (isNotNull(stockMsg.getValeur())) {
						fr.axabanque.crm.titre.api.msg.ValeurMsg valeur = stockMsg.getValeur();
						valeurMsg.setCodDevise(new CodMsg(valeur.getCodeDevise(), null));
						valeurMsg.setCodTypeValeur(new CodMsg(valeur.getCodeTypeValeur(), null));
						valeurMsg.setCodeValeur(valeur.getCodeValeur());
						valeurMsg.setCodeValeur(valeur.getCodeValeur());
						valeurMsg.setCoursConnuBln(valeur.getCoursConnuBln());
						valeurMsg.setCoursValeur(valeur.getCoursValeur());
						valeurMsg.setDroitEntree(valeur.getDroitEntree());
						valeurMsg.setNom(valeur.getLibelleValeur());
						if (VALUE_INT_1.equals(valeur.getTitreBln()) && VALUE_INT_0.equals(valeur.getPEABln()))
							valeurMsg.setRestriction(RESTRICTION_TITRE);
						else if (VALUE_INT_0.equals(valeur.getTitreBln()) && VALUE_INT_1.equals(valeur.getPEABln()))
							valeurMsg.setRestriction(RESTRICTION_PEA);
						else
							valeurMsg.setRestriction(null);
						valeurMsg.setPlaceCotation(valeur.getPlaceCotation());
						valeurMsg.setQuantiteUnite(valeur.getQuantiteUnite());
						valeurMsg.setQuotationType(valeur.getQuantiteUnite());
						valeurMsg.setVisuAbnEpargneBln(toBoolean(valeur.getVisuAbnEpargneBln()));
						valeurMsg.setVolatilite(valeur.getVolatilite());
					}
					valeurMsg.setValorisationPortefeuille(detailContratBancaireMsg.getValorisationPortefeuille());
					// droitsCELMsg
					valeursMsg[i] = valeurMsg;
				}
			}
		}
		return valeursMsg;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static CumulAchatCarteMsg[] getCumulAchatCarte(ParametrageBusinessComponentEJB parametrageBusinessComponentEJB,
			DetailContratBancaireMsg detailContratBancaireMsg) {
		CumulAchatCarteMsg[] cumulsAchatCarteMsg = null;
		if (isNotNull(detailContratBancaireMsg)) {
			CumulAchatCarteMsg cumulAchatCarteMsg = null;
			fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.api.msg.CumulCarteMsg[] cumulsCarteEnCoursMsg = getCumulCarteEnCoursMsg(detailContratBancaireMsg);
			// correction #438030 : [QUALIF - service lireDetailContratBancaire]
			// le cumulAchatCarte remont� est vide 
			fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.api.msg.CumulCarteMsg[] cumulsCartePrecMsg = getCumulsCartesAnneePrecedenteMsg(detailContratBancaireMsg);
			List<CumulAchatCarteMsg> tmpList = new ArrayList<CumulAchatCarteMsg>();
			if (cumulsCarteEnCoursMsg != null) {
				for (int i = 0; i < cumulsCarteEnCoursMsg.length; i++) {
					fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.api.msg.CumulCarteMsg cumulCarte = cumulsCarteEnCoursMsg[i];
					cumulAchatCarteMsg = new CumulAchatCarteMsg();
					cumulAchatCarteMsg.setIdCarte(null);
					cumulAchatCarteMsg.setIntituleCarte(cumulCarte.getIntituleCarte());
					cumulAchatCarteMsg.setNumCarte(masquerNumeroCarte(cumulCarte.getNumCarte()));
					cumulAchatCarteMsg.setCumulAchat(cumulCarte.getCumulAchat());
					cumulAchatCarteMsg.setDateTransfoCarte(cumulCarte.getDteTransfoCarte());
					cumulAchatCarteMsg.setDate(new Date());
					cumulAchatCarteMsg.setProduit(getProduit(parametrageBusinessComponentEJB, detailContratBancaireMsg));
					tmpList.add(cumulAchatCarteMsg);
				}
			}
			// get last year
			Calendar cal = GregorianCalendar.getInstance();
			cal.add(GregorianCalendar.YEAR, -1);
			cal.set(GregorianCalendar.MONTH, 11);
			cal.set(GregorianCalendar.DAY_OF_MONTH, 31);
			if (cumulsCartePrecMsg != null) {
				for (int i = 0; i < cumulsCartePrecMsg.length; i++) {
					fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.api.msg.CumulCarteMsg cumulCarte = cumulsCartePrecMsg[i];
					cumulAchatCarteMsg = new CumulAchatCarteMsg();
					cumulAchatCarteMsg.setIdCarte(null);
					cumulAchatCarteMsg.setIntituleCarte(cumulCarte.getIntituleCarte());
					cumulAchatCarteMsg.setNumCarte(masquerNumeroCarte(cumulCarte.getNumCarte()));
					cumulAchatCarteMsg.setCumulAchat(cumulCarte.getCumulAchat());
					cumulAchatCarteMsg.setDateTransfoCarte(cumulCarte.getDteTransfoCarte());
					cumulAchatCarteMsg.setDate(cal.getTime());
					cumulAchatCarteMsg.setProduit(getProduit(parametrageBusinessComponentEJB, detailContratBancaireMsg));
					tmpList.add(cumulAchatCarteMsg);
				}
			}
			cumulsAchatCarteMsg = tmpList.toArray(new CumulAchatCarteMsg[tmpList.size()]);
		}
		return cumulsAchatCarteMsg;
	}
	
	private static fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.api.msg.CumulCarteMsg[] getCumulsCartesAnneePrecedenteMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getCashBackCompte()) ? detailContratBancaireMsg.getCashBackCompte().getCumulsCartesAnneePrecedente() : null;
	}
	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static DetailsEpargneMsg getDetailsEpargne(DetailContratBancaireMsg detailContratBancaireMsg) {
		DetailsEpargneMsg detailsEpargneMsg = null;
		if (detailContratBancaireMsg != null) {
			detailsEpargneMsg = new DetailsEpargneMsg();
			// CSL
			if (isNotNull(getDetailsCSLMsg(detailContratBancaireMsg))) {
				DetailsCSLMsg detailsCSLMsg = getDetailsCSLMsg(detailContratBancaireMsg);
				detailsEpargneMsg.setTauxBase(detailsCSLMsg.getTauxBase());
				if (isNotNull(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsInitiales())
						&& isNotNull(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsInitiales().getDteVersementInitial())) {
					// DteVersementInitial
					detailsEpargneMsg.setDateVersementInitial(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsInitiales().getDteVersementInitial());
				}

				if (isNotNull(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsComplementaires())) {
					if(isNotNull(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsComplementaires().getMotifCloture()))
					detailsEpargneMsg.setMotifCloture(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsComplementaires().getMotifCloture());
					//17 Fev. 2016
					detailsEpargneMsg.setDateArretee(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsComplementaires().getDteArrete());
					// 12 Fev. 2016
					// Correction Les int�r�ts des livrets ne sont pas remont�s
					if(isNotNull(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsComplementaires().getInteretProvisionnes()))
					detailsEpargneMsg.setInteretsProvisionnes(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsComplementaires().getInteretProvisionnes());
				}
			}

			// PEL
			if (isNotNull(getDetailsPELMsg(detailContratBancaireMsg))) {
				DetailsPELMsg detailsPELMsg = getDetailsPELMsg(detailContratBancaireMsg);
				detailsEpargneMsg.setDateArretee(detailsPELMsg.getDteArretee());
				detailsEpargneMsg.setDateEcheance(detailsPELMsg.getDteEcheance());
				detailsEpargneMsg.setInteretsProvisionnes(detailsPELMsg.getInteretsProvisionnes());
				detailsEpargneMsg.setMontantPlafondVersement(detailsPELMsg.getMontantPlafondVersement());
				detailsEpargneMsg.setMontantVersementInitial(detailsPELMsg.getVersementInitial());
			
			}
			detailsEpargneMsg.setInformationsPEL(getInformationsPEL(detailContratBancaireMsg));

			// LIVRET JEUNE
			if (isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg())) {
				if (isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg().getInformationsInitiales())) {
					// DteVersementInitial
					detailsEpargneMsg.setDateVersementInitial(detailContratBancaireMsg.getDetailsLivretJeuneMsg().getInformationsInitiales().getDteVersementInitial());
				}
				if (isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg().getInformationsComplementaires())) {
					detailsEpargneMsg.setMontantPlafond(detailContratBancaireMsg.getDetailsLivretJeuneMsg().getInformationsComplementaires().getMontantPlafond());
					// 12 Fev. 2016
					// Correction Les int�r�ts des livrets ne sont pas remont�s
					detailsEpargneMsg.setInteretsProvisionnes(detailContratBancaireMsg.getDetailsLivretJeuneMsg().getInformationsComplementaires().getInteretProvisionne());
				}
				//17 Fev. 2016
				if (isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg().getEtat()))
				detailsEpargneMsg.setMotifCloture(detailContratBancaireMsg.getDetailsLivretJeuneMsg().getEtat().getMotifCloture());
			}

			// LIVRET A
			if (isNotNull(detailContratBancaireMsg.getDetailsLivretAMsg()) && isNotNull(detailContratBancaireMsg.getDetailsLivretAMsg().getInformationsComplementaires())) {
				detailsEpargneMsg.setInteretsNonEchus(detailContratBancaireMsg.getDetailsLivretAMsg().getInformationsComplementaires().getInteretsNonEchus());
				detailsEpargneMsg.setMontantPlafond(detailContratBancaireMsg.getDetailsLivretAMsg().getInformationsComplementaires().getMontantPlafond());
			}

			// LIVRET LDD
			if (isNotNull(detailContratBancaireMsg.getDetailsLDDMsg())){
				if(isNotNull(detailContratBancaireMsg.getDetailsLDDMsg().getInformationsComplementaires())) {
				detailsEpargneMsg.setInteretsNonEchus(detailContratBancaireMsg.getDetailsLDDMsg().getInformationsComplementaires().getInteretsNonEchus());
				detailsEpargneMsg.setMontantPlafond(detailContratBancaireMsg.getDetailsLDDMsg().getInformationsComplementaires().getMontantPlafond());
				}
				//17 Fev. 2016
				if(isNotNull(detailContratBancaireMsg.getDetailsLDDMsg().getEtat()))
					detailsEpargneMsg.setMotifCloture(detailContratBancaireMsg.getDetailsLDDMsg().getEtat().getMotifCloture());
			}
			// COMPTE TITRE
			if (isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg()) && isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getEtatCompte())
					&& isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getEtatCompte().getMotifCloture())) {
				detailsEpargneMsg.setMotifCloture(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getEtatCompte().getMotifCloture());
			}
			// CEL
			if (isNotNull(detailContratBancaireMsg.getDetailsCELMsg())) {
				detailsEpargneMsg.setInteretsProvisionnes(detailContratBancaireMsg.getDetailsCELMsg().getInteretsProvisionnes());
				detailsEpargneMsg.setDroitsCEL(getDroitsCEL(detailContratBancaireMsg));
			}
			
			// LDD && CEL && PEL  
			// 19 Fev 2016 ajouter PEL QC 3592 : Attribut versementProgrammes n'est pas renseign�
			detailsEpargneMsg.setVersementsProgrammes(getVersementsProgrammes(detailContratBancaireMsg));
		}
		return detailsEpargneMsg;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg2
	 * @return
	 */
	private static VersementProgrammeMsg[] getVersementsProgrammes(DetailContratBancaireMsg detailContratBancaireMsg) {
		List<VersementProgrammeMsg> tmpList = new ArrayList<VersementProgrammeMsg>();
		VersementProgrammeMsg versementProgramme = null;
		if (getDetailsLDDMsg(detailContratBancaireMsg) != null && getDetailsLDDMsg(detailContratBancaireMsg).getListeVersements() != null) {
			for (int i = 0; i < getDetailsLDDMsg(detailContratBancaireMsg).getListeVersements().length; i++) {
				VersementCpteACpteMsg versementCpt = getDetailsLDDMsg(detailContratBancaireMsg).getListeVersements()[i];
				versementProgramme = new VersementProgrammeMsg();
				versementProgramme.setIdContratDonneurOrdre(null);
				versementProgramme.setNumCompteDonneurOrdre(versementCpt.getCpteDonneurOrdre());
				versementProgramme.setDateProchaineEcheance(versementCpt.getDteProchEcheance());
				versementProgramme.setIntituleCompteDonneurOrdre(versementCpt.getIntituleCpteDonneurOrdre());
				versementProgramme.setMontantVersement(versementCpt.getMntVersement());
				versementProgramme.setPeriodiciteVersement(versementCpt.getPeriodiciteVersement());
				tmpList.add(versementProgramme);
			}
        //Attribut versementProgrammes n'est pas renseign� pour CEL
		} else if (getDetailsCELMsg(detailContratBancaireMsg) != null && getDetailsCELMsg(detailContratBancaireMsg).getListeVersementsCEL() != null) {
			for (int i = 0; i < getDetailsCELMsg(detailContratBancaireMsg).getListeVersementsCEL().length; i++) {
				VersementCELMsg versementCEL = getDetailsCELMsg(detailContratBancaireMsg).getListeVersementsCEL()[i];
				versementProgramme = new VersementProgrammeMsg();
				versementProgramme.setIdContratDonneurOrdre(null);
				versementProgramme.setNumCompteDonneurOrdre(versementCEL.getCpteDonneurOrdre());
				versementProgramme.setDateProchaineEcheance(versementCEL.getDteProchEcheance());
				versementProgramme.setIntituleCompteDonneurOrdre(versementCEL.getIntituleCpteDonneurOrdre());
				versementProgramme.setMontantVersement(versementCEL.getMntVersement());
				versementProgramme.setPeriodiciteVersement(versementCEL.getPeriodiciteVersement());
				tmpList.add(versementProgramme);
			}
		//Attribut versementProgrammes n'est pas renseign� pour PEL
		}else 	if (getDetailsPELMsg(detailContratBancaireMsg) != null && getDetailsPELMsg(detailContratBancaireMsg).getListeVersements() != null) {
			for (int i = 0; i < getDetailsPELMsg(detailContratBancaireMsg).getListeVersements().length; i++) {
				VersementCpteACpteMsg versementCpt = getDetailsPELMsg(detailContratBancaireMsg).getListeVersements()[i];
				versementProgramme = new VersementProgrammeMsg();
				versementProgramme.setIdContratDonneurOrdre(null);
				versementProgramme.setNumCompteDonneurOrdre(versementCpt.getCpteDonneurOrdre());
				versementProgramme.setDateProchaineEcheance(versementCpt.getDteProchEcheance());
				versementProgramme.setIntituleCompteDonneurOrdre(versementCpt.getIntituleCpteDonneurOrdre());
				versementProgramme.setMontantVersement(versementCpt.getMntVersement());
				versementProgramme.setPeriodiciteVersement(versementCpt.getPeriodiciteVersement());
				tmpList.add(versementProgramme);
			}
		}
	
		return tmpList.toArray(new VersementProgrammeMsg[tmpList.size()]);
	}

	/**
	 * 
	 * @param detailContratBancaireMsg2
	 * @return
	 */
	private static DroitsCELMsg[] getDroitsCEL(DetailContratBancaireMsg detailContratBancaireMsg) {
		DroitsCELMsg[] droitsCELMsg = null;
		if (isNotNull(detailContratBancaireMsg)) {
			if (isNotNull(getDroitsAcquisCELMsg(detailContratBancaireMsg))) {
				DroitsAcquisCELMsg[] droitsAcquisCELMsg = getDroitsAcquisCELMsg(detailContratBancaireMsg);
				droitsCELMsg = new DroitsCELMsg[droitsAcquisCELMsg.length];
				DroitsCELMsg droitCELMsg = null;
				for (int i = 0; i < droitsAcquisCELMsg.length; i++) {
					droitCELMsg = new DroitsCELMsg();
					DroitsAcquisCELMsg droitAcquisCELMsg = droitsAcquisCELMsg[i];
					droitCELMsg.setDateTauxRemuneration(droitAcquisCELMsg.getDteTauxRemuneration());
					droitCELMsg.setMontant(droitAcquisCELMsg.getMontant());
					droitCELMsg.setTauxRemuneration(droitAcquisCELMsg.getTauxRemuneration());
					// droitsCELMsg
					droitsCELMsg[i] = droitCELMsg;
				}
			}
		}
		return droitsCELMsg;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg2
	 * @return
	 */
	private static InformationsPELMsg getInformationsPEL(DetailContratBancaireMsg detailContratBancaireMsg) {
		InformationsPELMsg informationsPELMsg = null;
		if (detailContratBancaireMsg != null) {
		
			if (isNotNull(getCumulsPELMsg(detailContratBancaireMsg))) {
				informationsPELMsg = new InformationsPELMsg();
				CumulsPELMsg cumulsPELMsg = getCumulsPELMsg(detailContratBancaireMsg);
				informationsPELMsg.setDroitsAPretTotal(cumulsPELMsg.getDroitsAPretTotal());
				informationsPELMsg.setInteretsHorsPrimeTotal(cumulsPELMsg.getInteretHorsPrimeTotal());
				informationsPELMsg.setInteretsTauxBancaireTotal(cumulsPELMsg.getInteretTauxBancaireTotal());
				informationsPELMsg.setInteretsHorsContratTotal(cumulsPELMsg.getIntHorsContratTotal());
				informationsPELMsg.setPrimeEtatTotal(cumulsPELMsg.getPrimeEtatTotal());
				informationsPELMsg.setVersementsTotal(cumulsPELMsg.getVersementsTotal());
			}
			if (isNotNull(getProduitPELMsg(detailContratBancaireMsg))) {
				if(informationsPELMsg == null){
				    informationsPELMsg = new InformationsPELMsg();
				}
				ProduitPELMsg produitPELMsg = getProduitPELMsg(detailContratBancaireMsg);
				informationsPELMsg.setTauxHorsPrime(produitPELMsg.getTauxHorsPrime());
				informationsPELMsg.setTauxPret(produitPELMsg.getTauxPret());
				informationsPELMsg.setTauxPrime(produitPELMsg.getTauxPrime());
				informationsPELMsg.setTauxPrime(produitPELMsg.getTauxPrime());
				informationsPELMsg.setTauxRemuneration(produitPELMsg.getTauxRemuneration());
			}
			// versementsAnnuels
			VersementsAnnuelsMsg[] versementsAnnuelsPEL=getVersementsAnnuelsPEL(detailContratBancaireMsg);
			if(versementsAnnuelsPEL!=null){
				if(informationsPELMsg == null){
				    informationsPELMsg = new InformationsPELMsg();
				}
			    informationsPELMsg.setVersementsAnnuelsPEL(versementsAnnuelsPEL);
			}
		}
		return informationsPELMsg;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg2
	 * @return
	 */
	private static VersementsAnnuelsMsg[] getVersementsAnnuelsPEL(DetailContratBancaireMsg detailContratBancaireMsg) {
		VersementsAnnuelsMsg[] versementsAnnuelsMsg = null;
		if (isNotNull(detailContratBancaireMsg)) {
			if (isNotNull(getVersementsAnnuelsPELMsg(detailContratBancaireMsg))) {
				VersementsAnnuelsPELMsg[] versementsAnnuelsPELMsg = getVersementsAnnuelsPELMsg(detailContratBancaireMsg);
				versementsAnnuelsMsg = new VersementsAnnuelsMsg[versementsAnnuelsPELMsg.length];
				VersementsAnnuelsMsg versementAnnuelsMsg = null;
				for (int i = 0; i < versementsAnnuelsPELMsg.length; i++) {
					versementAnnuelsMsg = new VersementsAnnuelsMsg();
					VersementsAnnuelsPELMsg versementAnnuelsPELMsg = versementsAnnuelsPELMsg[i];
					versementAnnuelsMsg.setAnneeVersement(versementAnnuelsPELMsg.getAnneeVersement());
					versementAnnuelsMsg.setDoitsAPret(versementAnnuelsPELMsg.getDoitsApret());
					versementAnnuelsMsg.setInteretsHorsPrime(versementAnnuelsPELMsg.getInteretHorsPrime());
					versementAnnuelsMsg.setInteretsTauxBancaire(versementAnnuelsPELMsg.getInteretTauxBancaire());
					versementAnnuelsMsg.setInteretsHorsContrat(versementAnnuelsPELMsg.getIntHorsContrat());
					versementAnnuelsMsg.setPrimeEtat(versementAnnuelsPELMsg.getPrimeEtat());
					versementAnnuelsMsg.setVersementAnnuelTotal(versementAnnuelsPELMsg.getVersementAnnuelTotal());
					// versementsAnnuelsMsg
					versementsAnnuelsMsg[i] = versementAnnuelsMsg;
				}
			}
		}
		return versementsAnnuelsMsg;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static String[] getClientsMandataires(DetailContratBancaireMsg detailContratBancaireMsg) {
		String[] clientsMandataires = null;
		if (isNotNull(detailContratBancaireMsg)) {
			if (isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg().getNumClientsMandataires()))
				clientsMandataires = detailContratBancaireMsg.getDetailsLivretJeuneMsg().getNumClientsMandataires();
			else if (isNotNull(detailContratBancaireMsg.getDetailsLivretAMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsLivretAMsg().getNumClientsMandataires()))
				clientsMandataires = detailContratBancaireMsg.getDetailsLivretAMsg().getNumClientsMandataires();
			else if (isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getDetailCompteTitre())
					&& isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getDetailCompteTitre().getNumClientsMandataires()))
				clientsMandataires = detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getDetailCompteTitre()
						.getNumClientsMandataires();
			else if (isNotNull(detailContratBancaireMsg.getDetailsLDDMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsLDDMsg().getNumClientsMandataires()))
				clientsMandataires = detailContratBancaireMsg.getDetailsLDDMsg().getNumClientsMandataires();
			else if (isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg())
					&& isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg().getDetailsPEAMsg())
					&& isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg().getDetailsPEAMsg().getNumsClientsMandataires()))
				clientsMandataires = detailContratBancaireMsg.getInfoComptePEAMsg().getDetailsPEAMsg().getNumsClientsMandataires();
			else if (isNotNull(detailContratBancaireMsg.getDetailsCSLMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsComplementaires())
					&& isNotNull(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsComplementaires().getNumClientsMandataires()))
				clientsMandataires = detailContratBancaireMsg.getDetailsCSLMsg().getInformationsComplementaires().getNumClientsMandataires();
		}
		return toIdClient(clientsMandataires);
	}

	/**
	 * 
	 * @param clientsMandataires
	 * @return
	 */
	private static String[] toIdClient(String[] clientsMandataires) {
		String[] clientIds = null;
		if (clientsMandataires != null && clientsMandataires.length != 0) {
			clientIds = new String[clientsMandataires.length];
			IdClient idClient = null;
			for (int i = 0; i < clientsMandataires.length; i++) {
				idClient = new IdClient(clientsMandataires[i], TYPECLIENT_PHYSIQUE);
				clientIds[i] = ConverterFactory.getInstance().convertStructToId(idClient);
			}
		}
		return clientIds;
	}

	/**
	 * 
	 * @param parametrageBusinessComponentEJB
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static msg.detailbancaire.contrat.apirest.axb.adapt.med.ProduitMsg getProduit(
			ParametrageBusinessComponentEJB parametrageBusinessComponentEJB, DetailContratBancaireMsg detailContratBancaireMsg) {
		msg.detailbancaire.contrat.apirest.axb.adapt.med.ProduitMsg produitMsg = null;
		if (detailContratBancaireMsg != null) {
			produitMsg = new msg.detailbancaire.contrat.apirest.axb.adapt.med.ProduitMsg();
			Integer codProduit = null;
			Integer codSousProduit = null;
			Integer codFamille = null;
			Integer codPackage = null;
			Integer codTypeCarte = null;
			Integer codTypeDebit = null;
			Integer codProduitAgrege = null;

			alimenterProduitMap(parametrageBusinessComponentEJB);

			if (isNotNull(getDonneesCarteMsg(detailContratBancaireMsg))) {
				DonneesCarteMsg donnesCartemsg = getDonneesCarteMsg(detailContratBancaireMsg);
				codProduit = donnesCartemsg.getCodProduit();
				codSousProduit = donnesCartemsg.getCodSousProduit();
				codPackage = donnesCartemsg.getCodPackage();
				codTypeCarte = donnesCartemsg.getTypeCarte();
				codTypeDebit = donnesCartemsg.getTypeDebitCarte();
			} else if (isNotNull(getCompteBancaire(detailContratBancaireMsg))) {
				CompteBancaireMsg compteBancaireMsg = getCompteBancaire(detailContratBancaireMsg);
				codProduit = compteBancaireMsg.getCodProduit();
				codSousProduit = compteBancaireMsg.getCodSousProduit();
				codFamille = compteBancaireMsg.getCodFamille();
				codPackage = compteBancaireMsg.getCodPackage();
			}

			if (codProduit != null) {
				produitMsg.setCodProduit(TYPEPRODUITMAP.get(codProduit));
				produitMsg.setCodProduitAgrege(TYPEPRODUITMAP.get(codProduit));
			}
			if (codSousProduit != null) {
				CodMsg sousProduit = getSousProduit(codProduit, codSousProduit, codFamille);
				produitMsg.setCodSousProduit(sousProduit);
			}
			if (codFamille != null) {
				produitMsg.setCodFamille(FAMILLEMAP.get(codFamille));
			}
			if (codPackage != null) {
				produitMsg.setCodPackage(PACKAGEMAP.get(codPackage));
			}
			if (codTypeCarte != null) {
				produitMsg.setCodTypeCarte(CARTEMAP.get(codTypeCarte));
			}
			if (codTypeDebit != null) {
				produitMsg.setCodTypeDebit(DEBITMAP.get(codTypeDebit));
			}
		}
		return produitMsg;
	}

	private static Map<Integer, CodMsg> getDictionnaireMap(ParametrageBusinessComponentEJB parametrageBusinessComponentEJB, String refDictionnaire) {
		Map<Integer, CodMsg> dictionnaireMap = null;
		try {
			CritereDictionaryMsg critereDictionnaire = new CritereDictionaryMsg();
			critereDictionnaire.setDicoClassName(refDictionnaire);
			critereDictionnaire.setGroupElement("1");
			DictionaryRefMsg dictionnaire = parametrageBusinessComponentEJB.lireDicoRef(critereDictionnaire);
			dictionnaireMap = parseToDictionnaireMapAsObject(dictionnaire);
		} catch (RemoteException remoteException) {
			LOGGER.error("Erreur:ListerContratBusinessComponentProcr:RemoteException" + remoteException.getMessage());
		} catch (BusinessException businessException) {
			LOGGER.error("Erreur:ListerContratBusinessComponentProcr:BusinessException" + businessException.getMessage());
		} catch (TechnicalException technicalException) {
			LOGGER.error("Erreur:ListerContratBusinessComponentProcr:TechnicalException" + technicalException.getMessage());
		}
		return dictionnaireMap;
	}

	private static Map<Integer, CodMsg> parseToDictionnaireMapAsObject(DictionaryRefMsg dictionnaire) {
		Map<Integer, CodMsg> dictionnaireMap = null;
		if (dictionnaire != null) {
			if ((dictionnaire.getDicoValue() != null) && (dictionnaire.getDicoValue().length > 0)) {
				dictionnaireMap = new HashMap<Integer, CodMsg>();
				DictionaryRefValueMsg[] listeValeurs = dictionnaire.getDicoValue();
				CodMsg libelle = null;
				for (DictionaryRefValueMsg valeur : listeValeurs) {
					libelle = new CodMsg(valeur.getIntCodeElement().intValue(), valeur.getLibElement());
					dictionnaireMap.put(new Integer(valeur.getIntCodeElement().intValue()), libelle);
				}
			}
		}
		return dictionnaireMap;
	}

	private static CodMsg getSousProduit(Integer codProduit, Integer codSousProduit, Integer codFamille) {
		CodMsg sousProduit = null;
		if (codProduit != null) {
			switch (codProduit) {
			case 1: {
				//correctif QC 3447 (sous produit ne remonte pas)
				/*if (codFamille != null) {
					if (codFamille.equals(1) || codFamille.equals(2)) {*/
						sousProduit = SOUSPRODUITCDDMAP.get(codSousProduit);
					/*} else if (codFamille.equals(3)) {
						sousProduit = SOUSPRODUITCDDTITREMAP.get(codSousProduit);
					}
				}*/
				break;
			}
			case 8: {
				sousProduit = SOUSPRODUITLIVRETMAP.get(codSousProduit);
				break;
			}
			case 9: {
				sousProduit = SOUSPRODUITCARTEMAP.get(codSousProduit);
				break;
			}
			case 10: {
				sousProduit = SOUSPRODUITDECMAP.get(codSousProduit);
				break;
			}
			case 32: {
				sousProduit = SOUSPRODUITDATMAP.get(codSousProduit);
				break;
			}
			case 41: {
				sousProduit = SOUSPRODUITPRETSPECMAP.get(codSousProduit);
				break;
			}
			case 44: {
				if (codFamille != null) {
					if (codFamille.equals(1)) {
						sousProduit = SOUSPRODUITPMOCDDMAP.get(codSousProduit);
					} else if (codFamille.equals(3)) {
						sousProduit = SOUSPRODUITPMOTITREMAP.get(codSousProduit);
					}
				}
				break;
			}
			case 50: {
				sousProduit = SOUSPRODUITPPMAP.get(codSousProduit);
				break;
			}
			case 51: {
				sousProduit = SOUSPRODUITRAMAP.get(codSousProduit);
				break;
			}
			}
		}
		return sousProduit;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static DetailsCarteMsg getDetailsCarte(DetailContratBancaireMsg detailContratBancaireMsg) {
		DetailsCarteMsg detailsCarteMsg = null;
		if (detailContratBancaireMsg != null) {
			detailsCarteMsg = new DetailsCarteMsg();
			detailsCarteMsg.setIdContrat(getIdContratFromDonneesCarte(detailContratBancaireMsg));
			if (isNotNull(getDonneesCarteMsg(detailContratBancaireMsg))) {
				DonneesCarteMsg donneesCarteMsg = getDonneesCarteMsg(detailContratBancaireMsg);
				detailsCarteMsg.setNumCarte(getNumeroCarteDetailCarte(donneesCarteMsg.getNumCarte()));
				detailsCarteMsg.setDateCreation(donneesCarteMsg.getDteCreation());
				detailsCarteMsg.setDateCloture(donneesCarteMsg.getDteCloture());
				detailsCarteMsg.setLibelle(donneesCarteMsg.getLibelleCarte());
				detailsCarteMsg.setEncoursMontant(donneesCarteMsg.getEncoursMnt() != null ? donneesCarteMsg.getEncoursMnt().doubleValue() : null);
				detailsCarteMsg.setCodVisuel(new CodMsg(donneesCarteMsg.getVisuelCarte(), null));
			}
			if (isNotNull(getCompteurCarteMsg(detailContratBancaireMsg))) {
				CompteurCarteMsg compteurCarteMsg = getCompteurCarteMsg(detailContratBancaireMsg);
				detailsCarteMsg.setAchatCarteCourants(compteurCarteMsg.getAchatCarteCourants());
				detailsCarteMsg.setDateEcheance(compteurCarteMsg.getDteEcheance());
				detailsCarteMsg.setEnCours(compteurCarteMsg.getEnCours());
			}
			if (isNotNull(getCompteurRetraitCarteMsg(detailContratBancaireMsg))) {
				CompteurRetraitCarteMsg compteurRetraitCarteMsg = getCompteurRetraitCarteMsg(detailContratBancaireMsg);
				detailsCarteMsg.setRetraitAnneeEnCours(compteurRetraitCarteMsg.getRetraitAnneeEnCours() != null ? compteurRetraitCarteMsg
						.getRetraitAnneeEnCours().doubleValue() : null);
				detailsCarteMsg.setRetraitAnneePrecedente(compteurRetraitCarteMsg.getRetraitAnneePrecedente() != null ? compteurRetraitCarteMsg
						.getRetraitAnneePrecedente().doubleValue() : null);
				detailsCarteMsg.setRetraitMoisEnCours(compteurRetraitCarteMsg.getRetraitMoisEnCours() != null ? compteurRetraitCarteMsg
						.getRetraitMoisEnCours().doubleValue() : null);
				detailsCarteMsg.setRetraitMoisPrecedent(compteurRetraitCarteMsg.getRetraitMoisPrecedent() != null ? compteurRetraitCarteMsg
						.getRetraitMoisPrecedent().doubleValue() : null);
			}
			if (isNotNull(getPlafondCarteMsg(detailContratBancaireMsg))) {
				PlafondCarteMsg plafondCarteMsg = getPlafondCarteMsg(detailContratBancaireMsg);
				detailsCarteMsg.setMontantPaiementEtranger(plafondCarteMsg.getMntPaiementEtranger());
				detailsCarteMsg.setMontantPaiementFrance(plafondCarteMsg.getMntPaiementFrance());
				detailsCarteMsg.setMontantRetraitBanque(plafondCarteMsg.getMntRetraitBnp());
				detailsCarteMsg.setMontantRetraitEtranger(plafondCarteMsg.getMntRetraitEtranger());
				detailsCarteMsg.setMontantRetraitFrance(plafondCarteMsg.getMntRetraitFrance());
				detailsCarteMsg.setMontantPlafond(plafondCarteMsg.getMontantPlafond());
				detailsCarteMsg.setNiveauService(new CodMsg(Integer.valueOf(plafondCarteMsg.getNiveauService()), null));

			}
		}
		return detailsCarteMsg;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static String getIdContratFromDonneesCarte(DetailContratBancaireMsg detailContratBancaireMsg) {
		String idContrat = null;
		if (isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getDetailsCarteMsg())
				&& isNotNull(detailContratBancaireMsg.getDetailsCarteMsg().getDonneesCarte())) {
			DonneesCarteMsg donneesCarteMsg = detailContratBancaireMsg.getDetailsCarteMsg().getDonneesCarte();
			IdContrat id = new IdContrat(donneesCarteMsg.getNumContrat(), null, donneesCarteMsg.getCodProduit(), donneesCarteMsg.getCodSousProduit(),
					donneesCarteMsg.getCodPackage(), donneesCarteMsg.getNumCompte());
			idContrat = ConverterFactory.getInstance().convertStructToId(id);
		}
		return idContrat;
	}

	// //////////////////////////////// DES GETTER des diff�rets objets
	// imbriqu�s
	private static DetailsCompteTitreMsg getDetailsCompteTitreMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg())
				&& isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getDetailCompteTitre()) ? detailContratBancaireMsg
				.getDetailCompteTitreMsgEtatCompteMsg().getDetailCompteTitre() : null;
	}

	private static InformationsFiscalesPEAMsg getInformationsFiscalesPEAMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg())
				&& isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg().getInformationsFiscales()) ? detailContratBancaireMsg
				.getInfoComptePEAMsg().getInformationsFiscales() : null;
	}

	private static InformationsFiscalesCompteTitreMsg getInformationsFiscalesCompteTitreMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg())
				&& isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getInformationsFiscales()) ? detailContratBancaireMsg
				.getDetailCompteTitreMsgEtatCompteMsg().getInformationsFiscales() : null;
	}

	private static fr.axabanque.crm.tarification.api.msg.CashBackAssuranceMsg[] getCashBackAssuranceMsg(
			DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getDetailCompteCourantMsg()) ? detailContratBancaireMsg
				.getDetailCompteCourantMsg().getCashBackAssuranceMsg() : null;
	}

	private static fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.api.msg.CumulCarteMsg[] getCumulCarteEnCoursMsg(
			DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getCashBackCompte()) ? detailContratBancaireMsg
				.getCashBackCompte().getCumulsCartesEncours() : null;
	}

	private static fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.api.msg.CumulCarteMsg[] getCumulCarteAnneePrecMsg(
			DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getCashBackCompte()) ? detailContratBancaireMsg
				.getCashBackCompte().getCumulsCartesAnneePrecedente() : null;
	}

	private static fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.api.msg.CumulCarteMsg[] getCumulCarteMsg(
			DetailContratBancaireMsg detailContratBancaireMsg) {
		fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.api.msg.CumulCarteMsg[] enCours = getCumulCarteEnCoursMsg(detailContratBancaireMsg);
		fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.api.msg.CumulCarteMsg[] prec = getCumulCarteAnneePrecMsg(detailContratBancaireMsg);
		if (enCours == null)
			return prec;
		if (prec == null)
			return enCours;
		System.arraycopy(enCours, 0, prec, 0, enCours.length);
		return prec;
	}

	private static DetailsCompteDepotATermeMsg getDetailsCompteDepotATermeMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getDetailsCompteDepotATermeMsg()) ? detailContratBancaireMsg
				.getDetailsCompteDepotATermeMsg() : null;
	}

	private static CashBackCompteMsg getCashBackCompteMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getCashBackCompte()) ? detailContratBancaireMsg
				.getCashBackCompte() : null;
	}

	private static DroitsAcquisCELMsg[] getDroitsAcquisCELMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getDetailsCELMsg())
				&& isNotNull(detailContratBancaireMsg.getDetailsCELMsg().getListeDroitsAcquisCEL()) ? detailContratBancaireMsg.getDetailsCELMsg()
				.getListeDroitsAcquisCEL() : null;
	}

	private static VersementsAnnuelsPELMsg[] getVersementsAnnuelsPELMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getDetailsPELMsg())
				&& isNotNull(detailContratBancaireMsg.getDetailsPELMsg().getListeVersementsAnnuels()) ? detailContratBancaireMsg.getDetailsPELMsg()
				.getListeVersementsAnnuels() : null;
	}

	private static CumulsPELMsg getCumulsPELMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getDetailsPELMsg())
				&& isNotNull(detailContratBancaireMsg.getDetailsPELMsg().getCumulPEL()) ? detailContratBancaireMsg.getDetailsPELMsg().getCumulPEL()
				: null;
	}

	private static ProduitPELMsg getProduitPELMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getDetailsPELMsg())
				&& isNotNull(detailContratBancaireMsg.getDetailsPELMsg().getCaracteristiquesProduitPEL()) ? detailContratBancaireMsg
				.getDetailsPELMsg().getCaracteristiquesProduitPEL() : null;
	}

	private static DetailsCSLMsg getDetailsCSLMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getDetailsCSLMsg()) ? detailContratBancaireMsg
				.getDetailsCSLMsg() : null;
	}

	private static DetailsCELMsg getDetailsCELMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getDetailsCELMsg()) ? detailContratBancaireMsg
				.getDetailsCELMsg() : null;
	}

	private static DetailsLDDMsg getDetailsLDDMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getDetailsLDDMsg()) ? detailContratBancaireMsg
				.getDetailsLDDMsg() : null;
	}

	private static DetailsPELMsg getDetailsPELMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getDetailsPELMsg()) ? detailContratBancaireMsg
				.getDetailsPELMsg() : null;
	}

	private static PlafondCarteMsg getPlafondCarteMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getDetailsCarteMsg())
				&& isNotNull(detailContratBancaireMsg.getDetailsCarteMsg().getPlafond()) ? detailContratBancaireMsg.getDetailsCarteMsg().getPlafond()
				: null;
	}

	private static CompteurRetraitCarteMsg getCompteurRetraitCarteMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg) && isNotNull(detailContratBancaireMsg.getDetailsCarteMsg())
				&& isNotNull(detailContratBancaireMsg.getDetailsCarteMsg().getCompteurRetraitCarte()) ? detailContratBancaireMsg.getDetailsCarteMsg()
				.getCompteurRetraitCarte() : null;
	}

	private static String getNumeroCarteDetailCarte(String numCarte) {
		return masquerNumeroCarte(numCarte);
	}

	private static String masquerNumeroCarte(String numCarte) {
		if (numCarte != null && numCarte.length() == 16) {
			return MASQUE + numCarte.substring(12);
		}
		return null;
	}

	private static DonneesCarteMsg getDonneesCarteMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg.getDetailsCarteMsg()) && isNotNull(detailContratBancaireMsg.getDetailsCarteMsg().getDonneesCarte()) ? detailContratBancaireMsg
				.getDetailsCarteMsg().getDonneesCarte()
				: null;
	}

	private static CompteurCarteMsg getCompteurCarteMsg(DetailContratBancaireMsg detailContratBancaireMsg) {
		return isNotNull(detailContratBancaireMsg.getDetailsCarteMsg())
				&& isNotNull(detailContratBancaireMsg.getDetailsCarteMsg().getCompteurCarte()) ? detailContratBancaireMsg.getDetailsCarteMsg()
				.getCompteurCarte() : null;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static Date getDateCloture(DetailContratBancaireMsg detailContratBancaireMsg) {
		Date dateCloture = null;
		if (isNotNull(detailContratBancaireMsg)) {
			if (isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getEtatCompte())
					&& isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getEtatCompte().getDteCloture()))
				dateCloture = detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getEtatCompte().getDteCloture();
			else if (isNotNull(detailContratBancaireMsg.getDetailsCarteMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsCarteMsg().getDonneesCarte())
					&& isNotNull(detailContratBancaireMsg.getDetailsCarteMsg().getDonneesCarte().getDteCloture()))
				dateCloture = detailContratBancaireMsg.getDetailsCarteMsg().getDonneesCarte().getDteCloture();
			else if (isNotNull(detailContratBancaireMsg.getDetailsCSLMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsComplementaires())
					&& isNotNull(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsComplementaires().getDteCloture()))
				dateCloture = detailContratBancaireMsg.getDetailsCSLMsg().getInformationsComplementaires().getDteCloture();
		}
		return dateCloture;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static Double getMontantDecouvert(DetailContratBancaireMsg detailContratBancaireMsg) {
		Double montantDecouvert = null;
		if (detailContratBancaireMsg != null) {
			CompteBancaireMsg compte = getCompteBancaire(detailContratBancaireMsg);
			if (isNotNull(compte)) {
				montantDecouvert = compte.getMontantDecouvert();
			}
		}
		return montantDecouvert;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static CodMsg getBlocageDecouvert(DetailContratBancaireMsg detailContratBancaireMsg) {
		CodMsg blocageDecouvert = null;
		if (detailContratBancaireMsg != null) {
			CompteBancaireMsg compte = getCompteBancaire(detailContratBancaireMsg);
			if (isNotNull(compte)) {
				try {
					blocageDecouvert = new CodMsg(Integer.valueOf(compte.getBlocageDecouvert()), null);
				} catch (NumberFormatException nfe) {
					LOGGER.error("Error de de la transformation de la valeur [" + compte.getBlocageDecouvert()
							+ "] en valeur enti�re pour le compte num�ro : " + compte.getNumCompte());
					blocageDecouvert = null;
				}
			}
		}
		return blocageDecouvert;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static Date getDateOuverture(DetailContratBancaireMsg detailContratBancaireMsg) {
		Date dateOuverture = null;
		if (isNotNull(detailContratBancaireMsg)) {
			if (isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg().getInformationsComplementaires())
					&& isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg().getInformationsComplementaires().getDteOuverture()))
				dateOuverture = detailContratBancaireMsg.getDetailsLivretJeuneMsg().getInformationsComplementaires().getDteOuverture();
			else if (isNotNull(detailContratBancaireMsg.getDetailsLivretAMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsLivretAMsg().getInformationsComplementaires())
					&& isNotNull(detailContratBancaireMsg.getDetailsLivretAMsg().getInformationsComplementaires().getDteOuverture()))
				dateOuverture = detailContratBancaireMsg.getDetailsLivretAMsg().getInformationsComplementaires().getDteOuverture();
			else if (isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getDetailCompteTitre())
					&& isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getDetailCompteTitre().getDteOuverture()))
				dateOuverture = detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getDetailCompteTitre().getDteOuverture();
			else if (isNotNull(detailContratBancaireMsg.getDetailsLDDMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsLDDMsg().getInformationsComplementaires())
					&& isNotNull(detailContratBancaireMsg.getDetailsLDDMsg().getInformationsComplementaires().getDteOuverture()))
				dateOuverture = detailContratBancaireMsg.getDetailsLDDMsg().getInformationsComplementaires().getDteOuverture();
			else if (isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg())
					&& isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg().getDetailsPEAMsg())
					&& isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg().getDetailsPEAMsg().getDteOuverture()))
				dateOuverture = detailContratBancaireMsg.getInfoComptePEAMsg().getDetailsPEAMsg().getDteOuverture();
			else if (isNotNull(detailContratBancaireMsg.getDetailsCSLMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsComplementaires())
					&& isNotNull(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsComplementaires().getDteOuverture()))
				dateOuverture = detailContratBancaireMsg.getDetailsCSLMsg().getInformationsComplementaires().getDteOuverture();
		}
		return dateOuverture;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static String getIntitule(DetailContratBancaireMsg detailContratBancaireMsg) {
		String intitule = null;
		if (detailContratBancaireMsg != null) {
			CompteBancaireMsg compte = getCompteBancaire(detailContratBancaireMsg);
			if (isNotNull(compte))
				intitule = compte.getCompteIntitule();
			else if (isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg().getInformationsComplementaires())
					&& isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg().getInformationsComplementaires().getIntitule()))
				intitule = detailContratBancaireMsg.getDetailsLivretJeuneMsg().getInformationsComplementaires().getIntitule();
			else if (isNotNull(detailContratBancaireMsg.getDetailsLivretAMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsLivretAMsg().getInformationsComplementaires())
					&& isNotNull(detailContratBancaireMsg.getDetailsLivretAMsg().getInformationsComplementaires().getIntitule()))
				intitule = detailContratBancaireMsg.getDetailsLivretAMsg().getInformationsComplementaires().getIntitule();
			else if (isNotNull(detailContratBancaireMsg.getDetailsLDDMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsLDDMsg().getInformationsComplementaires())
					&& isNotNull(detailContratBancaireMsg.getDetailsLDDMsg().getInformationsComplementaires().getIntitule()))
				intitule = detailContratBancaireMsg.getDetailsLDDMsg().getInformationsComplementaires().getIntitule();
			else if (isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg())
					&& isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg().getCompteSupportMsg())
					&& isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg().getCompteSupportMsg().getIntituleCompte()))
				intitule = detailContratBancaireMsg.getInfoComptePEAMsg().getCompteSupportMsg().getIntituleCompte();
			else if (isNotNull(detailContratBancaireMsg.getCashBackCompte())
					&& isNotNull(detailContratBancaireMsg.getCashBackCompte().getIntituleCompte()))
				intitule = detailContratBancaireMsg.getCashBackCompte().getIntituleCompte();
		}
		return intitule;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static Boolean getGestionSousMandatBln(DetailContratBancaireMsg detailContratBancaireMsg) {
		Boolean gestionSousMandatBln = null;
		CompteBancaireMsg compte = getCompteBancaire(detailContratBancaireMsg);
		if (isNotNull(compte))
			gestionSousMandatBln = compte.getEstGestionSousMandat();
		return gestionSousMandatBln;
	}

	private static Double getSolde(DetailContratBancaireMsg detailContratBancaireMsg) {
		Double solde = null;
		CompteBancaireMsg compte = getCompteBancaire(detailContratBancaireMsg);
		if (isNotNull(compte))
			solde = compte.getSolde();
		return solde;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static CodMsg getDevise(DetailContratBancaireMsg detailContratBancaireMsg) {
		CodMsg devise = null;
		if (detailContratBancaireMsg != null) {
			if (isNotNull(detailContratBancaireMsg.getDetailCompteCourantMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailCompteCourantMsg().getCompteBancaire()))
				devise = getCodMsgForCode(detailContratBancaireMsg.getDetailCompteCourantMsg().getCompteBancaire().getDevise());
			else if(isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg())&&isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getDetailCompteTitre())&&isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getDetailCompteTitre().getCompteBancaireMsg()))
				devise = getCodMsgForCode(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getDetailCompteTitre().getCompteBancaireMsg().getDevise());
			else if(isNotNull(detailContratBancaireMsg.getDetailsCompteDepotATermeMsg())&&isNotNull(detailContratBancaireMsg.getDetailsCompteDepotATermeMsg().getCompteBancaireMsg())&&isNotNull(detailContratBancaireMsg.getDetailsCompteDepotATermeMsg().getCompteBancaireMsg().getDevise()))
				devise = getCodMsgForCode(detailContratBancaireMsg.getDetailsCompteDepotATermeMsg().getCompteBancaireMsg().getDevise());
			else if(isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg().getInformationsInitiales()))
				devise = getCodMsgForCode(detailContratBancaireMsg.getDetailsLivretJeuneMsg().getInformationsInitiales().getDevise());
			else if (isNotNull(detailContratBancaireMsg.getDetailsCarteMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsCarteMsg().getDonneesCarte()))
				devise = getCodMsgForCode(detailContratBancaireMsg.getDetailsCarteMsg().getDonneesCarte().getCodDevise());
			else if (isNotNull(detailContratBancaireMsg.getDetailsLivretAMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsLivretAMsg().getInformationsInitiales()))
				devise = getCodMsgForCode(detailContratBancaireMsg.getDetailsLivretAMsg().getInformationsInitiales().getDevise());
			else if (isNotNull(detailContratBancaireMsg.getDetailsLDDMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsLDDMsg().getInformationsInitiales()))
				devise = getCodMsgForCode(detailContratBancaireMsg.getDetailsLDDMsg().getInformationsInitiales().getDevise());
			else if (isNotNull(detailContratBancaireMsg.getDetailsCELMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsCELMsg().getCompte()))
				devise = getCodMsgForCode(detailContratBancaireMsg.getDetailsCELMsg().getCompte().getDevise());
			else if (isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg())
					&& isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg().getDetailsPEAMsg())
					&& isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg().getDetailsPEAMsg().getInformationsInitiales()))
				devise = getCodMsgForCode(detailContratBancaireMsg.getInfoComptePEAMsg().getDetailsPEAMsg().getInformationsInitiales().getDevise());
			else if (isNotNull(detailContratBancaireMsg.getDetailsCSLMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsInitiales()))
				devise = getCodMsgForCode(detailContratBancaireMsg.getDetailsCSLMsg().getInformationsInitiales().getDevise());
			else if (isNotNull(detailContratBancaireMsg.getDetailsPELMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsPELMsg().getDetailCompte()))
				devise = getCodMsgForCode(detailContratBancaireMsg.getDetailsPELMsg().getDetailCompte().getDevise());
		}
		return devise;
	}

	private static CodMsg getCodMsgForCode(Integer code) {
		return new CodMsg(code, null);
	}

	/**
	 * R�cup�re l'objet compte bancaire selon plusieurs compte
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static CompteBancaireMsg getCompteBancaire(DetailContratBancaireMsg detailContratBancaireMsg) {
		CompteBancaireMsg compte = null;
		if (isNotNull(detailContratBancaireMsg)) {
			if (isNotNull(detailContratBancaireMsg.getDetailCompteCourantMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailCompteCourantMsg().getCompteBancaire())) {
				System.out.println("getCompteBancaire:case CompteCourant");
				compte = detailContratBancaireMsg.getDetailCompteCourantMsg().getCompteBancaire();
			} else if (isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getDetailCompteTitre())
					&& isNotNull(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getDetailCompteTitre().getCompteBancaireMsg())) {
				System.out.println("getCompteBancaire:case CompteTitre");
				compte = detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg().getDetailCompteTitre().getCompteBancaireMsg();
			} else if (isNotNull(detailContratBancaireMsg.getDetailsCELMsg()) && isNotNull(detailContratBancaireMsg.getDetailsCELMsg().getCompte())) {
				System.out.println("getCompteBancaire:case CEL");
				compte = detailContratBancaireMsg.getDetailsCELMsg().getCompte();
			} else if (isNotNull(detailContratBancaireMsg.getDetailsCompteDepotATermeMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsCompteDepotATermeMsg().getCompteBancaireMsg())) {
				System.out.println("getCompteBancaire:case CompteDepotATerme");
				compte = detailContratBancaireMsg.getDetailsCompteDepotATermeMsg().getCompteBancaireMsg();
			} else if (isNotNull(detailContratBancaireMsg.getDetailsCSLMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsCSLMsg().getDetailCompte())) {
				System.out.println("getCompteBancaire:case CSL");
				compte = detailContratBancaireMsg.getDetailsCSLMsg().getDetailCompte();
			} else if (isNotNull(detailContratBancaireMsg.getDetailsLDDMsg()) && isNotNull(detailContratBancaireMsg.getDetailsLDDMsg().getCompte())) {
				System.out.println("getCompteBancaire:case LDD");
				compte = detailContratBancaireMsg.getDetailsLDDMsg().getCompte();
			} else if (isNotNull(detailContratBancaireMsg.getDetailsLivretAMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsLivretAMsg().getCompte())) {
				System.out.println("getCompteBancaire:case Livret A");
				compte = detailContratBancaireMsg.getDetailsLivretAMsg().getCompte();
			} else if (isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsLivretJeuneMsg().getDetailCompte())) {
				System.out.println("getCompteBancaire:case Livret Jeune");
				compte = detailContratBancaireMsg.getDetailsLivretJeuneMsg().getDetailCompte();
			} else if (isNotNull(detailContratBancaireMsg.getDetailsPELMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsPELMsg().getDetailCompte())) {
				System.out.println("getCompteBancaire:case PEL");
				compte = detailContratBancaireMsg.getDetailsPELMsg().getDetailCompte();
			} else if (isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg())
					&& isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg().getDetailsPEAMsg())
					&& isNotNull(detailContratBancaireMsg.getInfoComptePEAMsg().getDetailsPEAMsg().getDetailCompte())) {
				System.out.println("getCompteBancaire:case PEA");
				compte = detailContratBancaireMsg.getInfoComptePEAMsg().getDetailsPEAMsg().getDetailCompte();
			}
		}

		return compte;
	}

	/**
	 * 
	 * @param detailContratBancaireMsg
	 * @return
	 */
	private static String getNumero(DetailContratBancaireMsg detailContratBancaireMsg) {
		String numero = null;
		boolean isCarte = false;
		if (detailContratBancaireMsg != null) {
			if (isNotNull(detailContratBancaireMsg.getNumCompte()))
				numero = detailContratBancaireMsg.getNumCompte();
			else if (isNotNull(detailContratBancaireMsg.getCashBackCompte())
					&& isNotNull(detailContratBancaireMsg.getCashBackCompte().getNumCompte()))
				numero = detailContratBancaireMsg.getCashBackCompte().getNumCompte();
			else if (isNotNull(detailContratBancaireMsg.getDetailCompteCourantMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailCompteCourantMsg().getCompteBancaire()))
				numero = detailContratBancaireMsg.getDetailCompteCourantMsg().getCompteBancaire().getNumCompte();
			else if (isNotNull(detailContratBancaireMsg.getDetailsCarteMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsCarteMsg().getDonneesCarte())) {
				numero = detailContratBancaireMsg.getDetailsCarteMsg().getDonneesCarte().getNumCarte();
				isCarte = true;
			} else if (isNotNull(detailContratBancaireMsg.getDetailsCELMsg()) && isNotNull(detailContratBancaireMsg.getDetailsCELMsg().getCompte())
					&& isNotNull(detailContratBancaireMsg.getDetailsCELMsg().getCompte().getNumCompte()))
				numero = detailContratBancaireMsg.getDetailsCELMsg().getCompte().getNumCompte();
			else if (isNotNull(detailContratBancaireMsg.getDetailsCompteDepotATermeMsg())
					&& isNotNull(detailContratBancaireMsg.getDetailsCompteDepotATermeMsg().getNumCompte()))
				numero = detailContratBancaireMsg.getDetailsCompteDepotATermeMsg().getNumCompte();
		}
		if (isCarte && numero != null && numero.length() == 16) {
			return MASQUE + numero.substring(12);
		}
		return numero;
	}

	/**
	 * R�cup�re l'entr�e code action duis le tableau
	 * 
	 * @param dictionaryRefValueMsg
	 * @param cleAction
	 * @return
	 * @throws BusinessException
	 * @throws TechnicalException
	 * @throws RemoteException
	 */
	private static Map<Integer, String> getDicoRefCodeElementForCodeRole(ParametrageBusinessComponentEJB parametrageBusinessComponentEJB)
			throws BusinessException, RemoteException, TechnicalException {
		Map<Integer, String> map = new HashMap<Integer, String>();
		CritereDictionaryMsg critereDictionaryMsg = new CritereDictionaryMsg();
		critereDictionaryMsg.setDicoClassName(REF_TYPE_ROLE_CREDIT_DESC);
		critereDictionaryMsg.setGroupElement("1");
		DictionaryRefMsg dictionaryRefMsg;
		try {
			dictionaryRefMsg = parametrageBusinessComponentEJB.lireDicoRef(critereDictionaryMsg);
		} catch (BusinessException e) {
			throw new BusinessException("Erreur lors de l'appel du Param�trage lors de la lecture de la table DICOREF");
		}
		if (dictionaryRefMsg != null) {
			DictionaryRefValueMsg[] dictionaryRefValueMsg = dictionaryRefMsg.getDicoValue();
			for (int i = 0; i < dictionaryRefValueMsg.length; i++) {
				DictionaryRefValueMsg dicoRefValueMsg = dictionaryRefValueMsg[i];
				map.put(dicoRefValueMsg.getIntCodeElement().intValue(), dicoRefValueMsg.getLibElement());
			}
		}
		return map;
	}

	/**
	 * 
	 * @param contratCreditMsg
	 * @return
	 */
	private static boolean notNullContratCredit(ContratCreditMsg contratCreditMsg) {
		if (isNull(contratCreditMsg) || isNull(contratCreditMsg.getContratCreditPersonneMsgs()))
			return false;
		return true;
	}

	/**
	 * 
	 * @param pretSpecialiseMsg
	 * @return
	 */
	private static boolean notNullPretSpecialise(PretSpecialiseMsg pretSpecialiseMsg) {
		if (isNull(pretSpecialiseMsg) || isNull(pretSpecialiseMsg.getDetailCredit()))
			return false;
		return true;
	}

	/**
	 * Test si le tableau d'object est null ou pas
	 * 
	 * @param objects
	 * @return
	 */
	private static boolean notNullInfoPartenanire(ListeInfoPartenaireCreditMsg listeInfoPartenaireCreditMsg) {
		if (isNull(listeInfoPartenaireCreditMsg) || isNull(listeInfoPartenaireCreditMsg.getInfoPartenaireCreditPrincipal()))
			return Boolean.FALSE;
		return true;
	}

	/**
	 * 
	 * @param object
	 * @return
	 */
	private static boolean isNull(Object object) {
		return object == null;
	}

	/**
	 * 
	 * @param objects
	 * @return
	 */
	private static boolean isNotNull(Object... objects) {
		if (objects == null)
			return false;
		for (int i = 0; i < objects.length; i++) {
			if (objects[i] == null)
				return false;
		}
		return true;
	}

	/**
	 * 
	 * @param visuAbnEpargneBln
	 * @return
	 */
	private static Boolean toBoolean(Integer visuAbnEpargneBln) {
		if (visuAbnEpargneBln == null)
			return Boolean.FALSE;
		if (new Integer(1).equals(visuAbnEpargneBln))
			return Boolean.TRUE;
		return Boolean.FALSE;
	}
}
