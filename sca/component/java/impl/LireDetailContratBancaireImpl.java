package sca.component.java.impl;

import static fr.axb.lib.exchange.retour.msg.CodeRetourFactory.RETOUR_CORRECT;

import java.rmi.RemoteException;

import javax.xml.namespace.QName;

import listercontrat.msg.util.ContratUtil;
import msg.detailbancaire.contrat.apirest.axb.adapt.med.DetailsContratBancaireMsg;
import msg.detailbancaire.contrat.apirest.axb.adapt.med.LireDetailContratBancaireMsg;

import com.ibm.wbiserver.mediation.jtow.SDOJavaObjectMediator;

import commonj.sdo.DataObject;

import fr.axabanque.crm.comptebancaire.api.msg.AGRDetailCompteTitreMsgEtatCompteMsg;
import fr.axabanque.crm.comptebancaire.api.msg.ComplementLivretAMsg;
import fr.axabanque.crm.comptebancaire.api.msg.CompteBancaireMsg;
import fr.axabanque.crm.comptebancaire.api.msg.DetailsCarteMsg;
import fr.axabanque.crm.comptebancaire.api.msg.DetailsCompteDepotATermeMsg;
import fr.axabanque.crm.comptebancaire.api.msg.DetailsCompteTitreMsg;
import fr.axabanque.crm.comptebancaire.api.msg.DetailsLivretAMsg;
import fr.axabanque.crm.comptebancaire.api.msg.EtatCompteMsg;
import fr.axabanque.crm.comptebancaire.api.msg.InformationsFiscalesCompteTitreMsg;
import fr.axabanque.crm.comptebancaire.api.msg.VersementCpteACpteMsg;
import fr.axabanque.crm.tarification.api.msg.CashBackAssuranceMsg;
import fr.axabanque.framework.exceptions.BusinessException;
import fr.axabanque.framework.exceptions.TechnicalException;
import fr.axabanque.med.axb.apirest.common.excepions.HeaderSoapException;
import fr.axabanque.med.axb.apirest.common.excepions.InputParamException;
import fr.axabanque.med.axb.apirest.common.soap.parser.HeaderSoap;
import fr.axabanque.med.axb.apirest.common.validator.ParameterValidator;
import fr.axabanque.med.axb.apirest.common.validator.checks.IdRestControl;
import fr.axabanque.med.axb.apirest.common.validator.checks.MandatoryFieldControl;
import fr.axabanque.med.axb.apirest.common.validator.checks.MaxLengthControl;
import fr.axabanque.pilotage.axb.webclient.liredetailcomptecourant.api.msg.DetailCompteCourantMsg;
import fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.api.msg.CritereContratBancaireMsg;
import fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.api.msg.DetailContratBancaireMsg;
import fr.axb.lib.apirest.IdInfo;
import fr.axb.lib.apirest.IdRest;
import fr.axb.lib.apirest.msg.IdClient;
import fr.axb.lib.exchange.retour.msg.CodMsg;
import fr.axb.lib.exchange.retour.msg.CodeRetourFactory;
import fr.axb.lib.exchange.retour.msg.RetourMsg;

/**
 * 
 * @author naje99k
 *
 */
public class LireDetailContratBancaireImpl {
	private static final String NUMCLIENT  = "numClient";
	private static final String CODPRODUIT  = "codProduit";
	private static final String CODROLE     = "codRole";
	private ContratBusinessComponentImpl contratBusinessComponentImpl = null;
	/**
	 * Constructeur valu�
	 * @param contratBusinessComponentImpl
	 */
	public LireDetailContratBancaireImpl(ContratBusinessComponentImpl contratBusinessComponentImpl){
		this.contratBusinessComponentImpl = contratBusinessComponentImpl;
	}
	
	/**
	 * Method generated to support implementation of operation "lireDetailContratBancaire" defined for WSDL port type 
	 * named "IContratImport".
	 * 
	 * The presence of commonj.sdo.DataObject as the return type and/or as a parameter 
	 * type conveys that it is a complex type. Please refer to the WSDL Definition for more information 
	 * on the type of input, output and fault(s).
	 */
	public DataObject lireDetailContratBancaire(DataObject lireDetailContratBancaire) {
		RetourMsg retour = RETOUR_CORRECT;
		Integer CODE_RETOUR_SM_OK = new Integer(0);
		String document = null;
		DetailsContratBancaireMsg detailsContratBancaireMsg = new DetailsContratBancaireMsg();
		try {	
			// Valider header parameters
			contratBusinessComponentImpl.validerHeaderInputParameters();
			LireDetailContratBancaireMsg lireDetailContratBancaireMsg = (msg.detailbancaire.contrat.apirest.axb.adapt.med.LireDetailContratBancaireMsg)SDOJavaObjectMediator.data2Java(lireDetailContratBancaire, "msg.detailbancaire.contrat.apirest.axb.adapt.med.LireDetailContratBancaireMsg");
			String  idContrat = lireDetailContratBancaireMsg.getIdContrat();
			String  idClient  = lireDetailContratBancaireMsg.getIdClient();
			CodMsg role = lireDetailContratBancaireMsg.getCodRole();					
			// BEGIN CHECK INPUT PARAMETERS
			IdRestControl idContratCheck = new IdRestControl(IdInfo.CTR, idContrat); 
			IdRestControl idClientCheck  = new IdRestControl(IdInfo.CLI, idClient);	
			MandatoryFieldControl codRoleCheck = new MandatoryFieldControl(3, CODROLE, role);
			ParameterValidator.inputParamControl(ParameterValidator.MSTR_HDR_ERR_120).addControl(idContratCheck).addControl(idClientCheck).addControl(codRoleCheck).doCheck();
			// BEGIN CHECK BUSINESS PARAMETERS		
			IdRest idRestContrat = contratBusinessComponentImpl.constructIdRest(idContrat);
			IdRest idRestClient  = contratBusinessComponentImpl.constructIdRest(idClient);
			//InRangeControl inRange = new InRangeControl(IdInfo.CTR.getId(), CODPRODUIT, ((IdContrat)idRestContrat).getCodProduit(), new Integer[]{new Integer(21), new Integer(30)});
			MaxLengthControl numClientCheck = new MaxLengthControl(IdInfo.CLI.getId(), NUMCLIENT, ((IdClient)idRestClient).getNumClient(), 7);	
			//ParameterValidator.inputParamControl(ParameterValidator.MSTR_HDR_ERR_901).addControl(inRange).addControl(numClientCheck).doCheck();
			ParameterValidator.inputParamControl(ParameterValidator.MSTR_HDR_ERR_901).addControl(numClientCheck).doCheck();
			// GET Header
			HeaderSoap header = contratBusinessComponentImpl.getHeaderSoap();
			Integer codRole   = role.getCode();
			// GET SO CRITERIA
			CritereContratBancaireMsg critereContratBancaireMsg = ContratUtil.parseToCritereDetailContratBancaireMsgMsgSO(idRestContrat, header);
			// CALL SO
			DetailContratBancaireMsg detailContratBancaireMsg = contratBusinessComponentImpl.locateService_LireDetailContratBancaireEJBPartner().lireDetailContratBancaire(critereContratBancaireMsg);
			afficherDetailContratBancaire(detailContratBancaireMsg);
			// SP TRANSFORMATION
			detailsContratBancaireMsg = ContratUtil.parseToSPFromSO(contratBusinessComponentImpl.locateService_ParametrageBusinessComponentEJBPartner(),contratBusinessComponentImpl.locateService_ListerActionBusinessComponentPartner(), detailContratBancaireMsg, idContrat, idRestContrat, idClient, codRole);
		} catch (RemoteException remoteException) {
			remoteException.printStackTrace();
			retour = CodeRetourFactory.RETOUR_INCIDENT_DISTANT;
			remoteException.printStackTrace();
		} 		 
		catch (InputParamException inputParamException) {
			retour = inputParamException.getRetour();
			inputParamException.printStackTrace();
		}catch (BusinessException businessException) {
			retour = CodeRetourFactory.RETOUR_INCIDENT_METIER;
			businessException.printStackTrace();
		} catch (TechnicalException technicalException) {
			/*retour = CodeRetourFactory.RETOUR_INCIDENT_TECHNIQUE;
			detailsContratBancaireMsg.setRetour(new RetourMsg(
					RetourMsg.CODE_RETOUR.KO.toString(), new CodMsg(500,
							technicalException.getMessage())));*/
			/*retour = new RetourMsg(
					RetourMsg.CODE_RETOUR.KO.toString(), new CodMsg(500,
							technicalException.getMessage()));*/
			retour = CodeRetourFactory.RETOUR_INCIDENT_TECHNIQUE;
			technicalException.printStackTrace();
		} catch (HeaderSoapException headerException) {
			retour = headerException.getRetour();
			headerException.printStackTrace();
		}catch (Exception exception) {
			/*detailsContratBancaireMsg.setRetour(new RetourMsg(
					RetourMsg.CODE_RETOUR.KO.toString(), new CodMsg(500,
							exception.getMessage())));*/
			/*retour = new RetourMsg(
					RetourMsg.CODE_RETOUR.KO.toString(), new CodMsg(500,
							exception.getMessage()));*/
			retour = CodeRetourFactory.RETOUR_INCIDENT_TECHNIQUE;
			exception.printStackTrace();
		}
		// Set le retour
		detailsContratBancaireMsg.setRetour(retour);
		return SDOJavaObjectMediator.java2Data(detailsContratBancaireMsg, new QName("http://med.adapt.axb.apirest.contrat.detailbancaire.msg", DetailsContratBancaireMsg.class.getName()));
	}

	private void afficherDetailContratBancaire(
			DetailContratBancaireMsg detailContratBancaireMsg) {
		afficherDetailCompteCourant(detailContratBancaireMsg.getDetailCompteCourantMsg());
		afficherDetailCompteTitreMsgEtatCompte(detailContratBancaireMsg.getDetailCompteTitreMsgEtatCompteMsg());
		//afficherDetailCarteMsg(detailContratBancaireMsg.getDetailsCarteMsg());
		//afficherDetailCELMsg(detailContratBancaireMsg.getDetailsCELMsg());
		afficherDetailCompteDepotATermeMsg(detailContratBancaireMsg.getDetailsCompteDepotATermeMsg());
		//afficherDetailCSLMsg(detailContratBancaireMsg.getDetailsCSLMsg());
		//afficherDetailLDDMsg(detailContratBancaireMsg.getDetailsLDDMsg());
		afficherDetailLivretAMsg(detailContratBancaireMsg.getDetailsLivretAMsg());
		//afficherdetailLivretJeuneMsg(detailContratBancaireMsg.getDetailsLivretJeuneMsg());
		//afficherDetailPELMsg(detailContratBancaireMsg.getDetailsPELMsg());
		//afficherDetailCarteTabMsg(detailContratBancaireMsg.getDonneesCarteMsg());
		//afficherInfoComptePEAMsg(detailContratBancaireMsg.getInfoComptePEAMsg());
		//afficherDetailMouvementTabMsg(detailContratBancaireMsg.getMouvementMsgs());
		System.out.println("detailContratBancaireMsg:numCompte:" + detailContratBancaireMsg.getNumCompte());
		System.out.println("detailContratBancaireMsg:valorisationPortefeuille:" + detailContratBancaireMsg.getValorisationPortefeuille());
		//afficherCashBackAssuranceTabMsg(detailContratBancaireMsg.getCashBackCompteTitre());
		//afficherCashBackCompteMsg(detailContratBancaireMsg.getCashBackCompte());
		System.out.println("detailContratBancaireMsg:codTypeAppartenance:" + detailContratBancaireMsg.getCodTypeAppartenance());	
	}
	
	private void afficherDetailCompteDepotATermeMsg(
			DetailsCompteDepotATermeMsg detailsCompteDepotATermeMsg) {
		if(detailsCompteDepotATermeMsg != null){
			System.out.println("detailContratBancaireMsg:detailsCompteDepotATermeMsg:begin");
			System.out.println("detailContratBancaireMsg:afficherDetailCompteDepotATermeMsg:cleRIBSupport:" + detailsCompteDepotATermeMsg.getCleRIBSupport());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteDepotATermeMsg:codeBanqueSupport:" + detailsCompteDepotATermeMsg.getCodeBanqueSupport());
			afficherCompteBancaire(detailsCompteDepotATermeMsg.getCompteBancaireMsg());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteDepotATermeMsg:dteEcheancePrevue:" + detailsCompteDepotATermeMsg.getDteEcheancePrevue());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteDepotATermeMsg:duree:" + detailsCompteDepotATermeMsg.getDuree()); 
			System.out.println("detailContratBancaireMsg:afficherDetailCompteDepotATermeMsg:idClient:" + detailsCompteDepotATermeMsg.getIdClient());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteDepotATermeMsg:interetVersement:" + detailsCompteDepotATermeMsg.getInteretVersement()); 
			System.out.println("detailContratBancaireMsg:afficherDetailCompteDepotATermeMsg:montantVersement:" + detailsCompteDepotATermeMsg.getMontantVersement());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteDepotATermeMsg:natureOperation:" + detailsCompteDepotATermeMsg.getNatureOperation());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteDepotATermeMsg:numCompte:" + detailsCompteDepotATermeMsg.getNumCompte()); 
			System.out.println("detailContratBancaireMsg:afficherDetailCompteDepotATermeMsg:numCompteSupport:" + detailsCompteDepotATermeMsg.getNumCompteSupport());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteDepotATermeMsg:numContrat:" + detailsCompteDepotATermeMsg.getNumContrat());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteDepotATermeMsg:optionFiscale:" + detailsCompteDepotATermeMsg.getOptionFiscale());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteDepotATermeMsg:taux:" + detailsCompteDepotATermeMsg.getTaux());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteDepotATermeMsg:etatCompte:" + detailsCompteDepotATermeMsg.getEtatCompte());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteDepotATermeMsg:numOperation:" + detailsCompteDepotATermeMsg.getNumOperation());
			System.out.println("detailContratBancaireMsg:detailsCompteDepotATermeMsg:end");
		}
	}

	private void afficherDetailLivretAMsg(DetailsLivretAMsg detailLivretA) {
		if(detailLivretA != null){
			System.out.println("detailContratBancaireMsg:detailLivretA:begin");
			afficherCompteBancaire(detailLivretA.getCompte());
			afficherEtatCompte(detailLivretA.getEtat());
			afficherInformationsComplementaires(detailLivretA.getInformationsComplementaires());
			if(detailLivretA.getInformationsInitiales() != null){
				System.out.println("detailContratBancaireMsg:afficherDetailLivretAMsg:devise:" + detailLivretA.getInformationsInitiales().getDevise());
				System.out.println("detailContratBancaireMsg:afficherDetailLivretAMsg:mntVersementInitial:" + detailLivretA.getInformationsInitiales().getMntVersementInitial());
			}
			if(detailLivretA.getListeVersements() != null){
				int index = 0;
				VersementCpteACpteMsg[] listeVersement = detailLivretA.getListeVersements();
				for(VersementCpteACpteMsg versement : listeVersement){	
					index++;
					System.out.println("detailContratBancaireMsg:afficherDetailLivretAMsg:index:" + index + ":cpteDonneurOrdre:" + versement.getCpteDonneurOrdre());
					System.out.println("detailContratBancaireMsg:afficherDetailLivretAMsg:index:" + index + ":cpteBeneficiaire:" + versement.getCpteBeneficiaire());
					System.out.println("detailContratBancaireMsg:afficherDetailLivretAMsg:index:" + index + ":dteProchEcheance:" + versement.getDteProchEcheance());
					System.out.println("detailContratBancaireMsg:afficherDetailLivretAMsg:index:" + index + ":intituleCpteDonneurOrdre:" + versement.getIntituleCpteDonneurOrdre());
					System.out.println("detailContratBancaireMsg:afficherDetailLivretAMsg:index:" + index + ":mntVersement:" + versement.getMntVersement());
					System.out.println("detailContratBancaireMsg:afficherDetailLivretAMsg:index:" + index + ":periodiciteVersement:" + versement.getPeriodiciteVersement());
				}
			}
			if(detailLivretA.getNumClientsMandataires() != null){
				int index= 0;
				String[] numClientsMandataires = detailLivretA.getNumClientsMandataires();
				for(String numClientMandataire : numClientsMandataires){
					System.out.println("detailContratBancaireMsg:afficherDetailLivretAMsg:index:" + index + ":numClientMandataire:" + numClientMandataire);
				}
			}
			System.out.println("detailContratBancaireMsg:detailLivretA:end");
		}		
	}

	private void afficherInformationsComplementaires(
			ComplementLivretAMsg informationsComplementaires) {
		if(informationsComplementaires != null){
			System.out.println("detailContratBancaireMsg:informationsComplementaires:begin");
			System.out.println("detailContratBancaireMsg:afficherInformationsComplementaires:dteArrete:" + informationsComplementaires.getDteArrete());
			System.out.println("detailContratBancaireMsg:afficherInformationsComplementaires:dteOuverture:" + informationsComplementaires.getDteOuverture());
			System.out.println("detailContratBancaireMsg:afficherInformationsComplementaires:interetsNonEchus:" + informationsComplementaires.getInteretsNonEchus());
			System.out.println("detailContratBancaireMsg:afficherInformationsComplementaires:intitule:" + informationsComplementaires.getIntitule());
			System.out.println("detailContratBancaireMsg:afficherInformationsComplementaires:montantPlafond:" + informationsComplementaires.getMontantPlafond());
			System.out.println("detailContratBancaireMsg:informationsComplementaires:end");
		}		
	}

	private void afficherDetailCarteMsg(DetailsCarteMsg detailCarte) {
		if(detailCarte != null){
			System.out.println("detailContratBancaireMsg:detailsCarteMsg:begin");
			/*afficherCompteurCarte(detailCarte.getCompteurCarte());
			affichercompteurRetraitcarte(detailCarte.getCompteurRetraitCarte());
			afficherDonneesCarte(detailCarte.getDonneesCarte());
			afficherPlafondCarte(detailCarte.getPlafond());	
			afficherCumulCarte(detailCarte.getCumul());	
			afficherCompteurcarteAnnuel(detailCarte.getCompteurCarteAnnuelCollection());*/
			System.out.println("detailContratBancaireMsg:detailsCarteMsg:end");
		}
		
	}

	private void afficherDetailCompteTitreMsgEtatCompte(
			AGRDetailCompteTitreMsgEtatCompteMsg detailCompteTitreMsgEtatCompteMsg) {
		if(detailCompteTitreMsgEtatCompteMsg != null){
			System.out.println("detailContratBancaireMsg:detailCompteTitreMsgEtatCompteMsg:begin");			
			DetailsCompteTitreMsg detailCompteTitre = detailCompteTitreMsgEtatCompteMsg.getDetailCompteTitre();
		    EtatCompteMsg etatCompte = detailCompteTitreMsgEtatCompteMsg.getEtatCompte();
		    InformationsFiscalesCompteTitreMsg informationsFiscales = detailCompteTitreMsgEtatCompteMsg.getInformationsFiscales();
		    afficherDetailCompteTitre(detailCompteTitre);
		    afficherEtatCompte(etatCompte);
		    afficherInformationsFiscales(informationsFiscales);
		    System.out.println("detailContratBancaireMsg:detailCompteTitreMsgEtatCompteMsg:end");
		}
	}

	private void afficherInformationsFiscales(
			InformationsFiscalesCompteTitreMsg informationsFiscales) {
		if(informationsFiscales != null){
			System.out.println("detailContratBancaireMsg:informationsFiscales:begin");
			System.out.println("detailContratBancaireMsg:afficherInformationsFiscales:codProduit:" + informationsFiscales.getCodProduit());
			System.out.println("detailContratBancaireMsg:afficherInformationsFiscales:mntCessionsGlobales:" + informationsFiscales.getMntCessionsGlobales());
			System.out.println("detailContratBancaireMsg:afficherInformationsFiscales:mntFrSsAbat:" + informationsFiscales.getMntFrSsAbat());
			System.out.println("detailContratBancaireMsg:afficherInformationsFiscales:mntPL:" + informationsFiscales.getMntPL());
			System.out.println("detailContratBancaireMsg:afficherInformationsFiscales:mntPlusValues:" + informationsFiscales.getMntPlusValues());
			System.out.println("detailContratBancaireMsg:afficherInformationsFiscales:mntRevenusEtAbat:" + informationsFiscales.getMntRevenusEtAbat());
			System.out.println("detailContratBancaireMsg:afficherInformationsFiscales:mntRevenusFRAbat:" + informationsFiscales.getMntRevenusFRAbat());
			System.out.println("detailContratBancaireMsg:afficherInformationsFiscales:mntRevenusPL:" + informationsFiscales.getMntRevenusPL());
			System.out.println("detailContratBancaireMsg:afficherInformationsFiscales:numContrat:" + informationsFiscales.getNumContrat());
			System.out.println("detailContratBancaireMsg:informationsFiscales:end");
		}
		
	}

	private void afficherEtatCompte(EtatCompteMsg etatCompte) {
		if(etatCompte != null){
			System.out.println("detailContratBancaireMsg:etatCompte:begin");
			System.out.println("detailContratBancaireMsg:afficherEtatCompte:codEtat:" + etatCompte.getCodEtat());
			System.out.println("detailContratBancaireMsg:afficherEtatCompte:dteCloture:" + etatCompte.getDteCloture());
			System.out.println("detailContratBancaireMsg:afficherEtatCompte:motifCloture:" + etatCompte.getMotifCloture());
			System.out.println("detailContratBancaireMsg:etatCompte:end");
		}		
	}

	private void afficherDetailCompteTitre(
			DetailsCompteTitreMsg detailCompteTitre) {
		if(detailCompteTitre != null){
			System.out.println("detailContratBancaireMsg:detailCompteTitre:begin");
			afficherCompteBancaire(detailCompteTitre.getCompteBancaireMsg());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteTitre:cumulMontant:" + detailCompteTitre.getCumulMontant());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteTitre:dteOuverture:" + detailCompteTitre.getDteOuverture());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteTitre:numClient:" + detailCompteTitre.getNumClient());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteTitre:numClientsMandataires:" + detailCompteTitre.getNumClientsMandataires());
			System.out.println("detailContratBancaireMsg:afficherDetailCompteTitre:optionFiscale:" + detailCompteTitre.getOptionFiscale());
			System.out.println("detailContratBancaireMsg:detailCompteTitre:end");
		}		
	}

	private void afficherDetailCompteCourant(
			DetailCompteCourantMsg detailCompteCourantMsg) {
		if(detailCompteCourantMsg != null){
			System.out.println("afficherDetailCompteCourant:begin");
			CashBackAssuranceMsg[] cashBackAssurancesList = detailCompteCourantMsg.getCashBackAssuranceMsg();
			int index = 0;
			if(cashBackAssurancesList != null){
				System.out.println("afficherDetailCompteCourant:CashBackAssuranceMsg[]:begin");
				for(CashBackAssuranceMsg cashBackAssurance : cashBackAssurancesList){
					index ++;
					System.out.println("afficherDetailCompteCourant:cashBackAssurancesList:dteOperation:" + index + ":" + cashBackAssurance.getDteOperation());
					System.out.println("afficherDetailCompteCourant:libProduitAssurance:libProduitAssurance:" + index + ":" + cashBackAssurance.getLibProduitAssurance());
					System.out.println("afficherDetailCompteCourant:libProduitAssurance:mntCashBack:" + index + ":" + cashBackAssurance.getMntCashBack());
					System.out.println("afficherDetailCompteCourant:libProduitAssurance:mntNetAnnuel:" + index + ":" + cashBackAssurance.getMntNetAnnuel());
					System.out.println("afficherDetailCompteCourant:libProduitAssurance:numContratAssurance:" + index + ":" + cashBackAssurance.getNumContratAssurance());
					System.out.println("afficherDetailCompteCourant:libProduitAssurance:tauxRetrocession:" + index + ":" + cashBackAssurance.getTauxRetrocession());
				}
				System.out.println("afficherDetailCompteCourant:CashBackAssuranceMsg[]:end");
			}
			System.out.println("afficherDetailCompteCourant:CompteBancaireMsg:begin");
			afficherCompteBancaire(detailCompteCourantMsg.getCompteBancaire());
			System.out.println("afficherDetailCompteCourant:CompteBancaireMsg:end");
			System.out.println("afficherDetailCompteCourant:end");		
		}
	}

	private void afficherCompteBancaire(CompteBancaireMsg compteBancaire) {
		System.out.println("afficherCompteBancaire:afficheCompte:"+ compteBancaire.getAfficheCompte());
		System.out.println("afficherCompteBancaire:blocageDecouvert:"+ compteBancaire.getBlocageDecouvert());
		System.out.println("afficherCompteBancaire:iban:"+ compteBancaire.getIban());
		System.out.println("afficherCompteBancaire:cleIban:"+ compteBancaire.getCleIban());
		System.out.println("afficherCompteBancaire:cleRib:"+ compteBancaire.getCleRib());
		System.out.println("afficherCompteBancaire:codAgence:"+ compteBancaire.getCodAgence());
		System.out.println("afficherCompteBancaire:codeIdentifiantBancaire:"+ compteBancaire.getCodeIdentifiantBancaire());
		System.out.println("afficherCompteBancaire:codePaysIban:"+ compteBancaire.getCodePaysIban());	
		System.out.println("afficherCompteBancaire:codFamille:"+ compteBancaire.getCodFamille());
		System.out.println("afficherCompteBancaire:codGuichet:"+ compteBancaire.getCodGuichet());
		System.out.println("afficherCompteBancaire:codPackage:"+ compteBancaire.getCodPackage());
		System.out.println("afficherCompteBancaire:codProduit:"+ compteBancaire.getCodProduit());
		System.out.println("afficherCompteBancaire:codSousProduit:"+ compteBancaire.getCodSousProduit());
		System.out.println("afficherCompteBancaire:compteIntitule:"+ compteBancaire.getCompteIntitule());
		System.out.println("afficherCompteBancaire:devise:"+ compteBancaire.getDevise());
		System.out.println("afficherCompteBancaire:dteCloture:"+ compteBancaire.getDteCloture());
		System.out.println("afficherCompteBancaire:dteCreation:"+ compteBancaire.getDteCreation());
		System.out.println("afficherCompteBancaire:dteDebutDecouvert:"+ compteBancaire.getDteDebutDecouvert());
		System.out.println("afficherCompteBancaire:dteFinDecouvert:"+ compteBancaire.getDteFinDecouvert());
		System.out.println("afficherCompteBancaire:estGestionSousMandat:"+ compteBancaire.getEstGestionSousMandat());
		System.out.println("afficherCompteBancaire:montantDecouvert:"+ compteBancaire.getMontantDecouvert());
		System.out.println("afficherCompteBancaire:numCompte:"+ compteBancaire.getNumCompte());
		System.out.println("afficherCompteBancaire:numContrat:"+ compteBancaire.getNumContrat());
		System.out.println("afficherCompteBancaire:role:"+ compteBancaire.getRole());
		System.out.println("afficherCompteBancaire:solde:"+ compteBancaire.getSolde());
		System.out.println("afficherCompteBancaire:codCanalMetierCreationOffre:"+ compteBancaire.getCodCanalMetierCreationOffre());
		System.out.println("afficherCompteBancaire:codTypeAppartenance:"+ compteBancaire.getCodTypeAppartenance());
	}
}
