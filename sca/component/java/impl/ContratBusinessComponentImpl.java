package sca.component.java.impl;

import static fr.axb.lib.exchange.retour.msg.CodeRetourFactory.RETOUR_CORRECT;
import static fr.axb.lib.exchange.retour.msg.CodeRetourFactory.RETOUR_INCIDENT_TECHNIQUE;
import static fr.axb.lib.exchange.retour.msg.CodeRetourFactory.getRetour;
import historiquemvt.msg.util.HistoMouvementUtil;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import listercontrat.msg.util.ContratUtil;
import msg.alerte.contrat.apirest.axb.adapt.med.CritereModifierContratAlerteAPIMsg;
import msg.alerte.contrat.apirest.axb.adapt.med.DetailContratAlerteAPIMsg;
import msg.detailcredit.contrat.apirest.axb.adapt.med.LireDetailCreditMsg;
import msg.epargne.contrat.axb.adapt.med.DetailAbonnementEpargneMsgSP;
import msg.epargne.contrat.axb.adapt.med.DetailValeursAbonnementPossibleMsgSP;
import msg.epargne.contrat.axb.adapt.med.ModifierAbonnementEpargneMsgSP;
import msg.epargne.contrat.axb.adapt.med.ValeurPossibleMsgSP;
import msg.listercontrat.contrat.apirest.axb.adapt.med.CarteMsg;
import msg.listercontrat.contrat.apirest.axb.adapt.med.IContratMsg;
import msg.listercontrat.contrat.apirest.axb.adapt.med.PanoramaCompteMsg;

import com.ibm.wbiserver.mediation.jtow.SDOJavaObjectMediator;
import com.ibm.websphere.sca.ServiceManager;
import commonj.sdo.DataObject;

import contrat.utilis.AbonnementEpargneUtils;
import contrat.utilis.AlerteUtils;
import crm.contratservice.ws.client.AbonnementEpargneMsg;
import crm.contratservice.ws.client.AlerteParametreeMsg;
import crm.contratservice.ws.client.ContratServiceBusinessComponent;
import crm.contratservice.ws.client.ContratServiceBusinessComponentServiceLocator;
import crm.contratservice.ws.client.CritereContratAlerteMsg;
import crm.contratservice.ws.client.CritereModifierContratAlerteMsg;
import crm.contratservice.ws.client.DetailAbonnementEpargneMsg;
import crm.contratservice.ws.client.DetailContratAlerteMsg;
import crm.contratservice.ws.client.ModifierAbonnementEpargneMsg;
import crm.contratservice.ws.client.ValeurPossibleMsg;
import fr.axabanque.common.parametrage.bc.ejb.interfaces.ParametrageBusinessComponentEJB;
import fr.axabanque.crm.comptebancaire.api.msg.CritereMouvementPaginationMsg;
import fr.axabanque.crm.comptebancaire.bc.ejb.interfaces.CompteBancaireBusinessComponentEJB;
import fr.axabanque.framework.exceptions.BusinessException;
import fr.axabanque.framework.exceptions.InvalidPrmException;
import fr.axabanque.framework.exceptions.TechnicalException;
import fr.axabanque.med.axb.apirest.common.AbstractComponentImpl;
import fr.axabanque.med.axb.apirest.common.excepions.HeaderSoapException;
import fr.axabanque.med.axb.apirest.common.excepions.InputParamException;
import fr.axabanque.med.axb.apirest.common.soap.parser.HeaderSoap;
import fr.axabanque.med.axb.apirest.common.validator.ParameterValidator;
import fr.axabanque.med.axb.apirest.common.validator.checks.IdRestControl;
import fr.axabanque.pilotage.axb.apirest.commun.api.constants.CodeTypeRole;
import fr.axabanque.pilotage.axb.apirest.commun.api.contrat.msg.CritereInfoMsg;
import fr.axabanque.pilotage.axb.apirest.commun.api.contrat.msg.CritereListerActionContratMsg;
import fr.axabanque.pilotage.axb.apirest.commun.api.contrat.msg.ListeCritereListerActionContratMsg;
import fr.axabanque.pilotage.axb.apirest.commun.ejb.interfaces.ListerActionEJB;
import fr.axabanque.pilotage.axb.apirest.contrat.api.msg.CritereDetailCreditMsg;
import fr.axabanque.pilotage.axb.apirest.contrat.ejb.interfaces.ContratEJB;
import fr.axabanque.pilotage.axb.webclient.liredetailcontratbancaire.ejb.interfaces.LireDetailContratBancaireEJB;
import fr.axb.lib.apirest.ConverterFactory;
import fr.axb.lib.apirest.IdInfo;
import fr.axb.lib.apirest.IdRest;
import fr.axb.lib.apirest.msg.IdContrat;
import fr.axb.lib.apirest.msg.IdValeur;
import fr.axb.lib.exchange.action.msg.ActionMsg;
import fr.axb.lib.exchange.action.msg.ListeActionsMsg;
import fr.axb.lib.exchange.exception.RestBusinessException;
import fr.axb.lib.exchange.mapper.factory.MapperFactory;
import fr.axb.lib.exchange.retour.msg.CodMsg;
import fr.axb.lib.exchange.retour.msg.CodeRetourFactory;
import fr.axb.lib.exchange.retour.msg.RetourMsg;

/**
 * Contrat Business Component
 * 
 * @author naje99k
 * 
 */
public class ContratBusinessComponentImpl extends AbstractComponentImpl {
	private static final Logger log = LoggerFactory.getLogger(ContratBusinessComponentImpl.class);
	// Needed for web service call
	private static final String CONFIGURATION_FILE = "adaptedServices.properties";

	private static final String CONTRAT_SERVICE_URL_KEY = "contratServiceProxyURL";

	/**
	 * EJB Compte Bancaire
	 */
	private CompteBancaireBusinessComponentEJB compteBancaireBusinessComponent;
	/**
	 * EJB Lister Contrat
	 */
	private ContratEJB contratBusinessComponent;
	/**
	 * EJB Lister Action Contrat
	 */
	private ListerActionEJB listerActionBusinessComponent;
	/**
	 * EJB Parametrage
	 */
	private ParametrageBusinessComponentEJB parametrageBusinessComponent;

	/**
	 * Web service handle to SM contrat service
	 * <p>
	 * ok static attribute initialize at runtime to let other services up if
	 * init fails.
	 */
	private static ContratServiceBusinessComponent service;

	/**
	 * Default constructor.
	 * 
	 * @throws HeaderSoapException
	 * @throws IOException
	 */
	public ContratBusinessComponentImpl() throws HeaderSoapException, IOException {
		compteBancaireBusinessComponent = locateService_CompteBancaireBusinessComponentPartner();
		contratBusinessComponent = locateService_ContratBusinessComponentPartner();
		listerActionBusinessComponent = locateService_ListerActionBusinessComponentPartner();
		parametrageBusinessComponent = locateService_ParametrageBusinessComponentEJBPartner();
	}

	/**
	 * Get web service handle to SM contrat service (java 6+)
	 * <p>
	 * Not used into constructor to let other services up if this one failed.
	 * 
	 * @throws IOException
	 *             if unable to read configuration property file where web
	 *             service url key is into.
	 * @throws ServiceException
	 *             on web service client handle creation error
	 */
	private synchronized void initWs() throws IOException, ServiceException {
		if (service != null) {
			return;
		}
		Properties properties = new Properties();
		// Configuration file is into an external jar, so I use getClassLoader()
		properties.load(getClass().getClassLoader().getResourceAsStream(CONFIGURATION_FILE));
		URL url = new URL(properties.getProperty(CONTRAT_SERVICE_URL_KEY));
		// URL url= new
		// URL("http://localhost:15277/crm.contratservice/ContratServiceBusinessComponentEjbService");
//		URL url= new URL("http://dvabwas01:15277/crm.contratservice/ContratServiceBusinessComponentEjbService");
		log.info("url call SM: " + url.toString());
		service = new ContratServiceBusinessComponentServiceLocator().getContratServiceBusinessComponentPort(url);

	}

	/**
	 * This method is used to locate the service for the reference named
	 * "CompteBancaireBusinessComponentPartner". This will return an instance of
	 * {@link CompteBancaireBusinessComponentEJB}. If you would like to use this
	 * service asynchronously then you will need to cast the result to
	 * {@link CompteBancaireBusinessComponentEJBAsync}.
	 * 
	 * @generated (com.ibm.wbit.java)
	 * 
	 * @return CompteBancaireBusinessComponentEJB
	 */
	public CompteBancaireBusinessComponentEJB locateService_CompteBancaireBusinessComponentPartner() {
		return (CompteBancaireBusinessComponentEJB) ServiceManager.INSTANCE.locateService("CompteBancaireBusinessComponentPartner");
	}

	/**
	 * This method is used to locate the service for the reference named
	 * "ListerActionBusinessComponentPartner". This will return an instance of
	 * {@link ListerActionEJB}. If you would like to use this service
	 * asynchronously then you will need to cast the result to
	 * {@link ListerActionEJBAsync}.
	 * 
	 * @generated (com.ibm.wbit.java)
	 * 
	 * @return ListerActionEJB
	 */
	public ListerActionEJB locateService_ListerActionBusinessComponentPartner() {
		return (ListerActionEJB) ServiceManager.INSTANCE.locateService("ListerActionBusinessComponentPartner");
	}

	/**
	 * This method is used to locate the service for the reference named
	 * "ContratBusinessComponentPartner". This will return an instance of
	 * {@link ContratEJB}. If you would like to use this service asynchronously
	 * then you will need to cast the result to {@link ContratEJBAsync}.
	 * 
	 * @generated (com.ibm.wbit.java)
	 * 
	 * @return ContratEJB
	 */
	public ContratEJB locateService_ContratBusinessComponentPartner() {
		return (ContratEJB) ServiceManager.INSTANCE.locateService("ContratBusinessComponentPartner");
	}

	/**
	 * This method is used to locate the service for the reference named
	 * "ParametrageBusinessComponentEJBPartner". This will return an instance of
	 * {@link ParametrageBusinessComponentEJB}. If you would like to use this
	 * service asynchronously then you will need to cast the result to
	 * {@link ParametrageBusinessComponentEJBAsync}.
	 * 
	 * @generated (com.ibm.wbit.java)
	 * 
	 * @return ParametrageBusinessComponentEJB
	 */
	public ParametrageBusinessComponentEJB locateService_ParametrageBusinessComponentEJBPartner() {
		return (ParametrageBusinessComponentEJB) ServiceManager.INSTANCE.locateService("ParametrageBusinessComponentEJBPartner");
	}

	/**
	 * This method is used to locate the service for the reference named
	 * "LireDetailContratBancaireEJBPartner". This will return an instance of
	 * {@link LireDetailContratBancaireEJB}. If you would like to use this
	 * service asynchronously then you will need to cast the result to
	 * {@link LireDetailContratBancaireEJBAsync}.
	 * 
	 * @generated (com.ibm.wbit.java)
	 * 
	 * @return LireDetailContratBancaireEJB
	 */
	public LireDetailContratBancaireEJB locateService_LireDetailContratBancaireEJBPartner() {
		return (LireDetailContratBancaireEJB) ServiceManager.INSTANCE.locateService("LireDetailContratBancaireEJBPartner");
	}

	/**
	 * Return a reference to the component service instance for this
	 * implementation class. This method should be used when passing this
	 * service to a partner reference or if you want to invoke this component
	 * service asynchronously.
	 * 
	 * @generated (com.ibm.wbit.java)
	 */
	@SuppressWarnings("unused")
	private Object getMyService() {
		return (Object) ServiceManager.INSTANCE.locateService("self");
	}

	/**
	 * Method generated to support implemention of operation
	 * "listerHistoriqueMouvementPagination" defined for WSDL port type named
	 * "IListerHistoriqueMouvementImport".
	 * 
	 * The presence of commonj.sdo.DataObject as the return type and/or as a
	 * parameter type conveys that its a complex type. Please refer to the WSDL
	 * Definition for more information on the type of input, output and
	 * fault(s).
	 */
	public DataObject listerHistoriqueMouvementPagination(DataObject critereMouvementPaginationMsg) {
		msg.listerhistmouvement.contrat.apirest.axb.adapt.med.CritereMouvementPaginationMsg cMvtMsgSP = null;
		CritereMouvementPaginationMsg cMvtPagMsgSM = null;
		fr.axabanque.crm.comptebancaire.api.msg.ListeMouvementMsg listMvtMsgSM = null;
		DataObject msgRetourSP = null;
		IdRest idRest = null;
		RetourMsg retour = RETOUR_CORRECT;
		try {
			// Header parameters
			validerHeaderInputParameters();

			// Get Rest Id
			idRest = constructIdRest(critereMouvementPaginationMsg.getString("idContrat"));

			// BEGIN CHECK INPUT PARAMETERS
			IdRestControl idContratCheck = new IdRestControl(IdInfo.CTR, critereMouvementPaginationMsg.getString("idContrat"));
			ParameterValidator.inputParamControl(ParameterValidator.MSTR_HDR_ERR_120).addControl(idContratCheck).doCheck();

			// transformation du message DataObject en bean java de la mediation
			log.debug("***** transformation du message DataObject en bean java de la mediation *****");
			if (critereMouvementPaginationMsg != null) {
				cMvtMsgSP = (msg.listerhistmouvement.contrat.apirest.axb.adapt.med.CritereMouvementPaginationMsg) SDOJavaObjectMediator.data2Java(
						critereMouvementPaginationMsg, "msg.listerhistmouvement.contrat.apirest.axb.adapt.med.CritereMouvementPaginationMsg");
			}
			// adaptation du bean d'entree de la mediation en message d'entr�e
			// pour le SM
			log.debug("***** adaptation du bean d'entree de la mediation en message d'entree pour le SM *****");
			if (cMvtMsgSP != null) {
				cMvtPagMsgSM = HistoMouvementUtil.adaptMsgJavaSPToMsgSM(cMvtMsgSP, idRest);
				listMvtMsgSM = compteBancaireBusinessComponent.listerHistoriqueMouvementPagination(cMvtPagMsgSM);
			}
		} catch (InputParamException inputParamException) {
			// TODO : Revoir le retour du SP pour inclure le retourMsg dans la
			// r�ponse finale
			retour = inputParamException.getRetour();
			inputParamException.printStackTrace();
		} catch (BusinessException e) {
			log.error("****** ContratBusinessComponentImpl.listerHistoriqueMouvementPagination ******** ", e);
			retour = CodeRetourFactory.RETOUR_INCIDENT_METIER;
		} catch (RemoteException e) {
			log.error("****** ContratBusinessComponentImpl.listerHistoriqueMouvementPagination ******** ", e);
			retour = CodeRetourFactory.RETOUR_INCIDENT_DISTANT;
		} catch (TechnicalException e) {
			log.error("****** ContratBusinessComponentImpl.listerHistoriqueMouvementPagination ******** ", e);
			retour = CodeRetourFactory.RETOUR_INCIDENT_TECHNIQUE;
		} catch (HeaderSoapException e) {
			log.error("****** ContratBusinessComponentImpl.listerHistoriqueMouvementPagination ******** ", e);
			retour = e.getRetour();
		} catch (Exception e) {
			log.error("Erreur lors de l'appel des composants techniques au moments de l'appel de l'historique des mouvements:", e);
			retour = CodeRetourFactory.RETOUR_INCIDENT_TECHNIQUE;

		}
		// Converter
		msg.listerhistmouvement.contrat.apirest.axb.adapt.med.ListeMouvementMsg listHistMvtMsgSP = new msg.listerhistmouvement.contrat.apirest.axb.adapt.med.ListeMouvementMsg();
		if (listMvtMsgSM != null) {
			listHistMvtMsgSP = HistoMouvementUtil.adaptMsgJavaSMToMsgSP(listMvtMsgSM, ((IdContrat) idRest).getCodProduit(),
					parametrageBusinessComponent);
		}
		if (listHistMvtMsgSP == null) {
			listHistMvtMsgSP = new msg.listerhistmouvement.contrat.apirest.axb.adapt.med.ListeMouvementMsg();
		}
		listHistMvtMsgSP.setRetour(retour);
		msgRetourSP = SDOJavaObjectMediator.java2Data(listHistMvtMsgSP, new QName("http://med.adapt.axb.apirest.contrat.listerhistmouvement.msg",
				"ListeMouvementMsg"));
		return msgRetourSP;
	}

	/**
	 * Method generated to support implemention of operation
	 * "listerHistoriqueMouvementPagination" defined for WSDL port type named
	 * "IListerContratImport".
	 * 
	 * The presence of commonj.sdo.DataObject as the return type and/or as a
	 * parameter type conveys that its a complex type. Please refer to the WSDL
	 * Definition for more information on the type of input, output and
	 * fault(s).
	 * 
	 * @throws TechnicalException
	 * @throws BusinessException
	 * @throws RemoteException
	 */
	public DataObject listerContrat(DataObject critereListerContratMsg) throws RemoteException, BusinessException, TechnicalException {
		msg.listercontrat.contrat.apirest.axb.adapt.med.CritereListerContratMsg critereListerContraMsgSP = null;
		fr.axabanque.pilotage.axb.apirest.contrat.api.msg.CritereListerContratMsg cMvtPagMsgSM = null;
		fr.axabanque.pilotage.axb.apirest.contrat.api.msg.PanoramaCompteMsg panoramaCompteMsgSM = null;
		msg.listercontrat.contrat.apirest.axb.adapt.med.PanoramaCompteMsg panoramaMsgSP = new msg.listercontrat.contrat.apirest.axb.adapt.med.PanoramaCompteMsg();
		DataObject msgRetourSP = null;
		IdRest idRest = null;
		String idClient = critereListerContratMsg.getString("idClient");
		try {
			// Header parameters
			validerHeaderInputParameters();

			if (log.isDebugEnabled()) {
				log.debug("***** transformation du message DataObject en bean java de la mediation *****");
			}
			idRest = constructIdRest(idClient);
			// BEGIN CHECK INPUT PARAMETERS
			IdRestControl idClientCheck = new IdRestControl(IdInfo.CLI, idClient);
			ParameterValidator.inputParamControl(ParameterValidator.MSTR_HDR_ERR_120).addControl(idClientCheck).doCheck();

			HeaderSoap header = getHeaderSoap();
			// transformation du message DataObject en bean java de la mediation
			if (critereListerContratMsg != null)
				critereListerContraMsgSP = (msg.listercontrat.contrat.apirest.axb.adapt.med.CritereListerContratMsg) SDOJavaObjectMediator.data2Java(
						critereListerContratMsg, "msg.listercontrat.contrat.apirest.axb.adapt.med.CritereListerContratMsg");
			// adaptation du bean d'entree de la mediation en message d'entr�e
			// pour le SM
			if (log.isDebugEnabled()) {
				log.debug("***** adaptation du bean d'entree de la mediation en message d'entree pour le SM *****");
			}
			if (critereListerContraMsgSP != null) {
				cMvtPagMsgSM = ContratUtil.adaptMsgJavaSPToMsgSM(critereListerContraMsgSP, idRest, header);
				panoramaCompteMsgSM = contratBusinessComponent.listerContrat(cMvtPagMsgSM);
				if (panoramaCompteMsgSM != null) {
					panoramaMsgSP = ContratUtil.adaptMsgJavaSMToMsgSP(panoramaCompteMsgSM, parametrageBusinessComponent);
				}
				// Mise � jours de la liste actions
				setContratListActions(panoramaMsgSP);
			}
		} catch (InputParamException inputParamException) {
			log.error("Erreur technique lors de la r�cup�ration de la liste des contrats pour l'id client: " + idClient, inputParamException);
			panoramaMsgSP.setRetour(inputParamException.getRetour());
		} catch (BusinessException e) {
			log.error("Erreur fonctionnelle lors de la r�cup�ration de la liste des contrats pour l'id client: " + idClient, e);
			panoramaMsgSP.setRetour(getRetour(e.getClass().getName(), e.getErrorCodeESB()) != null ? getRetour(e.getClass().getName(), e
					.getErrorCodeESB()) : getRetour(BusinessException.class.getName(), null));
		} catch (RemoteException e) {
			log.error("Erreur lors de l'appel de l'ejb pour la r�cup�ration de la liste des contrats pour l'id client: " + idClient, e);
			panoramaMsgSP.setRetour(RETOUR_INCIDENT_TECHNIQUE);
		} catch (TechnicalException e) {
			log.error("Erreur technique lors de l'appel de la r�cup�ration de la liste des contrats pour l'id client:" + idClient, e);
			panoramaMsgSP.setRetour(getRetour(e.getClass().getName(), e.getErrorCodeESB()) != null ? getRetour(e.getClass().getName(), e
					.getErrorCodeESB()) : RETOUR_INCIDENT_TECHNIQUE);
		} catch (HeaderSoapException exception) {
			log.error("Erreur SOAP lors de la r�cup�ration de la liste des contrats pour l'id client: " + idClient, exception);
			panoramaMsgSP.setRetour(exception.getRetour());
		} catch (Exception e) {
			log.error(
					"Erreur lors de l'appel des composants techniques au moments de l'appel de la r�cup�ration de la liste des contrats pour l'id client:"
							+ idClient, e);
			panoramaMsgSP.setRetour(new RetourMsg(RetourMsg.CODE_RETOUR.KO.toString(), new CodMsg(500, e.getMessage())));
		}
		// If TOUT est OK
		if (panoramaMsgSP.getRetour() == null) {
			panoramaMsgSP.setRetour(new RetourMsg(RetourMsg.CODE_RETOUR.OK.toString()));
		}
		if (panoramaMsgSP != null) {
			QName qName = new QName("http://med.adapt.axb.apirest.contrat.listercontrat.msg", "PanoramaCompteMsg");
			msgRetourSP = SDOJavaObjectMediator.java2Data(panoramaMsgSP, qName);
		}
		return msgRetourSP;
	}

	/**
	 * Method generated to support implementation of operation
	 * "lireDetailContrat" defined for WSDL port type named "IContratImport".
	 * 
	 * The presence of commonj.sdo.DataObject as the return type and/or as a
	 * parameter type conveys that it is a complex type. Please refer to the
	 * WSDL Definition for more information on the type of input, output and
	 * fault(s).
	 */
	public DataObject lireDetailContrat(String idContrat) {
		RetourMsg retour = RETOUR_CORRECT;
		Integer CODE_RETOUR_SM_OK = new Integer(0);
		String document = null;
		LireDetailCreditMsg lireDetailCreditMsg = new LireDetailCreditMsg();
		try {
			// Header parameters
			validerHeaderInputParameters();

			log.info("ContratBusinessComponent:lireDetailContrat:step1");
			// BEGIN CHECK INPUT PARAMETERS
			IdRestControl idContratCheck = new IdRestControl(IdInfo.CTR, idContrat);
			log.info("ContratBusinessComponent:lireDetailContrat:step2");
			ParameterValidator.inputParamControl(ParameterValidator.MSTR_HDR_ERR_120).addControl(idContratCheck).doCheck();
			log.info("ContratBusinessComponent:lireDetailContrat:step3");
			IdRest idRestContrat = constructIdRest(idContrat);
			log.info("ContratBusinessComponent:lireDetailContrat:step4");
			CritereDetailCreditMsg critereDetailCreditMsg = ContratUtil.parseToCritereDetailCreditMsgMsgSO(idRestContrat);
			log.info("ContratBusinessComponent:lireDetailContrat:step5");
			fr.axabanque.pilotage.axb.apirest.contrat.api.msg.LireDetailCreditMsg lireDetailCreditMsgSO = contratBusinessComponent
					.lireDetailCredit(critereDetailCreditMsg);
			log.info("ContratBusinessComponent:lireDetailCreditMsgSO:" + lireDetailCreditMsgSO);
			lireDetailCreditMsg = ContratUtil.parseToSP(lireDetailCreditMsgSO, idRestContrat, converter, parametrageBusinessComponent);
			lireDetailCreditMsg.setIdContrat(idContrat);
			log.info("ContratBusinessComponent:lireDetailContrat:step6");
		} catch (RemoteException e) {
			log.error("Erreur technique lors de la r�cup�ration des details du contrat: " + idContrat, e);
			retour = CodeRetourFactory.RETOUR_INCIDENT_DISTANT;
		} catch (InputParamException e) {
			log.error("Erreur technique lors de la r�cup�ration des details du contrat: " + idContrat, e);
			retour = e.getRetour();
		} catch (BusinessException e) {
			log.error("Erreur fonctionnelle lors de la r�cup�ration des details du contrat: " + idContrat, e);
			retour = CodeRetourFactory.RETOUR_INCIDENT_METIER;
		} catch (TechnicalException e) {
			log.error("Erreur technique lors de la r�cup�ration des details du contrat: " + idContrat, e);
			retour = new RetourMsg(RetourMsg.CODE_RETOUR.KO.toString(), new CodMsg(500, e.getMessage()));
			// lireDetailCreditMsg.setRetour(new
			// RetourMsg(RetourMsg.CODE_RETOUR.KO.toString(), new CodMsg(500,
			// e.getMessage())));
		} catch (HeaderSoapException e) {
			log.error("Erreur SOAP lors de la r�cup�ration des details du contrat: " + idContrat, e);
			retour = e.getRetour();
		} catch (Exception e) {
			log.error("Erreur technique lors de la r�cup�ration des details du contrat: " + idContrat, e);
			retour = new RetourMsg(RetourMsg.CODE_RETOUR.KO.toString(), new CodMsg(500, e.getMessage()));
			// lireDetailCreditMsg.setRetour(new
			// RetourMsg(RetourMsg.CODE_RETOUR.KO.toString(), new CodMsg(500,
			// e.getMessage())));
		}
		lireDetailCreditMsg.setRetour(retour);
		return SDOJavaObjectMediator.java2Data(lireDetailCreditMsg, new QName("http://med.adapt.axb.apirest.document.detailcredit.msg",
				LireDetailCreditMsg.class.getName()));
	}

	/**
	 * Method generated to support implementation of operation
	 * "lireDetailContratBancaire" defined for WSDL port type named
	 * "IContratImport".
	 * 
	 * The presence of commonj.sdo.DataObject as the return type and/or as a
	 * parameter type conveys that it is a complex type. Please refer to the
	 * WSDL Definition for more information on the type of input, output and
	 * fault(s).
	 */
	public DataObject lireDetailContratBancaire(DataObject lireDetailContratBancaire) {
		return new LireDetailContratBancaireImpl(this).lireDetailContratBancaire(lireDetailContratBancaire);
	}

	/**
	 * Method generated to support implementation of operation
	 * "lireDetailAbonnementEpargne" defined for WSDL port type named
	 * "IContratImport".
	 * 
	 * The presence of commonj.sdo.DataObject as the return type and/or as a
	 * parameter type conveys that it is a complex type. Please refer to the
	 * WSDL Definition for more information on the type of input, output and
	 * fault(s).
	 */
	public DataObject lireDetailAbonnementEpargne(String idContrat) {

		DataObject result = null;
		DetailAbonnementEpargneMsgSP detailSp = null;

		RetourMsg retour = CodeRetourFactory.RETOUR_CORRECT;

		log.debug("***** Start the mediation lireDetailAbonnementEpargne **********");

		try {
			log.info("contrat:lireDetailAbonnementEpargne:debut initWS");
			initWs();
			log.info("contrat:lireDetailAbonnementEpargne:fin initWS");
		} catch (IOException e) {
			log.error("lireDetailAbonnementEpargne(): failed to init web service handle: {}", e.getMessage());

			detailSp = new DetailAbonnementEpargneMsgSP();
			detailSp.setRetour(CodeRetourFactory.getRetour(TechnicalException.class.getName(), null));
			e.printStackTrace();
			return SDOJavaObjectMediator.java2Data(detailSp, null);
		} catch (ServiceException e) {
			log.error("lireDetailAbonnementEpargne(): failed to init web service handle: {}", e.getMessage());

			detailSp = new DetailAbonnementEpargneMsgSP();
			detailSp.setRetour(CodeRetourFactory.getRetour(TechnicalException.class.getName(), null));
			e.printStackTrace();
			return SDOJavaObjectMediator.java2Data(detailSp, null);
		}

		try {
			log.info("contrat:lireDetailAbonnementEpargne:begin idContrat");
			IdContrat idContratObj = (IdContrat) converter.convertIdToStruct(idContrat);
			log.info("contrat:lireDetailAbonnementEpargne:fin idContrat");
			AbonnementEpargneMsg msg = new AbonnementEpargneMsg();
			msg.setNumContrat(idContratObj.getNumContrat());
			msg.setCodProduit(idContratObj.getCodProduit() != null ? Integer.valueOf(idContratObj.getCodProduit()) : null);
			log.info("contrat:lireDetailAbonnementEpargne:numContrat:" + msg.getNumContrat());
			log.info("contrat:lireDetailAbonnementEpargne:codProduit:" + msg.getCodProduit());
			DetailAbonnementEpargneMsg detail = service.lireDetailAbonnementEpargne(msg);
			log.info("contrat:lireDetailAbonnementEpargne:fin appel lireDetailAbonnementEpargne:detail:" + detail);
			detailSp = AbonnementEpargneUtils.mapDetailAbonnementSmToSp(detail);
			log.info("contrat:lireDetailAbonnementEpargne:fin detailSP:" + detailSp);
		} catch (IllegalArgumentException e) {
			retour = CodeRetourFactory.RETOUR_INCIDENT_TECHNIQUE;
			log.error("modifierAbonnementEpargne(): cannot decrypt input [{}]: {}", idContrat, e.getMessage(), e);
		} catch (crm.contratservice.ws.client.BusinessException e) {
			retour = constructRetourMsgSm2Sp(e.getRetourMsg());
			log.error("modifierAbonnementEpargne(): webservice call RemoteException: {}", e.getMessage(), e);
		} catch (crm.contratservice.ws.client.TechnicalException e) {
			retour = CodeRetourFactory.RETOUR_INCIDENT_TECHNIQUE;
			log.error("modifierAbonnementEpargne(): webservice call TechnicalException: {}", e.getFaultString(), e);
		} catch (RemoteException e) {
			retour = CodeRetourFactory.RETOUR_INCIDENT_DISTANT;
			log.error(e.getMessage());
		} catch (Exception e) {
			retour = CodeRetourFactory.RETOUR_INCIDENT_TECHNIQUE;
			log.error("modifierAbonnementEpargne(): GeneralException : {}", e.getMessage(), e);
		} finally {
			if (detailSp == null)
				detailSp = new DetailAbonnementEpargneMsgSP();

			detailSp.setRetour(retour);
			result = com.ibm.wbiserver.mediation.jtow.SDOJavaObjectMediator.java2Data(detailSp, new javax.xml.namespace.QName(
					"http://med.adapt.axb.contrat.epargne.msg", "DetailAbonnementEpargneMsgSP"));
		}

		return result;
	}

	/**
	 * Method generated to support implementation of operation
	 * "modifierAbonnementEpargne" defined for WSDL port type named
	 * "IContratImport".
	 * 
	 * The presence of commonj.sdo.DataObject as the return type and/or as a
	 * parameter type conveys that it is a complex type. Please refer to the
	 * WSDL Definition for more information on the type of input, output and
	 * fault(s).
	 */
	public DataObject modifierAbonnementEpargne(DataObject abonnement) {

		log.debug("***** Start of the mediation modifierAbonnementEpargne() *****");
		ModifierAbonnementEpargneMsgSP input_sp = null;
		ModifierAbonnementEpargneMsg input_sm = null;
		DetailAbonnementEpargneMsgSP detailSp = null;
		DataObject result = null;

		try {
			initWs();
		} catch (IOException e) {
			log.error("modifierAbonnementEpargne(): failed to init web service handle: {}", e.getMessage(), e);
			detailSp = new DetailAbonnementEpargneMsgSP();
			detailSp.setRetour(CodeRetourFactory.getRetour(TechnicalException.class.getName(), null));
			return SDOJavaObjectMediator.java2Data(detailSp, null);
		} catch (ServiceException e) {
			log.error("modifierAbonnementEpargne(): failed to init web service handle: {}", e.getMessage(), e);
			detailSp = new DetailAbonnementEpargneMsgSP();
			detailSp.setRetour(CodeRetourFactory.getRetour(TechnicalException.class.getName(), null));
			return SDOJavaObjectMediator.java2Data(detailSp, null);
		}

		RetourMsg retour = CodeRetourFactory.RETOUR_CORRECT;
		try { // msg.epargne.contrat.axb.adapt.med.ModifierAbonnementEpargneMsg
			HeaderSoap header = getHeaderSoap();
			// transformation du message DataObject en bean java de la mediation
			if (abonnement != null) {
				input_sp = (ModifierAbonnementEpargneMsgSP) SDOJavaObjectMediator.data2Java(abonnement,
						"msg.epargne.contrat.axb.adapt.med.ModifierAbonnementEpargneMsgSP");
			}

			AbonnementEpargneUtils.validerInputModifierAbonnementEpargene(input_sp);
			log.debug("***** adaptation du bean d'entree de modifierAbonnementEpargne() en message d'entree pour le SM *****");
			input_sm = AbonnementEpargneUtils.mapModifierAbonnementParamSpToSm(input_sp);
			log.info("modifierAbonnementEpargne:fin input sm");
			/** Appel du web service */
			DetailAbonnementEpargneMsg detailSM = service.modifierAbonnementEpargne(input_sm);
			log.info("modifierAbonnementEpargne:fin appel sm");
			detailSp = AbonnementEpargneUtils.mapDetailAbonnementSmToSp(detailSM);
			log.info("modifierAbonnementEpargne:fin detail sp");
		} catch (IllegalArgumentException e) {
			retour = CodeRetourFactory.RETOUR_INCIDENT_TECHNIQUE;
			log.error("modifierAbonnementEpargne(): cannot decrypt input [{}]: {}", input_sp.getIdContrat(), e.getMessage(), e);
		} catch (RestBusinessException e) {
			retour = e.getRetourMsg();
			log.error("modifierAbonnementEpargne(): check inputs: {}", e.getMessage(), e);
			System.out.println("RestBusinessException"+e.getMessage());
		} catch (crm.contratservice.ws.client.BusinessException e) {
			retour = constructRetourMsgSm2Sp(e.getRetourMsg());
			log.error("modifierAbonnementEpargne(): webservice call RemoteException: {}", e.getMessage(), e);
		} catch (crm.contratservice.ws.client.TechnicalException e) {
			retour = CodeRetourFactory.RETOUR_INCIDENT_TECHNIQUE;
			log.error("modifierAbonnementEpargne(): webservice call TechnicalException: {}", e.getFaultString(), e);
		} catch (RemoteException e) {
			retour = CodeRetourFactory.RETOUR_INCIDENT_DISTANT;
			log.error(e.getMessage(), e);
		} catch (HeaderSoapException e) {
			retour = CodeRetourFactory.RETOUR_INCIDENT_TECHNIQUE;
			log.error("modifierAbonnementEpargne(): HeaderSoapException: {}", e.getMessage(), e);
		} catch (Exception e) {
			retour = CodeRetourFactory.RETOUR_INCIDENT_TECHNIQUE;
			log.error("modifierAbonnementEpargne(): GeneralException : {}", e.getMessage(), e);
		} finally {
			if (detailSp == null)
				detailSp = new DetailAbonnementEpargneMsgSP();

			detailSp.setRetour(retour);

			result = com.ibm.wbiserver.mediation.jtow.SDOJavaObjectMediator.java2Data(detailSp, new javax.xml.namespace.QName(
					"http://med.adapt.axb.contrat.epargne.msg", "DetailAbonnementEpargneMsgSP"));
		}

		return result;
	}

	/**
	 * Method generated to support implementation of operation
	 * "lireContratAlerte" defined for WSDL port type named "IContratImport".
	 * 
	 * The presence of commonj.sdo.DataObject as the return type and/or as a
	 * parameter type conveys that it is a complex type. Please refer to the
	 * WSDL Definition for more information on the type of input, output and
	 * fault(s).
	 */
	public DataObject lireContratAlerte(DataObject input) {
		final String cryptId = input.getString("idContrat");
		final String codTypeRole = input.getString("codTypeRole");
		List<CodMsg> detailErreurs = new ArrayList<CodMsg>();

		DetailContratAlerteAPIMsg output = new DetailContratAlerteAPIMsg();

		if ((cryptId == null) || (cryptId.length() == 0)) {
			log.error("lireContratAlerte(): missing mandatory input attributs: idContrat=[{}]", cryptId);
			detailErreurs.add(CodeRetourFactory.getRetour(CodeRetourFactory.ERREUR_CONTRAT_IDCONTRAT_OBLIGATOIRE_CLE).getCodErreur());
		}
		if ((codTypeRole == null) || (codTypeRole.length() == 0)) {
			detailErreurs.add(CodeRetourFactory.getRetour(CodeRetourFactory.ERREUR_CONTRAT_TYPE_ROLE_OBLIGATOIRE_CLE).getCodErreur());
		}

		if ((detailErreurs != null) && (detailErreurs.size() > 0)) {
			RetourMsg retour = new RetourMsg(RetourMsg.CODE_RETOUR.KO.toString(), CodeRetourFactory.getRetour(
					CodeRetourFactory.ERREUR_COMMUN_PARAMETRES_ENTREE_INVALIDES_CLE).getCodErreur(), detailErreurs.toArray(new CodMsg[detailErreurs
					.size()]));
			output.setRetour(retour);
			return SDOJavaObjectMediator.java2Data(output, null);
		}
		IdContrat idContrat = null;
		try {
			idContrat = (IdContrat) converter.convertIdToStruct(cryptId);
		} catch (IllegalArgumentException e) {
			log.error("lireContratAlerte(): cannot decrypt input [{}]: {}", cryptId, e.getMessage(), e);
			output.setRetour(CodeRetourFactory.getRetour(InvalidPrmException.class.getName(), null));
			return SDOJavaObjectMediator.java2Data(output, null);
		}

		try {
			initWs();
		} catch (IOException e) {
			log.error("lireContratAlerte(): failed to init web service handle: {}", e.getMessage(), e);
			output.setRetour(RETOUR_INCIDENT_TECHNIQUE);
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (ServiceException e) {
			log.error("lireContratAlerte(): failed to init web service handle: {}", e.getMessage(), e);
			output.setRetour(RETOUR_INCIDENT_TECHNIQUE);
			return SDOJavaObjectMediator.java2Data(output, null);
		}

		// Call web service.
		try {
			DetailContratAlerteMsg retourSM = service.lireContratAlerte(new CritereContratAlerteMsg(Integer.valueOf(idContrat.getCodProduit()),
					getIdUtilisateur().toString(), idContrat.getNumContrat()));

			// Transform to an SP response
			final AlerteUtils alerteUtils = new AlerteUtils(parametrageBusinessComponent, listerActionBusinessComponent);
			output = alerteUtils.convertToSP(retourSM);
			output.setIdContrat(cryptId);

			// Set actions
			output.setActions(alerteUtils.getActions(idContrat, codTypeRole).toArray(new ActionMsg[0]));

		} catch (crm.contratservice.ws.client.BusinessException e) {
			log.error("lireContratAlerte(): webservice call BusinessException: {}", e.getFaultString(), e);
			output = new DetailContratAlerteAPIMsg(RETOUR_INCIDENT_TECHNIQUE);
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (crm.contratservice.ws.client.TechnicalException e) {
			log.error("lireContratAlerte(): webservice call TechnicalException: {}", e.getFaultString(), e);
			output = new DetailContratAlerteAPIMsg(RETOUR_INCIDENT_TECHNIQUE);
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (RemoteException e) {
			log.error("lireContratAlerte(): webservice call RemoteException: {}", e.getMessage(), e);
			output = new DetailContratAlerteAPIMsg(CodeRetourFactory.RETOUR_INCIDENT_DISTANT);
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (IllegalAccessException e) {
			log.error("lireContratAlerte(): IllegalAccessException: {}", e.getMessage(), e);
			output = new DetailContratAlerteAPIMsg(RETOUR_INCIDENT_TECHNIQUE);
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (InvocationTargetException e) {
			log.error("lireContratAlerte(): InvocationTargetException: {}", e.getMessage(), e);
			output = new DetailContratAlerteAPIMsg(RETOUR_INCIDENT_TECHNIQUE);
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (NumberFormatException e) {
			log.error("lireContratAlerte(): NumberFormatException: {}", e.getMessage(), e);
			output = new DetailContratAlerteAPIMsg(RETOUR_INCIDENT_TECHNIQUE);
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (HeaderSoapException e) {
			log.error("lireContratAlerte(): HeaderSoapException: {}", e.getMessage(), e);
			output = new DetailContratAlerteAPIMsg(RETOUR_INCIDENT_TECHNIQUE);
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (InvalidPrmException e) {
			log.error("lireContratAlerte(): InvalidPrmException: {}", e.getMessage(), e);
			output = new DetailContratAlerteAPIMsg(CodeRetourFactory.getRetour(InvalidPrmException.class.getName(), null));
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (TechnicalException e) {
			log.error("lireContratAlerte(): TechnicalException: {}", e.getMessage(), e);
			output = new DetailContratAlerteAPIMsg(RETOUR_INCIDENT_TECHNIQUE);
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (RestBusinessException e) {
			log.error("lireContratAlerte(): RestBusinessException: {}", e.getRetourMsg().getCodErreur().getLibelle(), e);
			output = new DetailContratAlerteAPIMsg(e.getRetourMsg());
			return SDOJavaObjectMediator.java2Data(output, null);
		}
		output.setRetour(RETOUR_CORRECT);
		return SDOJavaObjectMediator.java2Data(output, null);
	}

	/**
	 * Method generated to support implementation of operation
	 * "modifierContratAlerte" defined for WSDL port type named
	 * "IContratImport".
	 * 
	 * The presence of commonj.sdo.DataObject as the return type and/or as a
	 * parameter type conveys that it is a complex type. Please refer to the
	 * WSDL Definition for more information on the type of input, output and
	 * fault(s).
	 */
	public DataObject modifierContratAlerte(DataObject input) {

		final CritereModifierContratAlerteAPIMsg critereModifierContratAlerteAPIMsg = (CritereModifierContratAlerteAPIMsg) SDOJavaObjectMediator
				.data2Java(input, CritereModifierContratAlerteAPIMsg.class.getCanonicalName());
		log.info("modifierContratAlerte(): critereModifierContratAlerteAPIMsg={}", critereModifierContratAlerteAPIMsg);
		DetailContratAlerteAPIMsg output = new DetailContratAlerteAPIMsg();
		RetourMsg retour = validerParametresEntreeModifierContratAlerte(input);
		if (retour != null) {
			output.setRetour(retour);
			return SDOJavaObjectMediator.java2Data(output, null);
		}
		final String cryptId = critereModifierContratAlerteAPIMsg.getIdContrat();
		if (cryptId == null || "".equals(cryptId)) {
			log.error("modifierContratAlerte(): missing mandatory input attribute idContrat");

			output.setRetour(CodeRetourFactory.getRetour(InvalidPrmException.class.getName(), null));
			return SDOJavaObjectMediator.java2Data(output, null);
		}
		IdContrat idContrat = null;
		try {
			idContrat = (IdContrat) ConverterFactory.getInstance().convertIdToStruct(cryptId);
		} catch (IllegalArgumentException e) {
			log.error("modifierContratAlerte(): cannot decrypt input [{}]: {}", cryptId, e.getMessage());

			output.setRetour(CodeRetourFactory.getRetour(InvalidPrmException.class.getName(), null));
			e.printStackTrace();
			return SDOJavaObjectMediator.java2Data(output, null);
		}

		try {
			initWs();
		} catch (IOException e) {
			log.error("modifierContratAlerte(): failed to init web service handle: {}", e.getMessage(), e);
			output.setRetour(CodeRetourFactory.getRetour(TechnicalException.class.getName(), null));
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (ServiceException e) {
			log.error("modifierContratAlerte(): failed to init web service handle: {}", e.getMessage(), e);
			output.setRetour(CodeRetourFactory.getRetour(TechnicalException.class.getName(), null));
			return SDOJavaObjectMediator.java2Data(output, null);
		}

		// Call web service.
		try {
			final AlerteUtils alerteUtils = new AlerteUtils(parametrageBusinessComponent, listerActionBusinessComponent);
			final Integer nombreMaxAlertes = critereModifierContratAlerteAPIMsg.getNombreMaxAlertes() == null ? null : Integer
					.valueOf(critereModifierContratAlerteAPIMsg.getNombreMaxAlertes());

			DetailContratAlerteMsg retourSM = service.modifierContratAlerte(new CritereModifierContratAlerteMsg(Integer.valueOf(idContrat
					.getCodProduit()), getIdUtilisateur().toString(), idContrat.getNumContrat(), alerteUtils.convertToSM(
					Arrays.asList(critereModifierContratAlerteAPIMsg.getAlertes())).toArray(new AlerteParametreeMsg[0]), nombreMaxAlertes));

			// Transform to an SP response
			output = alerteUtils.convertToSP(retourSM);
			output.setIdContrat(cryptId);
		} catch (crm.contratservice.ws.client.BusinessException e) {
			log.error("modifierContratAlerte(): webservice call BusinessException: {}", e.getFaultString(), e);
			output.setRetour(CodeRetourFactory.getRetour(TechnicalException.class.getName(), null));
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (crm.contratservice.ws.client.TechnicalException e) {
			log.error("modifierContratAlerte(): webservice call TechnicalException: {}", e.getFaultString(), e);
			output.setRetour(CodeRetourFactory.getRetour(TechnicalException.class.getName(), null));
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (RemoteException e) {
			log.error("modifierContratAlerte(): webservice call RemoteException: {}", e.getMessage(), e);
			output.setRetour(CodeRetourFactory.getRetour(RemoteException.class.getName(), null));
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (IllegalAccessException e) {
			log.error("modifierContratAlerte(): IllegalAccessException: {}", e.getMessage(), e);
			output.setRetour(RETOUR_INCIDENT_TECHNIQUE);
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (InvocationTargetException e) {
			log.error("modifierContratAlerte(): InvocationTargetException: {}", e.getMessage(), e);
			output.setRetour(RETOUR_INCIDENT_TECHNIQUE);
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (NumberFormatException e) {
			log.error("modifierContratAlerte(): NumberFormatException: {}", e.getMessage(), e);
			output.setRetour(RETOUR_INCIDENT_TECHNIQUE);
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (HeaderSoapException e) {
			log.error("modifierContratAlerte(): HeaderSoapException: {}", e.getMessage(), e);
			output.setRetour(RETOUR_INCIDENT_TECHNIQUE);
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (InvalidPrmException e) {
			log.error("modifierContratAlerte(): InvalidPrmException: {}", e.getMessage(), e);
			output.setRetour(CodeRetourFactory.getRetour(InvalidPrmException.class.getName(), null));
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (IllegalArgumentException e) {
			log.error("modifierContratAlerte(): IllegalArgumentException: {}", e.getMessage(), e);
			output.setRetour(CodeRetourFactory.getRetour(InvalidPrmException.class.getName(), null));
			return SDOJavaObjectMediator.java2Data(output, null);
		}
		output.setRetour(RETOUR_CORRECT);
		return SDOJavaObjectMediator.java2Data(output, null);
	}

	private RetourMsg validerParametresEntreeModifierContratAlerte(DataObject critereModifierContratAlerte) {
		RetourMsg retour = null;
		List<CodMsg> detailErreurs = new ArrayList<CodMsg>();
		Map<String, CodMsg> detailsErreurMap = new HashMap<String, CodMsg>();
		if (critereModifierContratAlerte != null) {
			String idContrat = critereModifierContratAlerte.getString("idContrat");
			if ((idContrat == null) || (idContrat.length() == 0)) {
				detailsErreurMap.put("IDCONTRAT", CodeRetourFactory.getRetour(CodeRetourFactory.ERREUR_CONTRAT_IDCONTRAT_OBLIGATOIRE_CLE)
						.getCodErreur());
			}
			List<DataObject> alertes = critereModifierContratAlerte.getList("alertes");
			if ((alertes != null) && (alertes.size() > 0)) {
				for (DataObject alerte : alertes) {
					if ((alerte.getDataObject("code") == null) || (!alerte.getDataObject("code").isSet("code"))) {
						detailsErreurMap.put("CODEALERTE", CodeRetourFactory.getRetour(CodeRetourFactory.ERREUR_CONTRAT_CODE_ALERTE_OBLIGATOIRE_CLE)
								.getCodErreur());
					}
					if ((alerte.getDataObject("canal") == null) || (!alerte.getDataObject("canal").isSet("code"))) {
						detailsErreurMap.put("CANALALERTE", CodeRetourFactory
								.getRetour(CodeRetourFactory.ERREUR_CONTRAT_CANAL_ALERTE_OBLIGATOIRE_CLE).getCodErreur());
					}
					if ((alerte.getDataObject("support") == null) || (!alerte.getDataObject("support").isSet("code"))) {
						detailsErreurMap.put("SUPPORTALERTE", CodeRetourFactory.getRetour(
								CodeRetourFactory.ERREUR_CONTRAT_SUPPORT_ALERTE_OBLIGATOIRE_CLE).getCodErreur());
					}
					if ((alerte.getString("idContratSupport") == null) || (alerte.getString("idContratSupport").length() == 0)) {
						detailsErreurMap.put("IDCONTRATSUPPORT", CodeRetourFactory.getRetour(
								CodeRetourFactory.ERREUR_CONTRAT_IDCONTRATSUPPORT_ALERTE_OBLIGATOIRE_CLE).getCodErreur());
					}
					if (!alerte.isSet("seuil")) {
						detailsErreurMap.put("SEUIL", CodeRetourFactory.getRetour(CodeRetourFactory.ERREUR_CONTRAT_SEUIL_ALERTE_OBLIGATOIRE_CLE)
								.getCodErreur());
					}
					if (!alerte.isSet("id")) {
						detailsErreurMap.put("ID", CodeRetourFactory.getRetour(CodeRetourFactory.ERREUR_CONTRAT_ID_ALERTE_OBLIGATOIRE_CLE)
								.getCodErreur());
					}
				}
			}
		}
		if ((detailsErreurMap != null) && (!detailsErreurMap.isEmpty())) {
			detailErreurs = new ArrayList<CodMsg>(detailsErreurMap.values());
			retour = new RetourMsg(RetourMsg.CODE_RETOUR.KO.toString(), CodeRetourFactory.getRetour(
					CodeRetourFactory.ERREUR_COMMUN_PARAMETRES_ENTREE_INVALIDES_CLE).getCodErreur(), detailErreurs.toArray(new CodMsg[detailErreurs
					.size()]));
		}
		return retour;
	}

	/**
	 * Method generated to support implementation of operation
	 * "listerValeursAbonnementPossible" defined for WSDL port type named
	 * "IContratImport".
	 * 
	 * The presence of commonj.sdo.DataObject as the return type and/or as a
	 * parameter type conveys that it is a complex type. Please refer to the
	 * WSDL Definition for more information on the type of input, output and
	 * fault(s).
	 */
	public DataObject listerValeursAbonnementPossible(String input) {
		try {
			initWs();
		} catch (IOException e) {
			log.error("listerValeursAbonnementPossible(): failed to init web service handle: {}", e.getMessage(), e);
			DetailValeursAbonnementPossibleMsgSP output = new DetailValeursAbonnementPossibleMsgSP();
			output.setRetour(new MapperFactory().buildDefaultMapper().mapError());
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (ServiceException e) {
			log.error("listerValeursAbonnementPossible(): failed to init web service handle: {}", e.getMessage(), e);
			DetailValeursAbonnementPossibleMsgSP output = new DetailValeursAbonnementPossibleMsgSP();
			output.setRetour(new MapperFactory().buildDefaultMapper().mapError());
			return SDOJavaObjectMediator.java2Data(output, null);
		}

		try {
			DetailValeursAbonnementPossibleMsgSP output = new DetailValeursAbonnementPossibleMsgSP();
			output.setRetour(CodeRetourFactory.RETOUR_CORRECT);

			List<ValeurPossibleMsgSP> lstVal = new ArrayList<ValeurPossibleMsgSP>();
			// Ok, service returns no null list
			for (ValeurPossibleMsg item : service.listerValeursAbonnementPossible()) {
				ValeurPossibleMsgSP val = new ValeurPossibleMsgSP();

				val.setIdValeur(converter.convertStructToId(new IdValeur(item.getCodIsin(), item.getPlaceCotation())));
				val.setCodIsin(item.getCodIsin());
				val.setDernierCours(item.getDernierCours());
				val.setNom(item.getNom());
				val.setRestriction(item.getRestriction().toString());

				lstVal.add(val);
			}
			output.setValeursPossibles(lstVal.toArray(new ValeurPossibleMsgSP[0]));
			log.debug("listerValeursAbonnementPossible(): returns {}", output);
			return SDOJavaObjectMediator.java2Data(output, null);
		} catch (RemoteException e) {
			log.error("listerValeursAbonnementPossible(): RemoteException: {}", e.getMessage(), e);
			DetailValeursAbonnementPossibleMsgSP output = new DetailValeursAbonnementPossibleMsgSP();
			output.setRetour(new MapperFactory().buildDefaultMapper().mapError());
			return SDOJavaObjectMediator.java2Data(output, null);
		}
	}

	/**
	 * R�cup�re la liste des action pou tous les contrats
	 * 
	 * @param panoramaMsgSP
	 * @throws RemoteException
	 * @throws BusinessException
	 * @throws TechnicalException
	 * @throws RestBusinessException
	 */
	private void setContratListActions(final PanoramaCompteMsg panoramaMsgSP) throws RemoteException, BusinessException, TechnicalException,
			RestBusinessException {

		List<IContratMsg[]> lst = new ArrayList<IContratMsg[]>() {
			{
				add(panoramaMsgSP.getServices());
				add(panoramaMsgSP.getComptes());
				add(panoramaMsgSP.getCartes());
				add(panoramaMsgSP.getCredits());
				add(panoramaMsgSP.getAssurancesvie());
			}
		};

		for (IContratMsg[] tabContract : lst) {
			if (tabContract != null) {
				setActions(tabContract);
			}
		}
	}

	/**
	 * Met � jours la list des actions
	 * 
	 * @param contracts
	 * @throws RemoteException
	 * @throws TechnicalException
	 * @throws RestBusinessException
	 */
	private void setActions(final IContratMsg[] contracts) throws RemoteException, TechnicalException, RestBusinessException {

		ListeCritereListerActionContratMsg listeCritereListerActionContratMsg = new ListeCritereListerActionContratMsg();

		List<CritereListerActionContratMsg> lstCrit = new ArrayList<CritereListerActionContratMsg>();
		for (IContratMsg contract : contracts) {
			lstCrit.add(buildCritereListerActionContratMsg(contract));
		}

		listeCritereListerActionContratMsg.setCritereListerActionContratMsg(lstCrit.toArray(new CritereListerActionContratMsg[lstCrit.size()]));

		// Appel du service lister Action
		ListeActionsMsg listeActionsMsg = listerActionBusinessComponent.listerActionContrat(listeCritereListerActionContratMsg);

		for (IContratMsg contract : contracts) {
			contract.setActions(listeActionsMsg.getActionsForFunctionalKey(contract.getIdContrat()));
		}
	}

	private CritereListerActionContratMsg buildCritereListerActionContratMsg(final IContratMsg contract) {

		CritereListerActionContratMsg critereListerActionContratMsg = new CritereListerActionContratMsg();

		critereListerActionContratMsg.setCleFonctionnelle(contract.getIdContrat());

		CritereInfoMsg crit = new CritereInfoMsg();
		crit.setCodTypeRole(CodeTypeRole.LUI_MEME);
		crit.setCodProduit(contract.getProduit().getCodProduit().getCode());
		if (contract instanceof CarteMsg) {
			crit.setCodTypeDebit(((CarteMsg) contract).getCodTypeDebit().getCode());
		}

		critereListerActionContratMsg.setCritereInfoMsg(crit);

		return critereListerActionContratMsg;
	}

	/**
	 * Construit un retourMsg au format SP � partrir du retourMsg retourn�
	 * par le web service
	 * 
	 * @param retour_sm
	 * @return
	 */
	private RetourMsg constructRetourMsgSm2Sp(crm.contratservice.ws.client.RetourMsg retour_sm) {
		CodMsg code_sp = new CodMsg(retour_sm.getCodErreur().getCode(), retour_sm.getCodErreur().getLibelle());
		RetourMsg r = new RetourMsg(retour_sm.getCodRetour(), code_sp, copyCodeMsgSm2Sp(retour_sm.getDetailsErreur()));

		return r;
	}

	/**
	 * Convertit les codesMsg au fromat SM vers les code
	 * 
	 * @param codes_sm
	 * @return
	 */
	private CodMsg[] copyCodeMsgSm2Sp(crm.contratservice.ws.client.CodMsg[] codes_sm) {
		if (codes_sm == null || codes_sm.length < 1)
			return null;

		CodMsg[] codes_sp = new CodMsg[codes_sm.length];
		for (int i = 0; i < codes_sm.length; i++) {
			crm.contratservice.ws.client.CodMsg code_sm = codes_sm[i];
			CodMsg cod_sp = new CodMsg(code_sm.getCode(), code_sm.getLibelle());
			log.info("code/libelle:" + code_sm.getCode() + "/" + code_sm.getLibelle());
			codes_sp[i] = cod_sp;
		}

		return codes_sp;
	}
}
