package contrat.utilis;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import msg.epargne.contrat.axb.adapt.med.ContratMsgSP;
import msg.epargne.contrat.axb.adapt.med.DetailAbonnementEpargneMsgSP;
import msg.epargne.contrat.axb.adapt.med.ModifierAbonnementEpargneMsgSP;
import msg.epargne.contrat.axb.adapt.med.ValeurInvestieMsgSP;
import msg.epargne.contrat.axb.adapt.med.ValeurParametreeMsgSP;
import msg.epargne.contrat.axb.adapt.med.ValeurPossibleMsgSP;

import org.apache.commons.beanutils.BeanUtils;

import crm.contratservice.ws.client.ContratMsg;
import crm.contratservice.ws.client.DetailAbonnementEpargneMsg;
import crm.contratservice.ws.client.ModifierAbonnementEpargneMsg;
import crm.contratservice.ws.client.PeriodicitePrelevement;
import crm.contratservice.ws.client.ValeurInvestieMsg;
import crm.contratservice.ws.client.ValeurParametreeMsg;
import crm.contratservice.ws.client.ValeurPossibleMsg;
import fr.axabanque.framework.exceptions.TechnicalException;
import fr.axb.lib.apirest.ConverterFactory;
import fr.axb.lib.apirest.msg.IdContrat;
import fr.axb.lib.apirest.msg.IdValeur;
import fr.axb.lib.exchange.exception.RestBusinessException;
import fr.axb.lib.exchange.retour.msg.CodMsg;
import fr.axb.lib.exchange.retour.msg.CodeRetourFactory;
import fr.axb.lib.exchange.retour.msg.RetourMsg;

public class AbonnementEpargneUtils {
	
	private static final fr.axb.lib.apirest.Converter idConverter =  ConverterFactory.getInstance();
	/**
	 * transforme le d�tail abonnement provenant du SM ves le format du SP
	 * @param detail_sm
	 * @return
	 * @throws TechnicalException
	 */
	public static DetailAbonnementEpargneMsgSP mapDetailAbonnementSmToSp(DetailAbonnementEpargneMsg detail_sm ) throws TechnicalException {
		
		if(detail_sm == null)
			return null;
		DetailAbonnementEpargneMsgSP detail_sp = new DetailAbonnementEpargneMsgSP();
		
		detail_sp.setIdContrat(detail_sm.getNumContrat());
		System.out.println(">> mapDetailAbonnementSmToSp: step1");
		detail_sp.setCodProduit(detail_sm.getCodProduit());
		System.out.println(">> mapDetailAbonnementSmToSp: step2");
		detail_sp.setPeriodicite(getPeriodicite(detail_sm.getPeriodicite()));
		System.out.println(">> mapDetailAbonnementSmToSp: step3");
		detail_sp.setJourPrelevement(detail_sm.getJourPrelevement());
		System.out.println(">> mapDetailAbonnementSmToSp: step4");
		if (detail_sm.getDateCreation() != null){
			detail_sp.setDateCreation(detail_sm.getDateCreation().getTime());
			System.out.println(">> mapDetailAbonnementSmToSp: step4.1");
		}
		if (detail_sm.getDateDebutSuspension() != null){
			detail_sp.setDateDebutSuspension(detail_sm.getDateDebutSuspension().getTime());
			System.out.println(">> mapDetailAbonnementSmToSp: step4.2");
		}
		if (detail_sm.getDateFinSuspension() != null){
			detail_sp.setDateFinSuspension(detail_sm.getDateFinSuspension().getTime());
			System.out.println(">> mapDetailAbonnementSmToSp: step4.3");
		}
		detail_sp.setContratPortefeuille(mapContrat(detail_sm.getContratPortefeuille()));
		System.out.println(">> mapDetailAbonnementSmToSp: step5");
		detail_sp.setContratPrelevement(mapContrat(detail_sm.getContratPrelevement()));
		System.out.println(">> mapDetailAbonnementSmToSp: step6");
		detail_sp.setValeursPossibles(mapValeursPossible(detail_sm.getValeursPossibles()));
		System.out.println(">> mapDetailAbonnementSmToSp: step7");
		detail_sp.setValeursParametrees(mapValeursParametree(detail_sm.getValeursParametrees()));
		System.out.println(">> mapDetailAbonnementSmToSp: step8");
		return detail_sp;
	}
	
	/**
	 * 
	 * @param modifAbnEpargne_sp
	 * @return
	 * @throws TechnicalException
	 */
	public static ModifierAbonnementEpargneMsg  mapModifierAbonnementParamSpToSm(ModifierAbonnementEpargneMsgSP modifAbnEpargne_sp) throws TechnicalException {
		
		if (modifAbnEpargne_sp == null)
			return null;
		
		ModifierAbonnementEpargneMsg inputModifAbn_sm = new ModifierAbonnementEpargneMsg();
		IdContrat idContrat = (IdContrat)idConverter.convertIdToStruct(modifAbnEpargne_sp.getIdContrat());
		if(modifAbnEpargne_sp.getIdContratPrelevement() != null){
			IdContrat idContratPrelevement = (IdContrat)idConverter.convertIdToStruct(modifAbnEpargne_sp.getIdContratPrelevement());
			inputModifAbn_sm.setNumContratPrelevement(idContratPrelevement.getNumContrat());
		}
		inputModifAbn_sm.setNumContrat(idContrat.getNumContrat());
		
		inputModifAbn_sm.setCodProduit(idContrat.getCodProduit() != null ? Integer.valueOf(idContrat.getCodProduit()) : null);		
		inputModifAbn_sm.setPeriodicite(modifAbnEpargne_sp.getPeriodicite());
		inputModifAbn_sm.setJourPrelevement(modifAbnEpargne_sp.getJourPrelevement());
		inputModifAbn_sm.setValeursInvsties(mapValeursInvesties(modifAbnEpargne_sp.getValeursInvsties()));
		
		return inputModifAbn_sm;
	}
	/**
	 * 
	 * @param contrat_sm
	 * @return
	 * @throws TechnicalException
	 */
	private static ContratMsgSP mapContrat(ContratMsg contrat_sm) throws TechnicalException {

		if (contrat_sm == null)
			return null;
		
		ContratMsgSP contrat_sp = new ContratMsgSP();
		copyProperties(contrat_sp, contrat_sm);
		
		return contrat_sp;
		
	}
	
	/**
	 * 
	 * @param valeurs_sm
	 * @return
	 * @throws TechnicalException
	 */
	private static ValeurPossibleMsgSP [] mapValeursPossible(ValeurPossibleMsg[] valeurs_sm) throws TechnicalException {
		if(valeurs_sm == null || valeurs_sm.length < 1)
			return null;
		
		List<ValeurPossibleMsgSP> valeurPosibles_sp = new ArrayList<ValeurPossibleMsgSP>();
		for (ValeurPossibleMsg valeur_sm : valeurs_sm) {
			if (valeur_sm != null) {
				ValeurPossibleMsgSP valeur_sp = new ValeurPossibleMsgSP();
				copyProperties(valeur_sp, valeur_sm);
				valeur_sp.setIdValeur(idConverter.convertStructToId(new IdValeur(valeur_sm.getCodIsin(), valeur_sm.getPlaceCotation())));
				valeurPosibles_sp.add(valeur_sp);
			}
		}
		
		ValeurPossibleMsgSP [] contratArr = new ValeurPossibleMsgSP[valeurPosibles_sp.size()];  
		
		return valeurPosibles_sp.toArray(contratArr);
	}
	
	
	
	/**
	 * map les valeurs possibles
	 * @param valeurs_sm
	 * @return
	 * @throws TechnicalException
	 */
	private static ValeurParametreeMsgSP [] mapValeursParametree(ValeurParametreeMsg[] valeurs_sm) throws TechnicalException {
		if(valeurs_sm == null || valeurs_sm.length < 1)
			return null;
		
		List<ValeurParametreeMsgSP> valeursParam_sp = new ArrayList<ValeurParametreeMsgSP>();
		for (ValeurParametreeMsg valeur_sm : valeurs_sm) {
			if (valeur_sm != null) {
				ValeurParametreeMsgSP valeur_sp = new ValeurParametreeMsgSP();
				copyProperties(valeur_sp, valeur_sm);
				valeur_sp.setIdValeur(idConverter.convertStructToId(new IdValeur(valeur_sm.getCodIsin(), valeur_sm.getPlaceCotation())));
				valeursParam_sp.add(valeur_sp);
			}
		}
		
		ValeurParametreeMsgSP [] contratArr = new ValeurParametreeMsgSP[valeursParam_sp.size()];  
		
		return valeursParam_sp.toArray(contratArr);
	}
	
	private static Integer getPeriodicite(PeriodicitePrelevement perdiodicitePrelev) {
		Integer result = null;
		
		if (perdiodicitePrelev == null)
			return null;
		
		if (perdiodicitePrelev.getValue().equalsIgnoreCase("MENSUEL"))
			result = 1;			
		else if (perdiodicitePrelev.getValue().equalsIgnoreCase("TRIMESTRIEL")) 
			result = 2;

		return result;
	}
	
	/**
	 * map les valeurs possibles
	 * @param valeurs_sm
	 * @return
	 * @throws TechnicalException
	 */
	private static ValeurInvestieMsg [] mapValeursInvesties(ValeurInvestieMsgSP[] valeurs_sp) throws TechnicalException {
		if(valeurs_sp == null || valeurs_sp.length < 1)
			return null;
		
		List<ValeurInvestieMsg> valeurs_investies_sm = new ArrayList<ValeurInvestieMsg>();
		for (ValeurInvestieMsgSP valeur_sp : valeurs_sp) {
			if (valeur_sp != null) {
				ValeurInvestieMsg valeur_sm = new ValeurInvestieMsg();
				// 17/02/2016 QC 3324 Le service REST ModifierAbonnementEpargne re�oit un code erreur 900
				IdValeur idValeur= (IdValeur)idConverter.convertIdToStruct(valeur_sp.getIdValeur());
				copyProperties(valeur_sm, valeur_sp);
				valeur_sm.setCodIsin(idValeur.getCodIsin());
				valeur_sm.setPlaceCotation(idValeur.getPlaceCotation());
				valeurs_investies_sm.add(valeur_sm);
			}
		}
		
		ValeurInvestieMsg [] valeurArr = new ValeurInvestieMsg[valeurs_investies_sm.size()];  
		
		return valeurs_investies_sm.toArray(valeurArr);
	}
	/**
	 * 
	 * @param <E>
	 * @param <F>
	 * @param dest
	 * @param origine
	 * @throws TechnicalException
	 */
	private static <E, F> void copyProperties(E dest, F origine) throws TechnicalException {

		if (origine == null)
			return;
		
		try {
			BeanUtils.copyProperties(dest, origine);
		} catch (IllegalAccessException e) {
			throw new TechnicalException(
					"Erreur technique lors de la conversion des donn�es des valeurs du format SM vers le format SP ");
		} catch (InvocationTargetException e) {
			throw new TechnicalException(
					"Erreur technique lors de la conversion des donn�es des valeurs du format SM vers le format SP ");
		}
	}
	
	/**
	 * valide les entr�es du SP
//	 
	 * @throws RestBusinessException */
	public static void validerInputModifierAbonnementEpargene(ModifierAbonnementEpargneMsgSP abonnement) 
		throws RestBusinessException {
		
		RetourMsg retour;
		if (abonnement == null) {
			retour = CodeRetourFactory.getRetour(CodeRetourFactory.ERREUR_CONTRAT_PARAMETRES_ENTREE_INVALIDES_CLE);
			throw new RestBusinessException("Parametre d'entr�e invalide", retour);
		}
		
		List<CodMsg> codes = new ArrayList<CodMsg>();
		
		if (isNullOrEmpty(abonnement.getIdContrat())) {
			codes.add(CodeRetourFactory.getRetour(CodeRetourFactory.ERREUR_CONTRAT_IDCONTRAT_OBLIGATOIRE_CLE).getCodErreur());
		}
			
		/*if (abonnement.getIdContratPrelevement() == null || abonnement.getIdContratPrelevement().equals("")) {
			codes.add(CodeRetourFactory.getRetour(CodeRetourFactory.ERREUR_CONTRAT_IDCONTRATPREVLEMEENT_OBLIGATOIRE_CLE).getCodErreur());
		}*/
		
		if (abonnement.getValeursInvsties() != null && abonnement.getValeursInvsties().length > 0) {
			List<CodMsg> codeValeurs = validerInputValeurInveties(abonnement.getValeursInvsties());
			if (codeValeurs != null && codeValeurs.size() > 0) {
				for (CodMsg codMsg : codeValeurs) {
					codes.add(codMsg);
				}
			}
		}
		
		if (codes.size() < 1)
			return;
		
		CodMsg [] codesArr = new CodMsg[codes.size()];
		
		codesArr = codes.toArray(codesArr);
		retour = new RetourMsg(
				RetourMsg.CODE_RETOUR.KO.toString(),
				CodeRetourFactory.getRetour(CodeRetourFactory.ERREUR_DOCUMENT_PARAMETRES_ENTREE_INVALIDES_CLE).getCodErreur(),
				codesArr);
		throw new RestBusinessException("Parametre d'entr�e invalide", retour);
	}
	
	/**
	 * validation des valeures investies quand elles sont pr�sentes
	 * @param investies
	 */
	private static List<CodMsg> validerInputValeurInveties(ValeurInvestieMsgSP[] investies) {
		List<CodMsg> codes = new ArrayList<CodMsg>();
		for (ValeurInvestieMsgSP invest : investies) {
			/** si lidInvet/idValeur sont null, le montant et la suppression doivent l'�tre */
			if (isNullOrEmpty(invest.getIdInvestissement()) && isNullOrEmpty(invest.getIdValeur())){
				System.out.println("========================================= if (isNullOrEmpty(invest.getIdInvestissement()");
				if (!isNullOrEmpty(invest.getMontant()) || !isNullOrEmpty(invest.getSuppressionBln())) {
					codes.add(CodeRetourFactory.getRetour(CodeRetourFactory.ERREUR_CONTRAT_IDINVESTISSEMENT_OBLIGATOIRE_CLE).getCodErreur());
				}
			} else {
				System.out.println("========================================= else (isNullOrEmpty(invest.getIdInvestissement()");
				/** Si idInvest/idValeur pr�sent le montant est obligatoire*/
				if (isNullOrEmpty(invest.getMontant())) {
					
					codes.add(CodeRetourFactory.getRetour(CodeRetourFactory.ERREUR_CONTRAT_MONTANT_INVESTISSEMENT_OBLIGATOIRE_CLE).getCodErreur());
				}
			}
		}
		return codes;
	}
	
	private static boolean isNullOrEmpty(String s) {
		if (s == null || s.equals(""))
			return true;
		return false;
	}
}
