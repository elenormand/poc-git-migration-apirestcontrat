package contrat.utilis;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import msg.alerte.contrat.apirest.axb.adapt.med.AlerteParametreeAPIMsg;
import msg.alerte.contrat.apirest.axb.adapt.med.AlertePossibleAPIMsg;
import msg.alerte.contrat.apirest.axb.adapt.med.DetailContratAlerteAPIMsg;
import msg.alerte.contrat.apirest.axb.adapt.med.HistoriqueAlerteAPIMsg;

import org.apache.commons.beanutils.BeanUtils;

import crm.contratservice.ws.client.AlerteParametreeMsg;
import crm.contratservice.ws.client.AlertePossibleMsg;
import crm.contratservice.ws.client.DetailContratAlerteMsg;
import crm.contratservice.ws.client.HistoriqueAlerteMsg;
import crm.contratservice.ws.client.IdContratMsg;
import fr.axabanque.common.parametrage.api.msg.CritereDictionaryMsg;
import fr.axabanque.common.parametrage.api.msg.DictionaryRefValueMsg;
import fr.axabanque.common.parametrage.bc.ejb.interfaces.ParametrageBusinessComponentEJB;
import fr.axabanque.framework.exceptions.InvalidPrmException;
import fr.axabanque.framework.exceptions.TechnicalException;
import fr.axabanque.pilotage.axb.apirest.commun.api.constants.CodeTypeRole;
import fr.axabanque.pilotage.axb.apirest.commun.api.contrat.msg.CritereInfoMsg;
import fr.axabanque.pilotage.axb.apirest.commun.api.contrat.msg.CritereListerActionContratMsg;
import fr.axabanque.pilotage.axb.apirest.commun.api.contrat.msg.ListeCritereListerActionContratMsg;
import fr.axabanque.pilotage.axb.apirest.commun.ejb.interfaces.ListerActionEJB;
import fr.axb.lib.apirest.ConverterFactory;
import fr.axb.lib.apirest.msg.IdContrat;
import fr.axb.lib.exchange.action.msg.ActionMsg;
import fr.axb.lib.exchange.action.msg.ListeActionsMsg;
import fr.axb.lib.exchange.exception.RestBusinessException;
import fr.axb.lib.exchange.retour.msg.CodMsg;

public class AlerteUtils {

	private static final String COMMON_GROUP_ELT = "1";

	private static final List<String> lstTranscoAlerte = Collections.unmodifiableList(new ArrayList<String>() {{
		add("RefTypeAlerteCarteDesc");
		add("RefTypeAVPCarteDesc");
		add("RefTypeAlerteCompteDesc");
		add("RefTypeAVPCompteDesc");
	}} );

	private static final String POSSIBLE_CHANNELS = "RefCanalAlerteDesc";
	
	private static final List<String> lstTranscoProducts = Collections.unmodifiableList(new ArrayList<String>() {{
			add("RefTypeProduitAlerteCarteDesc");
			add("RefTypeProduitAlerteCompteDesc");
		}} );

	private final ParametrageBusinessComponentEJB paramBC;
	private final ListerActionEJB listerActionBC;

	private final Map<Integer, CodMsg> mapTranscoAlerte;
	private final Map<Integer, CodMsg> mapTranscoChannels;
	private final Map<Integer, CodMsg> mapTranscoProducts;


	public AlerteUtils(final ParametrageBusinessComponentEJB parametrageBusinessComponent, final ListerActionEJB listerActionEJB) throws InvalidPrmException {
		this.paramBC        = parametrageBusinessComponent;
		this.listerActionBC = listerActionEJB;

		mapTranscoAlerte   = Collections.unmodifiableMap(getMapTransco(lstTranscoAlerte));
		mapTranscoChannels = Collections.unmodifiableMap(getMapTransco(Arrays.asList(new String[] {POSSIBLE_CHANNELS})));
		mapTranscoProducts = Collections.unmodifiableMap(getMapTransco(lstTranscoProducts));
	}

	public List<ActionMsg> getActions(final IdContrat idContrat, final String codTypeRole) throws RemoteException, TechnicalException, RestBusinessException {

		final List<ActionMsg> retour = new ArrayList<ActionMsg>();

		// Build criteria
		CritereListerActionContratMsg critereListerActionContratMsg = new CritereListerActionContratMsg();
		critereListerActionContratMsg.setCleFonctionnelle(idContrat.getNumContrat());

		CritereInfoMsg critereInfoMsg = new CritereInfoMsg();
		critereInfoMsg.setCodTypeRole(CodeTypeRole.valueOf(Integer.valueOf(codTypeRole)));
		critereInfoMsg.setCodProduit(Integer.valueOf((idContrat.getCodProduit())));

		critereListerActionContratMsg.setCritereInfoMsg(critereInfoMsg);

		ListeCritereListerActionContratMsg listeCritereListerActionContratMsg = new ListeCritereListerActionContratMsg();
		listeCritereListerActionContratMsg.setCritereListerActionContratMsg(new CritereListerActionContratMsg[] { critereListerActionContratMsg });

		// Call BC
		ListeActionsMsg listeActionsMsg = listerActionBC.listerActionContrat(listeCritereListerActionContratMsg);
		if (listeActionsMsg != null ) {
			retour.addAll(Arrays.asList(listeActionsMsg.getActionsForFunctionalKey(idContrat.getNumContrat())));			
		}
		return retour;
	}

	public List<AlerteParametreeMsg> convertToSM(final List<AlerteParametreeAPIMsg> lstAlertParamSP) {

		List<AlerteParametreeMsg> lstAlerteParamSM = new ArrayList<AlerteParametreeMsg>();
		for (AlerteParametreeAPIMsg alertParamSP: lstAlertParamSP) {

			String cryptIdSupport      = alertParamSP.getIdContratSupport();
			IdContrat idContratSupport = null;
			IdContratMsg contratSupport = null;
			if((cryptIdSupport != null) && (cryptIdSupport.length() > 0)){
				idContratSupport = (IdContrat)ConverterFactory.getInstance().convertIdToStruct(cryptIdSupport);
				contratSupport = new IdContratMsg(
						idContratSupport.getCodFamille(),
						idContratSupport.getCodProduit(),
						idContratSupport.getCodSousProduit(),
						idContratSupport.getNumCompte(),
						idContratSupport.getNumContrat(),
						idContratSupport.getNumReference()
						);
			}	
			Integer codeAlerte =  null;
			if(alertParamSP.getCode() != null){
				codeAlerte = alertParamSP.getCode().getCode();
			}
			Integer codeCanal =  null;
			if(alertParamSP.getCanal() != null){
				codeCanal = alertParamSP.getCanal().getCode();
			}
			Integer codeSupport = null;
			if(alertParamSP.getSupport() != null){
				codeSupport = alertParamSP.getSupport().getCode();
			}
			Integer id = null;
			if((alertParamSP.getId() != null) && (alertParamSP.getId().length() > 0)){
				id = Integer.valueOf(alertParamSP.getId()); 
			}
			AlerteParametreeMsg inputSM = new AlerteParametreeMsg(
						codeAlerte,
						codeCanal,
						null,
						codeSupport,
						id,
						contratSupport,
						null,
						alertParamSP.getSeuil()
						);
	
			lstAlerteParamSM.add(inputSM);
			
		}
		return lstAlerteParamSM;
	}

	public DetailContratAlerteAPIMsg convertToSP(final DetailContratAlerteMsg retourSM) throws IllegalAccessException, InvocationTargetException {

		DetailContratAlerteAPIMsg output = new DetailContratAlerteAPIMsg();

		output.setIdContratFacturation(new IdContrat(
				retourSM.getCompteFacturation().getNumContrat(),
				// PNI 07/03/16 
				// Le numRéférence doit avoir la même valeur que numCompte
				// null,
				retourSM.getCompteFacturation().getNumCompte(),			
				retourSM.getCompteFacturation().getCodProduit(),
				retourSM.getCompteFacturation().getCodSousProduit(),
				retourSM.getCompteFacturation().getCodFamille(),
				retourSM.getCompteFacturation().getNumCompte())
				.toId());

		output.setNombreMaxAlertes(retourSM.getNombreMaxAlertes());


		if (retourSM.getHistoriqueAlertes() != null) {
			output.setHistoriqueAlertes(getHisto(retourSM.getHistoriqueAlertes()).toArray(new HistoriqueAlerteAPIMsg[0]));
		}
		if (retourSM.getAlertesParametrees() != null) {
			output.setAlertesParametrees(getConfiguredAlert(retourSM.getAlertesParametrees()).toArray(new AlerteParametreeAPIMsg[0]));			
		}
		if (retourSM.getAlertesPossibles() != null) {
			output.setAlertesPossibles(getPossibleAlert(retourSM.getAlertesPossibles()).toArray(new AlertePossibleAPIMsg[0]));
		}
		return output;
	}

	private List<HistoriqueAlerteAPIMsg> getHisto(final HistoriqueAlerteMsg[] lstHistoSM) throws IllegalAccessException, InvocationTargetException {
		List<HistoriqueAlerteAPIMsg> lstHisto = new ArrayList<HistoriqueAlerteAPIMsg>();

		for (HistoriqueAlerteMsg histoSM: lstHistoSM) {
			HistoriqueAlerteAPIMsg histo = new HistoriqueAlerteAPIMsg();

			BeanUtils.copyProperties(histo, histoSM);
			histo.setCodAlerte(mapTranscoAlerte.get(histoSM.getIdAlerte()));
			histo.setCodCanal(mapTranscoChannels.get(histoSM.getIdCanal()));
			histo.setIdClient(histoSM.getNumClient());
			histo.setIdContrat(new IdContrat(
					histoSM.getIdContrat().getNumContrat(),
					// PNI 07/03/16 
					// Le numRéférence doit avoir la même valeur que numCompte
					// null,
					histoSM.getIdContrat().getNumCompte(),
					histoSM.getIdContrat().getCodProduit(),
					histoSM.getIdContrat().getCodSousProduit(),
					histoSM.getIdContrat().getCodFamille(),
					histoSM.getIdContrat().getNumCompte())
					.toId());

			lstHisto.add(histo);
		}
		return lstHisto;
	}

	private List<AlerteParametreeAPIMsg> getConfiguredAlert(AlerteParametreeMsg[] lstConfiguredAlertSM) {
		List<AlerteParametreeAPIMsg> lstAlerteParametreeAPIMsg = new ArrayList<AlerteParametreeAPIMsg>();

		for (AlerteParametreeMsg alerteParamSM: lstConfiguredAlertSM) {
			AlerteParametreeAPIMsg alerteParametreeAPIMsg = new AlerteParametreeAPIMsg();

			alerteParametreeAPIMsg.setCode(mapTranscoAlerte.get(alerteParamSM.getCodAlerte()));
			alerteParametreeAPIMsg.setCanal(mapTranscoChannels.get(alerteParamSM.getCodCanal()));
			alerteParametreeAPIMsg.setSupport(mapTranscoProducts.get(alerteParamSM.getCodProduit()));
			alerteParametreeAPIMsg.setId(alerteParamSM.getIdAlerte().toString());
			alerteParametreeAPIMsg.setIdContratSupport(new IdContrat(
					alerteParamSM.getIdContrat().getNumContrat(),
					// PNI 07/03/16 
					// Le numRéférence doit avoir la même valeur que numCompte
					// null,
					alerteParamSM.getIdContrat().getNumCompte(),	
					alerteParamSM.getIdContrat().getCodProduit(),
					alerteParamSM.getIdContrat().getCodSousProduit(),
					alerteParamSM.getIdContrat().getCodFamille(),
					alerteParamSM.getIdContrat().getNumCompte())
					.toId());
			alerteParametreeAPIMsg.setSeuil(alerteParamSM.getSeuil());

			lstAlerteParametreeAPIMsg.add(alerteParametreeAPIMsg);
		}
		return lstAlerteParametreeAPIMsg;
	}

	private List<AlertePossibleAPIMsg> getPossibleAlert(AlertePossibleMsg[] lstPossibleAlert) {
		List<AlertePossibleAPIMsg> lstAlertePossibleAPIMsg = new ArrayList<AlertePossibleAPIMsg>();

		for (AlertePossibleMsg alertePossibleSM: lstPossibleAlert) {
			AlertePossibleAPIMsg alertePossibleAPIMsg = new AlertePossibleAPIMsg();

			alertePossibleAPIMsg.setCode(mapTranscoAlerte.get(alertePossibleSM.getCodAlerte()));
			List<CodMsg> lstCanaux = new ArrayList<CodMsg>();
			List<Integer> lstCanauxSM = alertePossibleSM.getCodCanaux() != null ? Arrays.asList(alertePossibleSM.getCodCanaux()) : new ArrayList<Integer>(); 
			for (Integer canal: lstCanauxSM) {
				lstCanaux.add(mapTranscoChannels.get(canal));
			}
			alertePossibleAPIMsg.setCanaux(lstCanaux.toArray(new CodMsg[0]));

			List<CodMsg> lstSupports = new ArrayList<CodMsg>();
			List<Integer> lstSupportSM = alertePossibleSM.getCodProduit() != null ? Arrays.asList(alertePossibleSM.getCodProduit()) : new ArrayList<Integer>();
			for (Integer support: lstSupportSM) {
				lstSupports.add(mapTranscoProducts.get(support));
			}
			alertePossibleAPIMsg.setSupports(lstSupports.toArray(new CodMsg[0]));

			lstAlertePossibleAPIMsg.add(alertePossibleAPIMsg);
		}
		return lstAlertePossibleAPIMsg;
	}

	private Map<Integer, CodMsg> getMapTransco(final List<String> lstDicoName) throws InvalidPrmException {

		CritereDictionaryMsg critere = new CritereDictionaryMsg();
		critere.setGroupElement(COMMON_GROUP_ELT);

		Map<Integer, CodMsg> returnMap = new HashMap<Integer, CodMsg>();

		for (String item: lstDicoName) {
			critere.setDicoClassName(item);
			try {
				for (DictionaryRefValueMsg retour: paramBC.lireDicoRef(critere).getDicoValue()) {
						returnMap.put(
							retour.getIntCodeElement().intValue(),
							new CodMsg(retour.getIntCodeElement().intValue(), retour.getLibElement()));
				}
			} catch (Exception e) {
				// This not true, lireDicoRef() does NOT throw BusinessException!!!!
				throw new InvalidPrmException(e.getMessage());
			}
		}
		return returnMap;
	}
}
